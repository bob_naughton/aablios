//
//  AppDelegate.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved.
 *
 */

#import <UIKit/UIKit.h>
#import "ERBAppController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) ERBAppController *appController;

@end
