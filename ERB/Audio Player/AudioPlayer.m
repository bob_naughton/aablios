//
//  AudioPlayer.m
//  ERB
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import "AudioPlayer.h"

@interface AudioPlayer (Private)
@end


@implementation AudioPlayer

@synthesize audioPlayer, audioFile, audioVolume, playerState;

#pragma mark Singleton Methods

SYNTHESIZE_SINGLETON(AudioPlayer);

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
    [audioPlayer release];
    [audioFile release];
    [super dealloc];
}

#pragma mark - AudioPlayer Configuration, play and pause methods

// This method in used for playing non-encrypted audio files bundled within the app
- (void)configureAudioPlayerWithAudioFile:(NSString *)_audioFile withDelegate:(id)delegate
{
    
    self.audioFile = _audioFile;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:self.audioFile ofType:nil];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        //Declare the audio file location and settup the player
        NSURL *audioFileLocationURL = [NSURL fileURLWithPath:path];
        
        NSError *error = nil;
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
        [audioSession setActive:YES error:nil];
        AVAudioPlayer *temp = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileLocationURL error:&error];
        [temp prepareToPlay];
        temp.delegate = delegate;
        temp.volume = self.audioVolume;
        self.audioPlayer = temp;
        [temp release];
        temp = nil;
        
        if (error) {
            DLog(@"AUDIO PLAYING ERROR %@", [error localizedDescription]);
        }
    }
    else{
        self.audioPlayer = nil;
    }
    
}

// This method in used for playing non-encrypted tutorial sample question and answer audio files available in Documents directory
- (void)configureAudioPlayerWithAudioFilePath:(NSString *)_audioFilePath withDelegate:(id)delegate
{
    self.audioFile = _audioFilePath;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:_audioFilePath]) {
        //Declare the audio file location and settup the player
        NSURL *audioFileLocationURL = [NSURL fileURLWithPath:_audioFilePath];
        
        NSError *error = nil;
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
        [audioSession setActive:YES error:nil];
        AVAudioPlayer *temp = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileLocationURL error:&error];
        [temp prepareToPlay];
        temp.delegate = delegate;
        temp.volume = self.audioVolume;
        self.audioPlayer = temp;
        [temp release];
        temp = nil;
        
        if (error) {
            DLog(@"AUDIO PLAYING ERROR %@", [error localizedDescription]);
        }
    }
    else{
        self.audioPlayer = nil;
    }
}

// This method in used for playing encrypted exam question and answer audio files available in Documents directory
- (void)configureAudioPlayerWithEncryptedAudioFile:(NSString *)_audioFile withDelegate:(id)delegate
{
    self.audioFile = _audioFile;
        
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.audioFile]) {
        //Declare the audio file location and settup the player
        NSURL *audioFileLocationURL = [NSURL encryptedFileURLWithPath:self.audioFile];
        NSData *audioData = [NSData dataWithContentsOfURL:audioFileLocationURL];
        NSError *error = nil;
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
        [audioSession setActive:YES error:nil];
        AVAudioPlayer *temp = [[AVAudioPlayer alloc] initWithData:audioData error:&error];
        [temp prepareToPlay];
        temp.delegate = delegate;
        temp.volume = self.audioVolume;
        self.audioPlayer = temp;
        [temp release];
        temp = nil;
        
        if (error) {
            DLog(@"ENCRYPTED AUDIO PLAYING ERROR %@", [error localizedDescription]);
        }
    }
    else{
        self.audioPlayer = nil;
    }
}

- (void)playAudio {
    //Play the audio
    [self.audioPlayer play];
    self.playerState = PLAYING;
}

- (void)pauseAudio {
    //Pause the audio
    [self.audioPlayer pause];
    self.playerState = PAUSED;
}

-(void)stopAudio{
    [self.audioPlayer stop];
    self.playerState = STOPPED;
}

- (void) resetAudioPlayer{
    [self stopAudio];
    [self setAudioFile:nil];
    [[self audioPlayer] setDelegate:nil];
    [self setAudioPlayer:nil];
    
}
@end
