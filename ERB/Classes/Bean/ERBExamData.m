//
//  ERBExamData.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBExamData.h"

@implementation ERBExamData
@synthesize examName, examDate, examTime, schoolName;

- (void)dealloc{
    [examDate release];
    [examName release];
    [examTime release];
    [schoolName release];
    [super dealloc];
}

@end
