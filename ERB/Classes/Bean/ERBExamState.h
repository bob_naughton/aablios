//
//  ERBExamState.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <Foundation/Foundation.h>
#import "ERBCheckIn.h"

#define EXAM_STATE_KEY_CHECKIN_DATA             @"checkinData"
#define EXAM_STATE_KEY_CURRENT_SECTION          @"section"
#define EXAM_STATE_KEY_CURRENT_QUESTION_NUMBER  @"questionNumber"
#define EXAM_STATE_KEY_QUESTIONS_ARRAY          @"questionsArray"
#define EXAM_STATE_KEY_CURRENT_STATE            @"examState"
#define EXAM_STATE_KEY_CURRENT_PROGRESS         @"examProgress"

@interface ERBExamState : NSObject

@property (assign, nonatomic) int currentSection;
@property (assign, nonatomic) int currentQuestionNumber;
@property (nonatomic, retain) NSMutableArray *questionsArray;
@property (retain, nonatomic) ERBCheckIn *checkinData;
@property (assign, nonatomic) ERB_EXAM_STATE currentExamState;
@property (assign, nonatomic) EXAM_PROGRESS_IDENTIFIER currentExamProgress;

@end
