//
//  ERBCheckIn.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <Foundation/Foundation.h>

#define ERB_CHECKIN_KEY_EXAM_ID         @"examId"
#define ERB_CHECKIN_KEY_STUDENT_ID      @"studentId"
#define ERB_CHECKIN_KEY_STUDENT_NAME    @"studentName"
#define ERB_CHECKIN_KEY_SEAT_NUMBER     @"seatNumber"

@interface ERBCheckIn : NSObject
@property (nonatomic, retain) NSString *examId;
@property (nonatomic, retain) NSString *studentId;
@property (nonatomic, retain) NSString *studentName;
@property (nonatomic, retain) NSString *seatNumber;
@end
