//
//  ERBExamData.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import <Foundation/Foundation.h>

@interface ERBExamData : NSObject

@property (nonatomic, retain) NSString *examName;
@property (nonatomic, retain) NSString *schoolName;
@property (nonatomic, retain) NSString *examDate;
@property (nonatomic, retain) NSString *examTime;

@end
