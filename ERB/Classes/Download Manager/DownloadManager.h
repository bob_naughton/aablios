//
//  DownloadManager.h
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Download.h"

@class DownloadManager;
@class Download;

@protocol DownloadManagerDelegate <NSObject>

@optional
- (void)didFinishLoadingAllForManager:(DownloadManager *)downloadManager;
- (void)didCancelAllForManager:(DownloadManager *)downloadManager;
- (void)downloadManager:(DownloadManager *)downloadManager downloadDidFinishLoading:(Download *)download;
- (void)downloadManager:(DownloadManager *)downloadManager downloadDidFail:(Download *)download;
- (void)downloadManager:(DownloadManager *)downloadManager downloadDidReceiveData:(Download *)download;

@end

@interface DownloadManager : NSObject
@property NSInteger maxConcurrentDownloads;
@property (nonatomic, retain) NSMutableArray *downloads;
@property (nonatomic, assign) id<DownloadManagerDelegate> delegate;

- (void)addDownloadWithFilePath:(NSString *)filePath URL:(NSURL *)url md5Hash:(NSString *)hash;
- (void)start;
- (void)cancelAll;


@end
