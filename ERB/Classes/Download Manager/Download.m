//
//  Download.m
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import "Download.h"

@interface Download () <NSURLConnectionDelegate>

@property (retain, nonatomic) NSURLConnection *connection;
@property (retain, nonatomic) NSFileHandle *fileHandle;

@end

@implementation Download

#pragma mark - Public methods

- (id)init
{
    self = [super init];
    
    if (self)
    {
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:self.filePath forKey:@"filePath"];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self.filePath = [decoder decodeObjectForKey:@"filePath"];
   
    return self;
}

- (void) dealloc{
    [super dealloc];
}

- (void)start
{
    // initialize progress variables
    
    self.downloading = YES;
    self.reportDownloadFailure = YES;
    self.expectedContentLength = -1;
    self.progressContentLength = 0;
    
    NSError *error = nil;
    
    // Create the necessary directory structure
    NSString *directoryStruct = [self.filePath stringByDeletingLastPathComponent];
    if (![[NSFileManager defaultManager] fileExistsAtPath:directoryStruct])
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryStruct withIntermediateDirectories:YES attributes:nil error:&error];
    
    // Remove the file if it exists
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:self.filePath error:nil];
    }
    
    // Create the file
    [[NSFileManager defaultManager] createFileAtPath:self.filePath contents:nil attributes:nil];
    
    if (error) {
        self.error = error;
        [self cleanupConnectionSuccessful:NO];
        return;
    }
    
    // Initialize the file handler
    self.fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:self.filePath];
    [self.fileHandle seekToEndOfFile];
    
    // Create the request
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    if (!request)
    {
        self.error = [NSError errorWithDomain:[NSBundle mainBundle].bundleIdentifier
                                         code:-1
                                     userInfo:@{@"message": @"Unable to create URL", @"function": @(__FUNCTION__), @"URL" : self.url}];
        
        [self cleanupConnectionSuccessful:NO];
        return;
    }
    
    // Intialize the connection
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    if (!self.connection)
    {
        self.error = [NSError errorWithDomain:[NSBundle mainBundle].bundleIdentifier
                                         code:-1
                                     userInfo:@{@"message": @"Unable to create NSURLConnection", @"function" : @(__FUNCTION__), @"NSURLRequest" : request}];
        
        [self cleanupConnectionSuccessful:NO];
    }
}

- (void)cancel
{
    [self cleanupConnectionSuccessful:NO];
}

#pragma mark - Private methods

- (void)cleanupConnectionSuccessful:(BOOL)success {
    
    // clean up connection and file handle
    if (self.connection){
        if (!success)
            [self.connection cancel];
        SAFE_RELEASE(_connection);
    }
    
    if (self.fileHandle) {
        [self.fileHandle closeFile];
        SAFE_RELEASE(_fileHandle);
    }
    
    self.downloading = NO;
    
    if (success){
        [self.delegate downloadDidFinishLoading:self];
    }
    else{
        if (self.reportDownloadFailure) {
            [self.delegate downloadDidFail:self];
        }
    }
}

#pragma mark - NSURLConnectionDataDelegate methods

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse *)response{
    
    if ([response isKindOfClass:[NSHTTPURLResponse class]]){
        NSHTTPURLResponse *httpResponse = (id)response;
        
        NSInteger statusCode = [httpResponse statusCode];
        
        if (statusCode == 200){
            self.expectedContentLength = [response expectedContentLength];
        }
        else if (statusCode >= 400){
            self.error = [NSError errorWithDomain:[NSBundle mainBundle].bundleIdentifier
                                             code:statusCode
                                         userInfo:@{
                                                    @"message" : @"bad HTTP response status code",
                                                    @"function": @(__FUNCTION__),
                                                    @"NSHTTPURLResponse" : response
                                                    }];
            [self cleanupConnectionSuccessful:NO];
        }
    }
    else{
        self.expectedContentLength = -1;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
   self.dataLength = [data length];
    // Move to the end of file and write data to it.
    [self.fileHandle seekToEndOfFile];
    [self.fileHandle writeData:data];
    
    self.progressContentLength += self.dataLength;

    if ([self.delegate respondsToSelector:@selector(downloadDidReceiveData:)])
        [self.delegate downloadDidReceiveData:self];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self cleanupConnectionSuccessful:YES];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    self.error = error;
    [self cleanupConnectionSuccessful:NO];
    
}

@end
