//
//  Download.h
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Download;

@protocol DownloadDelegate <NSObject>
@optional
- (void)downloadDidFinishLoading:(Download *)download;
- (void)downloadDidFail:(Download *)download;
- (void)downloadDidReceiveData:(Download *)download;
@end

@interface Download : NSObject <NSURLConnectionDelegate>
@property (nonatomic, retain) NSString *filePath;
@property (nonatomic, retain) NSURL *url;
@property (nonatomic, retain) NSString *md5Hash;
@property (nonatomic, assign) id<DownloadDelegate> delegate;
@property (getter = isDownloading) BOOL downloading;
@property (nonatomic, assign) BOOL reportDownloadFailure;
@property long long expectedContentLength;
@property long long progressContentLength;
@property NSUInteger dataLength;
@property (nonatomic, retain) NSError *error;

- (void)start;
- (void)cancel;

@end
