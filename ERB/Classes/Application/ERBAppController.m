//
//  ERBAppController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBAppController.h"
#import <QuartzCore/QuartzCore.h>
#import "ERBMainMenuViewController.h"
#import "ERBBeginTutorialViewController.h"
#import "SSZipArchive.h"
#import "AudioPlayer.h"

@interface ERBAppController ()
- (void) launchStartupScreen;
- (void) applyFadeAnimation;
- (void) setupReachability;

@property (nonatomic, retain) ERBMainMenuViewController *mainMenuViewController;

@end

@implementation ERBAppController

@synthesize bytesSent, bytesReceived;
@synthesize examMode;

#pragma mark - INIT
- (id)init
{
    self = [super init];
    if (self) {
        self.bytesSent = 0;
        self.bytesReceived = 0;
    }
    return self;
}

#pragma mark Public Methods
- (void) applicationDidLaunchWithWindow: (UIWindow *) window
{
	mainWindow = window;
    dispatch_async(dispatch_get_main_queue(), ^{
        //Do reachability setup.
        [self setupReachability];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(guidedAccessChanged:) name:UIAccessibilityGuidedAccessStatusDidChangeNotification object:nil];
    });
    
    [self sendClientEventLogs];
    [self launchStartupScreen];
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Sends client logs
///////////////////////////////////////////////////////////////////////
- (void) sendClientEventLogs
{
    //Set the flag to YES by default so that we capture the Login web service.
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:RESPONSE_KEY_ENABLE_CLIENT_LOGGING];
    [[NSUserDefaults standardUserDefaults] synchronize];

    // Set the current event as App Restart for logging purpose
    [[EventLogger sharedEventLogger] setEventType:LOG_EVENT_APP_RESTART];
    // Send Event Logs to server
//    [[EventLogger sharedEventLogger] sendLogs];
}

- (void) setupReachability
{
    //Internet Check using reachability
	internetReach = [[Reachability reachabilityForInternetConnection] retain];
	[internetReach startNotifier];
	[self updateInterfaceWithReachability: internetReach];
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Apply a simple fade animation to window..
//// Parameters         : Nil 
//// Return type        : Nil
//// Comments           : Nil
///////////////////////////////////////////////////////////////////////
- (void) applyFadeAnimation
{
	CATransition *animation = [CATransition animation];
	[animation setType:kCATransitionFade];
	[animation setSubtype:nil];
	[animation setDuration:1.0];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	[mainWindow.layer addAnimation:animation forKey:nil];
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Launch the startup screen..
//// Parameters         : Nil 
//// Return type        : Nil
//// Comments           : Nil
///////////////////////////////////////////////////////////////////////
- (void) launchStartupScreen
{
    //[self applyFadeAnimation];
    //[splashViewController.view removeFromSuperview];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
#ifdef GA_ENABLED
    //Make Main Menu as the root view controller
    _mainMenuViewController = [[ERBMainMenuViewController alloc] init];
    mainWindow.rootViewController = _mainMenuViewController;
    SAFE_RELEASE(_mainMenuViewController);
#else
    ERBBeginTutorialViewController *beginTutorialViewController = [[ERBBeginTutorialViewController alloc] init];
    mainWindow.rootViewController = beginTutorialViewController;
    [beginTutorialViewController release];
#endif
}

#pragma mark -
#pragma mark Rechability method

- (void) reachabilityChanged: (NSNotification* )note 
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
	if(curReach == internetReach) 
	{
		NetworkStatus netStatus = [curReach currentReachabilityStatus];
		switch (netStatus)
        {
			case NotReachable:
            {
				isInternetAvailable = FALSE;
				break;
			}
			case ReachableViaWWAN: 
            {
				isInternetAvailable = TRUE;
				break;
			}
			case ReachableViaWiFi: 
            {
				isInternetAvailable = TRUE;
				break;
			}
		}
	}
}
#pragma mark -
#pragma mark InternetCheck method

- (BOOL)internetCheck {
	return isInternetAvailable;
}


#pragma mark -
#pragma mark Guided Access method

- (void)guidedAccessChanged:(NSNotification *)notice{
    if(!UIAccessibilityIsGuidedAccessEnabled()){
        //Make Main Menu as the root view controller
        _mainMenuViewController = [[ERBMainMenuViewController alloc] init];
        mainWindow.rootViewController = self.mainMenuViewController;
        SAFE_RELEASE(_mainMenuViewController);
    }
}


         
@end
