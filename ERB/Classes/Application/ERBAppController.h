//
//  ERBAppController.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface ERBAppController : NSObject
{
    UIWindow               *mainWindow;
    //For Reachability
    Reachability           *internetReach;
    BOOL                   isInternetAvailable;
}

@property (nonatomic, assign) unsigned long long bytesSent;
@property (nonatomic, assign) unsigned long long bytesReceived;
@property (nonatomic, retain) NSString *examMode;

- (void) applicationDidLaunchWithWindow: (UIWindow *) window;
//For Reachability
//Internet Connection Check
- (void) updateInterfaceWithReachability: (Reachability*) curReach;
- (BOOL) internetCheck;
- (void) guidedAccessChanged: (NSNotification *)notice;

@end
