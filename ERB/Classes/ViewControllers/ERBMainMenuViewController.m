//
//  ERBMainMenuViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBMainMenuViewController.h"
#import "ERBBeginTutorialViewController.h"
#import "ERBDeviceSetupViewController.h"
#import "ERBAboutAABLViewController.h"


@interface ERBMainMenuViewController ()
-(void)removeProctorTestErrorForButton:(UIButton *)button;
@property (nonatomic, retain) ERBBeginTutorialViewController *beginTutorialViewController;
@end

@implementation ERBMainMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Guided Access Disabled" object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    SAFE_RELEASE(_beginTutorialViewController);
    [_backgroundImageView release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Action Methods

- (IBAction)aboutButtonTapped:(id)sender {
    ERBAboutAABLViewController *aboutViewController = [[ERBAboutAABLViewController alloc] init];
    [self presentViewController:aboutViewController animated:YES completion:nil];
    [aboutViewController release];
}

- (IBAction)deviceSetUpTapped:(id)sender {
    ERBDeviceSetupViewController *deviceSetupViewController = [[ERBDeviceSetupViewController alloc] init];
    [self presentViewController:deviceSetupViewController animated:YES completion:nil];
    [deviceSetupViewController release];
}

- (IBAction)proctoredTestTapped:(id)sender {
#if TARGET_IPHONE_SIMULATOR
    if (!UIAccessibilityIsGuidedAccessEnabled()) {
#else
        if (UIAccessibilityIsGuidedAccessEnabled()) {
#endif
            //Make begin Exam as the root view controller
            _beginTutorialViewController = [[ERBBeginTutorialViewController alloc] init];
            UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
            mainWindow.rootViewController = _beginTutorialViewController;
            SAFE_RELEASE(_beginTutorialViewController);
        }
        else{
            [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
            UIButton *button = (UIButton*) sender;
            [button setBackgroundImage:SET_IMAGE(@"ProctoredTest-NoGA-Error.png") forState:UIControlStateNormal];
            [self performSelector:@selector(removeProctorTestErrorForButton:) withObject:button afterDelay:PROCTOR_ERROR_DELAY];
        }
    }
    
#pragma mark - Private Methods
    
    -(void)removeProctorTestErrorForButton:(UIButton *)button{
        [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
        [button setBackgroundImage:SET_IMAGE(@"ProctoredTest-Idle.png") forState:UIControlStateNormal];
    }
    
    @end
