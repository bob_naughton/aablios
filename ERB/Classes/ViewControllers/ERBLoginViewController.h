//
//  ERBLoginViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBWebOperation.h"

@protocol ERBLoginViewControllerDelegate <NSObject>
- (void) adminDidLogin: (BOOL) didLogin withData:(NSArray *)dataArray;
- (void) didPressHelp;
- (void) testDidResumeInState: (NSString *)examState;
@end

@interface ERBLoginViewController : UIViewController<UITextFieldDelegate,ERBWebServiceDelegate>

@property (nonatomic, assign) id <ERBLoginViewControllerDelegate> delegate;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UITextField *userName;
@property (retain, nonatomic) IBOutlet UITextField *password;
@property (retain, nonatomic) IBOutlet UIButton *loginButton;
@property (retain, nonatomic) IBOutlet UILabel *userLoginLabel;
@property (retain, nonatomic) IBOutlet UILabel *helpLabel;
@property (retain, nonatomic) IBOutlet UIView *iOS7StatusBar;
@property (retain, nonatomic) IBOutlet UIView *uploadResponsesHoverView;
@property (nonatomic, retain) NSDictionary *loginResponse;

- (IBAction)loginButtonPressed:(id)sender;
- (IBAction)helpButtonPressed:(id)sender;

@end
