//
//  ERBTutorialViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBTutorialViewControllerOffline.h"
#import "AudioPlayer.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ERBTutorialViewControllerOffline ()
@property (nonatomic, retain) MPMoviePlayerController *moviePlayerController;
@property (nonatomic, retain) NSArray *dynamicTutorialHTMLNames;
@property (nonatomic, retain) NSArray *dynamicTutorialVideoNames;
@property (nonatomic, assign) ERB_CURRENT_TUTORIAL_USER_ACTION currentUserAction;
@property (nonatomic, assign) BOOL isOmitQuestionAudioPlayed;

- (void) configureMoviePlayerWithMovieName: (NSString *) movieName ofType: (NSString *)type;
- (void) playPreQuestionAudio;
- (void) playQuestionAudio;
- (void) playPostQuestionAudio;
- (void) disableAllButtons;
- (void) enableAllButtons;
- (void) pauseVideo;
- (void) playVideo;
- (void) playAnswerAudioWithAnswerID: (NSString *)answerId;
- (void) displayTutorialQuestion;
- (void) examStopNotificationReceived:(NSNotification *)notification;

@end

@implementation ERBTutorialViewControllerOffline
@synthesize delegate;
@synthesize tutorialImageView;
@synthesize checkInData;
@synthesize currentTutorialEvent;
@synthesize moviePlayerController;
@synthesize dynamicTutorialHTMLNames;
@synthesize dynamicTutorialVideoNames;
@synthesize currentUserAction;
@synthesize isOmitQuestionAudioPlayed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    [NSArray arrayWithObjects:@"", nil];
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - UIWebViewDelegate

-(BOOL) webView:(UIWebView *)_webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [[request URL] absoluteString];
    
    if ([requestString hasPrefix:@"js-frame:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        
        NSString *function = (NSString*)[components objectAtIndex:1];
        NSString *answerId = [components objectAtIndex:2];
        
        if ([function isEqualToString:@"playAnswerAudio"])
        {
            [self playAnswerAudioWithAnswerID:answerId];
        }
        return NO;
    }
    return YES;
}

-(void) webViewDidFinishLoad:(UIWebView *)_webView
{
    [self performSelector:@selector(displayTutorialQuestion) withObject:Nil afterDelay:0.5];
}
#pragma mark - Private Methods

- (void) disableAllButtons
{
#ifdef FAST_FORWARD
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#else
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#endif
}

- (void) enableAllButtons
{
    self.nextButton.enabled = YES;
    self.replayButton.enabled = YES;
}

- (void) playPostQuestionAudio{
    // Play the try yourself audio
    self.currentUserAction = TUTORIAL_USER_ACTION_POST_QUESTION_AUDIO;
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_POST_QUESTION_ONE_AUDIO withDelegate:self];
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void) playPreQuestionAudio{
    // Play the try yourself audio
    self.currentUserAction = TUTORIAL_USER_ACTION_PRE_QUESTION_AUDIO;
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_TRY_QUESTION withDelegate:self];
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void) playQuestionAudio
{
    self.currentUserAction = TUTORIAL_USER_ACTION_QUESTION_AUDIO;
    
    NSString *jsonPath = [[TUTORIAL_JSON_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"json"];
    NSString *json = [NSString stringWithContentsOfURL:[NSURL encryptedFileURLWithPath:jsonPath] encoding:NSUTF8StringEncoding error:nil];
    
    NSError *error = nil;
    
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        //Fix for iOS 7
        NSString *questionAudioPath = [dataDic objectForKey:@"questionAudio"];
        NSString *absoluteAudioPath = [TUTORIAL_AUDIO_PATH stringByAppendingPathComponent:[questionAudioPath lastPathComponent]];
        
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] performSelector:@selector(playAudio) withObject:nil afterDelay:0.75];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
}

- (void) playAnswerAudioWithAnswerID: (NSString *)answerId
{
    
    NSString *jsonPath = [[TUTORIAL_JSON_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"json"];
    NSString *json = [NSString stringWithContentsOfURL:[NSURL encryptedFileURLWithPath:jsonPath] encoding:NSUTF8StringEncoding error:nil];
    
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSArray *answerChoicesArray = [dataDic objectForKey:@"choices"];
        
        for (NSDictionary *dict in answerChoicesArray)
        {
            NSString *tempAnswerId = [dict objectForKey:@"answerID"];
            if ([tempAnswerId isEqualToString: answerId])
            {
                NSString *answerAudioPath = [dict objectForKey:@"answerAudio"];
                NSString *absoluteAudioPath = [TUTORIAL_AUDIO_PATH stringByAppendingPathComponent:[answerAudioPath lastPathComponent]];
                
                [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
                //Set the state
                self.currentUserAction = TUTORIAL_USER_ACTION_ANSWER_AUDIO;
                [[AudioPlayer sharedAudioPlayer] playAudio];
                break;
            }
        }
        
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) enableAnswerChoices
{
    //Enable choices
    [self.dynamicQuestionWebView stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
}

- (void) customizeUI
{
    self.tutorialLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:45.0];
    self.readyToBeginLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    self.beginPracticeLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
}

- (void) adjustGUI{
    
    self.currentUserAction = TUTORIAL_USER_ACTION_SLIDE_AUDIO;
    
    //Set the image for tutorial image view based on the tag of previously tapped button according to the help mode
    BOOL isHelpMode = [USER_DEFAULTS boolForKey:RESPONSE_KEY_HELP_HAND];
    
    NSString *imageName = nil;
    if (isHelpMode)
    {
        imageName = [NSString stringWithFormat:@"slide0%d.png",self.currentTutorialEvent];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"slide0%d_without_help.png",self.currentTutorialEvent];
    }
    
    UIImage *tutorialImage = [UIImage imageWithContentsOfFile:IMAGE_PATH(imageName)];
    [self.tutorialImageView setImage:tutorialImage];
    
    //Configure the audio player to play file based on current tutorial scene
    switch (self.currentTutorialEvent+1) {
        case TUTORIAL_EVENT_START:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_INTRO withDelegate:self];
            break;
        case TUTORIAL_EVENT_HELP:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_HELP_BUTTON withDelegate:self];
            break;
        case TUTORIAL_EVENT_REPLAY:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_REPLAY_AUDIO_BUTTON withDelegate:self];
            break;
        case TUTORIAL_EVENT_NEXT:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_NEXT_BUTTON withDelegate:self];
            break;
        default:
            break;
    }
    if (self.currentTutorialEvent != TUTORIAL_EVENT_NEXT)
    {
        [[AudioPlayer sharedAudioPlayer] playAudio];
    }
}


- (void) configureMoviePlayerWithMovieName: (NSString *) movieName ofType: (NSString *)type
{
    moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:[[TUTORIAL_VIDEO_PATH stringByAppendingPathComponent:movieName] stringByAppendingPathExtension:type]]];
    ;
    
    self.moviePlayerController.controlStyle = MPMovieControlStyleNone;
    self.moviePlayerController.view.frame = self.view.frame;
    self.moviePlayerController.currentPlaybackTime = 1.0;
    self.moviePlayerController.backgroundView.backgroundColor = [UIColor clearColor];
    self.moviePlayerController.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.moviePlayerController.view];
    [self.moviePlayerController prepareToPlay];
    [self.moviePlayerController play];
}

- (void) loadHtml
{
    //Covert JSON to NSDictionary
    
    NSString *jsonPath = [[TUTORIAL_JSON_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"json"];
    NSString *json = [NSString stringWithContentsOfURL:[NSURL encryptedFileURLWithPath:jsonPath] encoding:NSUTF8StringEncoding error:nil];
    
    if([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0)){
        json = [json stringByReplacingOccurrencesOfString:@".png" withString:@"@2x.png"];
    }
    
    //Create a request and load HTML Template in webview based on Type Parameter
    NSString *htmlFilePath = nil;
    NSURL *url = nil;
    
    htmlFilePath = [[TUTORIAL_HTML_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"html"];
    
    url = [NSURL encryptedFileURLWithPath:htmlFilePath];
    
    NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    html = [html stringByReplacingOccurrencesOfString:@"//$$REPLACEDATA$$//" withString:[NSString stringWithFormat:@"var jsondata = '%@';",[json stringByReplacingOccurrencesOfString:@"\n" withString:@""]]];
    
    [self.dynamicQuestionWebView loadHTMLString:html baseURL:url];
    
    [self disableAllButtons];
}

-(void) displayTutorialQuestion {
    [self.moviePlayerController.view removeFromSuperview];
    SAFE_RELEASE(moviePlayerController);
    self.dynamicTutorialView.hidden = NO;
}

- (void) examStopNotificationReceived:(NSNotification *)notification{
    [self.moviePlayerController stop];
}

#pragma mark - MPMoviePlayerController Notification methods
- (void) playbackFinished : (NSNotification *) notification
{
    [self loadHtml];
    
    [self.view addSubview:self.dynamicTutorialView];
    self.dynamicTutorialView.hidden = YES;
    // Do not perform any activity if global pause is on
    
    //    [self playPreQuestionAudio];
    [self playQuestionAudio];
}

-(void)pauseVideo{
    if (moviePlayerController && self.moviePlayerController.playbackState == MPMoviePlaybackStatePlaying) {
        [self.moviePlayerController pause];
    }
}

-(void)playVideo{
    if (self.moviePlayerController) {
        [self.moviePlayerController play];
    }
}


#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    
    if (self.currentUserAction == TUTORIAL_USER_ACTION_QUESTION_AUDIO)
    {
        //We play post question audio only for the first question.
        if ((self.currentTutorialEvent - TUTORIAL_DYNAMIC_1) == 0)
        {
            [self enableAllButtons];
            [self enableAnswerChoices];
        }
        else
        {
            //Enable all buttons
            [self enableAllButtons];
            [self enableAnswerChoices];
        }
    }
    else if (self.currentUserAction == TUTORIAL_USER_ACTION_BEGIN_PRACTICE)
    {
        self.beginPracticeButton.enabled = YES;
    }
    else if (self.currentUserAction ==  TUTORIAL_USER_ACTION_SLIDE_AUDIO)
    {
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        
        self.currentTutorialEvent++;
        if (self.currentTutorialEvent == TUTORIAL_EVENT_REPLAY)
        {
            BOOL isHelpMode = [USER_DEFAULTS boolForKey:RESPONSE_KEY_HELP_HAND];
            
            if (!isHelpMode)
            {
                self.currentTutorialEvent++;
            }
        }
        if (self.currentTutorialEvent <  TUTORIAL_EVENT_NEXT)
        {
            [self performSelector:@selector(adjustGUI) withObject:nil afterDelay:DELAY];
        }
        else
        {
            [self callMethod];
        }
    }
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - Action Methods
- (IBAction)beginPracticeButtonPressed:(id)sender
{
    [self.beginPracticeView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1];
    [self configureMoviePlayerWithMovieName:[self.dynamicTutorialVideoNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1] ofType:@"m4v"];
    
#ifdef FAST_FORWARD
    [self loadHtml];
#endif
}

- (IBAction)nextButtonPressed:(id)sender
{
    // Get the response of question
    [self.dynamicQuestionWebView stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
    NSString *questionResponse = [self.dynamicQuestionWebView stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    
    
    if ([questionResponse isEqualToString:@"0"] && !self.isOmitQuestionAudioPlayed)
    {
        self.isOmitQuestionAudioPlayed = YES;
        // Stop the audio player
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_OMIT_QUESTION withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] playAudio];
        return;
    }
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    //Set the user action to NONE
    self.currentUserAction = TUTORIAL_USER_ACTION_NONE;
    [self callMethod];
}

- (IBAction)helpButtonPressed:(id)sender
{
    [[AudioPlayer sharedAudioPlayer] stopAudio];
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_WAIT_FOR_HELP withDelegate:self];
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (IBAction)repeatAudioButtonPressed:(id)sender
{
    [self playQuestionAudio];
}

- (void) callMethod
{
    if (self.currentTutorialEvent == TUTORIAL_EVENT_NEXT)
    {
        //Increment the tutorial event
        self.currentTutorialEvent++;
        [self.view addSubview:self.beginPracticeView];
        [self.tutorialImageView setImage:nil];
        
        self.currentUserAction = TUTORIAL_USER_ACTION_BEGIN_PRACTICE;
        
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_BEGIN_PRACTICE withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] playAudio];
        
    }
    else if (self.currentTutorialEvent <= TUTORIAL_EVENT_END)
    {
        //Increment the tutorial event
        self.currentTutorialEvent++;
        self.isOmitQuestionAudioPlayed = NO;
        if (self.currentTutorialEvent > TUTORIAL_EVENT_END)
        {
            //Check the currentTutorialEvent count. If it is more than the number of dynamic tutorials, call delegate
            if (delegate && [delegate respondsToSelector:@selector(didEndTutorial)])
            {
                [delegate didEndTutorial];
                return;
            }
        }
        
        [self.dynamicTutorialView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1];
        [self configureMoviePlayerWithMovieName:[self.dynamicTutorialVideoNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1] ofType:@"m4v"];
#ifdef FAST_FORWARD
        [self loadHtml];
#endif
    }
}

#pragma mark - UIScrollViewDelegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

#pragma mark- LifeCycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Disable scroll in web view
    UIScrollView *scrollView = self.dynamicQuestionWebView.scrollView;
    scrollView.delegate = self;
    scrollView.scrollEnabled = NO;
    scrollView.bounces = NO;
    
    self.currentUserAction = TUTORIAL_USER_ACTION_NONE;
    self.currentUserAction = TUTORIAL_USER_ACTION_SLIDE_AUDIO;
    self.dynamicTutorialHTMLNames = [[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:DYNAMIC_TUTORIAL_PLIST_NAME ofType:@"plist"]] objectAtIndex:0];
    self.dynamicTutorialVideoNames = [[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:DYNAMIC_TUTORIAL_PLIST_NAME ofType:@"plist"]] objectAtIndex:2];
    
    //Customize all fonts
    [self customizeUI];
    
    // Add notification for movie player
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseVideo) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(examStopNotificationReceived:) name:NOTIFICATION_EXAM_STOPPED object:nil];
    
    // Set current tutorial event to non existent value
    self.currentTutorialEvent = TUTORIAL_EVENT_NONE;
    
    // Set the Hello Slide for tutorial intro
    NSString *imageName = [NSString stringWithFormat:@"slide0%d.png",self.currentTutorialEvent];
    UIImage *tutorialImage = [UIImage imageWithContentsOfFile:IMAGE_PATH(imageName)];
    [self.tutorialImageView setImage:tutorialImage];
    
    // Do any additional setup after loading the view from its nib.
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_INTRO withDelegate:self];
    
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0, 0, 1024, 768);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setTutorialImageView:nil];
    [self setCheckInData:nil];
    [self setBeginPracticeView:nil];
    [self setTutorialLabel:nil];
    [self setReadyToBeginLabel:nil];
    [self setBeginPracticeLabel:nil];
    [self setDynamicQuestionWebView:nil];
    [self setDynamicTutorialView:nil];
    [self setReplayButton:nil];
    [self setNextButton:nil];
    [self setBeginPracticeButton:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    SAFE_RELEASE(dynamicTutorialHTMLNames);
    [checkInData release];
    [tutorialImageView release];
    [_beginPracticeView release];
    [_tutorialLabel release];
    [_readyToBeginLabel release];
    [_beginPracticeLabel release];
    [_dynamicQuestionWebView release];
    [_dynamicTutorialView release];
    [_replayButton release];
    [_nextButton release];
    [_beginPracticeButton release];
    [super dealloc];
}

@end
