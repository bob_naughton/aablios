//
//  ERBMainMenuViewController.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>

@interface ERBMainMenuViewController : UIViewController
- (IBAction)aboutButtonTapped:(id)sender;
- (IBAction)deviceSetUpTapped:(id)sender;
- (IBAction)proctoredTestTapped:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
