//
//  ERBHelpViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBHelpViewController.h"
#define kBuildDate @"Build Date"

@interface ERBHelpViewController ()

@end

@implementation ERBHelpViewController   
@synthesize dateLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.helpTextView.font = [UIFont fontWithName:FONT_NAME_FUTURA_BOOK size:22.0];
    NSString *scheme = @"";
    
#ifdef VGD_SCHEME
    scheme = @"DEV VGD";
#elif QA_VGD_SCHEME
    scheme = @"QA VGD";
#elif AABL_STAGE_SCHEME
    scheme = @"AABL Stage";
#endif
    
#ifdef LOCAL
    dateLabel.text = [NSString stringWithFormat:@"%s %@ Local v1.0", __DATE__, scheme];
#else
    dateLabel.text = [NSString stringWithFormat:@"%s %@ v1.0", __DATE__, scheme];
#endif
    
#ifdef AABL_SCHEME
    dateLabel.text = @"v1.0";
#endif
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dealloc {
    [dateLabel release];
    [_helpTextView release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setDateLabel:nil];
    [self setHelpTextView:nil];
    [super viewDidUnload];
}
@end
