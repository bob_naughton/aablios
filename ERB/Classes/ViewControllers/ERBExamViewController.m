//
//  ERBExamViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBExamViewController.h"
#import "EncryptedFileURLProtocol.h"
#import "AudioPlayer.h"
#import "EventLogger.h"
#import "ERBBeginTutorialViewController.h"
#import "ERBExamState.h"

@interface ERBExamViewController ()

@property (nonatomic, assign) BOOL isHelpNeeded;
- (void) unHideExamViewController;
- (void) callReportUserActionWebServiceForAction:(NSString *)userAction;
- (void) endSection;
- (void) loadQuestionUsingJson:(NSString *)json inWebView:(UIWebView *)_webView;
- (void) recallWebService;
- (void) playAudio;
- (void) playAnswerAudioWithAnswerID: (NSString *)answerId;
- (NSString *) getCorrectAnswerFromJson:(NSDictionary *)json;
- (void) callHelpWebService;
- (void) enableAnswerChoices;
- (void) saveExamState;

@property (assign, nonatomic) int elapsedSectionTime;
@property (assign, nonatomic) int elapsedQuestionTime;
@property (assign, nonatomic) NSTimeInterval questionStartTime;
@property (assign, nonatomic) NSTimeInterval questionEndTime;
@property (retain, nonatomic) NSString *secondsElapsed;
@property (nonatomic, assign) AUDIO_STATE currentAudioState;
@property (nonatomic, assign) BOOL didReceiveNextQuestionResponse;
@property (nonatomic, assign) BOOL didPlayQuestionAudioOnce;
@property (retain, nonatomic) ERBWebOperation *userActionWebServiceHelper;
@property (retain, nonatomic) ERBWebOperation *helpWebServiceHelper;
@property (nonatomic, assign) BOOL isOmitQuestionAudioPlayed;

@end

@implementation ERBExamViewController
@synthesize webView, webViewAlternate, sectionEndImageView, checkinData, activityIndicator, nextButton, replayButton, delegate;
@synthesize section_id, questionsCount;
@synthesize questionsArray, questionResponse;
@synthesize currentUserAction;
@synthesize isHelpNeeded;
@synthesize elapsedSectionTime, elapsedQuestionTime, questionEndTime, questionStartTime, secondsElapsed;
@synthesize currentAudioState;
@synthesize didReceiveNextQuestionResponse;
@synthesize didPlayQuestionAudioOnce;
@synthesize userActionWebServiceHelper, helpWebServiceHelper;
@synthesize isOmitQuestionAudioPlayed;

#pragma mark - INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - UIWebViewDelegate

-(BOOL) webView:(UIWebView *)_webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [[request URL] absoluteString];
    
    if ([requestString hasPrefix:@"js-frame:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        
        NSString *function = (NSString*)[components objectAtIndex:1];
        NSString *answerId = [components objectAtIndex:2];
        
        if ([function isEqualToString:@"playAnswerAudio"])
        {
            [self playAnswerAudioWithAnswerID:answerId];
        }
        return NO;
    }
    return YES;
}

-(void) webViewDidStartLoad:(UIWebView *)_webView{
    
}

-(void) webViewDidFinishLoad:(UIWebView *)_webView
{
    //Disable copy in UIWebView
    // Disable user selection
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    // Disable callout
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    
    //FIx for white screen appearing for the first time.
    if (_webView == self.webView && self.questionsCount == 1)
    {
        [self performSelector:@selector(unHideExamViewController) withObject:nil afterDelay:3.0];
        
    }
}

- (void) unHideExamViewController
{
    if (delegate && [delegate respondsToSelector:@selector(questionDidLoadSuccessfullyForFirstTime)])
    {
        // Set the start time for first question
        self.questionStartTime = [[NSDate date] timeIntervalSince1970];
        
        [delegate questionDidLoadSuccessfullyForFirstTime];
    }
}

-(void) webView:(UIWebView *)_webView didFailLoadWithError:(NSError *)error{
    DLog("Exam Web View error: %@",[error localizedDescription]);
}

#pragma mark - Action Methods
- (IBAction)nextButtonPressed:(id)sender
{
    // Get the response of question
    if (!self.webView.isHidden) {
        [self.webView stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
        self.questionResponse = [self.webView stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    }
    else{
        [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
        self.questionResponse = [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    }
    
    if ([self.questionResponse isEqualToString:@"0"] && NO == self.isOmitQuestionAudioPlayed)
    {
        self.isOmitQuestionAudioPlayed = YES;
        // Stop the audio player
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_OMIT_QUESTION withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] playAudio];
        return;
    }
    
    //Reset the bool flag to NO
    self.isOmitQuestionAudioPlayed = NO;
    
    // Cancel the help web service request sent for previous question
    CANCEL_CONNECTION(self.helpWebServiceHelper);
    
    // Stop the audio player
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    
    // Set the question end time and get the elapsed time
    self.questionEndTime = [[NSDate date] timeIntervalSince1970];
    self.secondsElapsed = [ERBCommons secondsBetweenStartInterval:self.questionStartTime andEndInterval:self.questionEndTime];
    
    // Set the start time for next question
    self.questionStartTime = self.questionEndTime;
    
    //Reset the didReceiveNextQuestionResponse to NO
    self.didReceiveNextQuestionResponse = NO;
    
    //Reset the didPlayQuestionAudioOnce to NO
    self.didPlayQuestionAudioOnce = NO;
    
    // End Section if question Id is equal to max limit of questions per section
    if (self.questionsCount == QUESTION_LIMIT_PER_SECTION)
    {
        [self endSection];
    }
    else
    {
        self.currentUserAction = ERB_EXAM_USER_ACTION_SUBMIT;
        //Call web service to get next question.
        [self callReportUserActionWebServiceForAction:USER_ACTION_SUBMIT];
        
        // Apply the fade animation for questions
        [ERBCommons applyFadeAnimationTo:self.view.window withDuration:0.5];
        
        // Disable Next and Replay Audio Button
        self.nextButton.enabled = NO;
        self.replayButton.enabled = NO;
        
        // Based on current webview's visibility, hide/unhide the webviews
        if (self.webViewAlternate.isHidden) {
            //enable audio
            [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:1]];
            
            self.webViewAlternate.hidden = NO;
            self.webView.hidden = YES;
        }
        else{
            //enable audio
            [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:1]];
            self.webViewAlternate.hidden = YES;
            self.webView.hidden = NO;
        }
    }
}

- (IBAction)helpButtonPressed:(id)sender
{
    self.isHelpNeeded = YES;
    
    self.currentUserAction = ERB_EXAM_USER_ACTION_HELP;
    
    // Disable Next and Replay Audio Button
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
    
    // Stop the previosly playing audio; Play the help Audio
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_WAIT_FOR_HELP withDelegate:self];
    
    [[AudioPlayer sharedAudioPlayer] playAudio];
    
    // Call the help web service
    [self callHelpWebService];
    
}

- (IBAction)playAudioButtonPressed:(id)sender
{
    self.currentUserAction = ERB_EXAM_USER_ACTION_NONE;
    
    if (self.didPlayQuestionAudioOnce && self.didReceiveNextQuestionResponse) {
        // Enable Next and Replay Audio Button
        self.nextButton.enabled = YES;
        
        //Enable answer choices
        [self enableAnswerChoices];
        
        // Disable the Replay Button
        self.replayButton.enabled = NO;
    }
    
    //Play question audio according to whether we have received next question. If we have not, we play the audio at index 0, else we play audio at index 1.
    if (self.didReceiveNextQuestionResponse)
    {
        [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:0]];
    }
    else
    {
        [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:1]];
    }
}

#pragma mark - Private Methods

- (void) saveExamState{
    
    NSData *encodedObject = [USER_DEFAULTS objectForKey:KEY_EXAM_STATE];
    ERBExamState *examState = nil;
    if (encodedObject) {
        examState = (ERBExamState *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    }
    if (!examState) {
        examState = [[[ERBExamState alloc] init] autorelease];
    }
    examState.currentSection = self.section_id;
    examState.currentQuestionNumber = self.questionsCount;
    examState.questionsArray = self.questionsArray;
    examState.checkinData = self.checkinData;
    examState.currentExamState = ERB_EXAM_STATE_EXAM;
    examState.currentExamProgress = [(ERBBeginTutorialViewController *)self.delegate currentExamProgress];
    
    [(ERBBeginTutorialViewController *)self.delegate setSavedQuestionsArray:self.questionsArray];
    
    encodedObject = [NSKeyedArchiver archivedDataWithRootObject:examState];
    SAVE_USER_DEFAULTS_AND_SYNC(encodedObject, KEY_EXAM_STATE);
    
}

- (void) enableAnswerChoices
{
    //Enable choices
    if (!self.webView.isHidden)
    {
        [self.webView stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
    }
    else
    {
        [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
    }
}

- (void)playAudio{
    
    if (!self.didPlayQuestionAudioOnce) {
        [self playAudioButtonPressed:nil];
    }
    else if (self.didReceiveNextQuestionResponse) {
        // Enable Next and Replay Audio Button
        self.nextButton.enabled = YES;
        // Enable the Replay Button
        self.replayButton.enabled = YES;
        //Enable answer choices
        [self enableAnswerChoices];
    }
}

- (void) playAnswerAudioWithAnswerID: (NSString *)answerId
{
    ERBQuestion *question = [self.questionsArray objectAtIndex:0];
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[question.question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSArray *answerChoicesArray = [dataDic objectForKey:@"choices"];
        
        for (NSDictionary *dict in answerChoicesArray)
        {
            NSString *tempAnswerId = [dict objectForKey:@"answerID"];
            if ([tempAnswerId isEqualToString: answerId])
            {
                NSString *answerAudioPath = [dict objectForKey:@"answerAudio"];
                NSString *absoluteAudioPath = [[answerAudioPath stringByReplacingOccurrencesOfString:@"audio/" withString:@""] stringByReplacingOccurrencesOfString:@"mp3" withString:@"m4a"];
                absoluteAudioPath = [EXAM_AUDIO_PATH stringByAppendingPathComponent:absoluteAudioPath];
                
                [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
                //Set the state
                self.currentAudioState = AUDIO_STATE_ANSWER;
                [[AudioPlayer sharedAudioPlayer] playAudio];
                break;
            }
        }
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) playQuestionAudioAtIndex: (NSNumber *)index
{
    if ([[AudioPlayer sharedAudioPlayer] playerState] == PLAYING && ([[[AudioPlayer sharedAudioPlayer] audioFile] isEqualToString:AUDIO_PAUSE_SCREEN_PRE_EXAM_START] || [[[AudioPlayer sharedAudioPlayer] audioFile] isEqualToString:AUDIO_PAUSE])) {
        return;
    }
    
    ERBQuestion *question = [self.questionsArray objectAtIndex:[index intValue]];
    
    NSError *error = nil;
    
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[question.question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSString *questionAudioPath = [dataDic objectForKey:@"questionAudio"];
        NSString *absoluteAudioPath = [[questionAudioPath stringByReplacingOccurrencesOfString:@"mp3" withString:@"m4a"] stringByReplacingOccurrencesOfString:@"audio/" withString:@""];
        absoluteAudioPath = [EXAM_AUDIO_PATH stringByAppendingPathComponent:absoluteAudioPath];
        
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
        
        //Set the state
        self.currentAudioState = AUDIO_STATE_QUESTION;
        
        // Play Question Audio
        [[AudioPlayer sharedAudioPlayer] performSelector:@selector(playAudio) withObject:nil afterDelay:0.75];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Change request by Customer to show Reinforcement screen for 1 more second.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) callEndSectionDelegate
{
    if (delegate && [delegate respondsToSelector:@selector(didEndSection:withData:withQuestion:)])
    {
        CANCEL_CONNECTION(self.helpWebServiceHelper);
        CANCEL_CONNECTION(self.userActionWebServiceHelper);
        
        [delegate didEndSection:self.section_id withData:self.checkinData withQuestion:[self.questionsArray lastObject]];
    }
}

- (void) loadQuestionUsingJson:(NSString *)json inWebView:(UIWebView *)_webView
{
    //Covert JSON to NSDictionary
    
    if([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0)){
        json = [json stringByReplacingOccurrencesOfString:@".png" withString:@"@2x.png"];
    }
    
    NSError *error = nil;
    
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        int type=1;
        
        if(dataDic && [dataDic count]>0){
            type = [[dataDic objectForKey:@"type"] intValue];
        }
        
        //Create a request and load HTML Template in webview based on Type Parameter
        NSString *htmlFilePath = nil;
        NSURL *url = nil;
        
        htmlFilePath = [EXAM_HTML_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"template%d.html",type]];
        url = [NSURL encryptedFileURLWithPath:htmlFilePath];
        
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        html = [html stringByReplacingOccurrencesOfString:@"//$$REPLACEDATA$$//" withString:[NSString stringWithFormat:@"var jsondata = '%@';",[json stringByReplacingOccurrencesOfString:@"\n" withString:@""]]];
        [_webView loadHTMLString:html baseURL:url];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) endSection
{
    //Set the state
    self.currentAudioState = AUDIO_STATE_SECTION;
    
    //Set the current user action
    self.currentUserAction = ERB_EXAM_USER_ACTION_END_SECTION;
    
    self.view.userInteractionEnabled = NO;
    
    //Set the end section image and play the encouragement audio file based on section that ended
    switch (self.section_id)
    {
        case VERBAL_REASONING:
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"super_job.png")];
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_PRACTICAL withDelegate:self];
            break;
        case QUANTITATIVE_REASONING:
            //Set the current user action
            self.currentUserAction = ERB_EXAM_USER_ACTION_BREAK;
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"nice_work.png")];
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_NICE_WORK withDelegate:self];
            break;
        case EARLY_LITERACY:
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"great_work.png")];
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_MATH withDelegate:self];
            break;
        case MATH:
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"the_end.png")];
            [self.view addSubview:self.sectionEndImageView];
            [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
            self.currentUserAction = ERB_EXAM_USER_ACTION_END_EXAM;
            [self callReportUserActionWebServiceForAction:USER_ACTION_END_EXAM];
            return;
        default:
            break;
    }
    
    [self.view addSubview:self.sectionEndImageView];
    [[AudioPlayer sharedAudioPlayer] playAudio];
    
}

- (void)recallWebService{
    NSString *userAction = nil;
    switch (self.currentUserAction) {
        case ERB_EXAM_USER_ACTION_END_EXAM:
            userAction = USER_ACTION_END_EXAM;
            break;
        case ERB_EXAM_USER_ACTION_END_SECTION:
            userAction = USER_ACTION_END_SECTION;
            break;
        case ERB_EXAM_USER_ACTION_BREAK:
            userAction = USER_ACTION_BREAK;
            break;
        case ERB_EXAM_USER_ACTION_SUBMIT:
            userAction = USER_ACTION_SUBMIT;
            break;
        default:
            break;
    }
    
    if (userAction) {
        DLog(@"Recall in Exam View Controller for User Action: %@", userAction);
        [self callReportUserActionWebServiceForAction:userAction];
    }
    
}

- (NSString *) getCorrectAnswerFromJson:(NSDictionary *)json{
    NSArray *choices = [json objectForKey:@"choices"];
    int correct = 0;
    
    for (NSDictionary *choice in choices) {
        correct ++ ;
        if ([[choice objectForKey: @"correct"] integerValue] == 1) {
            return [NSString stringWithFormat:@"%d",correct];
        }
    }
    return @"";
}

- (void) callHelpWebService
{
    //    authToken, exam_id, student_id (mandatory)....and section_id, question_id (optional)
    if ([[ERBCommons appController] internetCheck])
    {
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN,
                         REQUEST_POST_KEY_EXAM_ID,
                         REQUEST_POST_KEY_SECTION_ID,
                         REQUEST_POST_KEY_STUDENT_ID,
                         REQUEST_POST_KEY_QUESTION_ID,
                         REQUEST_POST_KEY_ERB_QUESTION_ID, nil];
        
        ERBQuestion *question = nil;
        
        question = [self.questionsArray objectAtIndex:0];
        
        NSError *error = nil;
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[question.question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if (!error) {
            
            NSString *questionId = [json objectForKey:@"questionID"];
            
            NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken],
                               self.checkinData.examId,
                               [NSNumber numberWithInt:self.section_id],
                               self.checkinData.studentId,
                               question.questionId,
                               questionId,nil];
            
            
            NSDictionary *paramsDict = nil;
            
            @try {
                paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
            if (self.helpWebServiceHelper) {
                
                CANCEL_CONNECTION(self.helpWebServiceHelper);
                [helpWebServiceHelper release];
                helpWebServiceHelper = nil ;
            }
            
            if (paramsDict) {
                helpWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_STUDENT_HELP]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_HELP_WEB_SERVICE withHttpMethod:ERB_REQUEST_POST];
                [paramsDict release];
                self.helpWebServiceHelper.delegate = self;
                [self.helpWebServiceHelper sendWebServiceRequest];
            }
            
        }
        else{
            DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
        }
    }
}

- (void)callReportUserActionWebServiceForAction:(NSString *)userAction
{
    if ([[ERBCommons appController] internetCheck])
    {
        self.nextButton.enabled = NO;
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN,
                         REQUEST_POST_KEY_EXAM_ID,
                         REQUEST_POST_KEY_SECTION_ID,
                         REQUEST_POST_KEY_STUDENT_ID,
                         REQUEST_POST_KEY_ACTION,
                         REQUEST_POST_KEY_QUESTION_ID,
                         REQUEST_POST_KEY_ERB_QUESTION_ID,
                         REQUEST_POST_KEY_RESPONSE,
                         REQUEST_POST_KEY_DESIRED_RESPONSE,
                         REQUEST_POST_KEY_IS_RESPONSE_CORRECT,
                         REQUEST_POST_KEY_SECONDS_ELAPSED,
                         REQUEST_POST_KEY_SECONDS_PAUSED,
                         REQUEST_POST_KEY_HELP_FLAG,
                         REQUEST_POST_KEY_PAUSE_FLAG, nil];
        
        NSArray *values = nil;
        
        // Check the current user action and accordingly set the values array
        
        if ([userAction isEqualToString:USER_ACTION_SUBMIT] || [userAction isEqualToString:USER_ACTION_BREAK] || [userAction isEqualToString:USER_ACTION_END_SECTION] || [userAction isEqualToString:USER_ACTION_END_EXAM])
        {
            // Send question ID of the question which is being submitted
            ERBQuestion *question = [self.questionsArray objectAtIndex:0];
            for (ERBQuestion *q in self.questionsArray)
            {
                DLog(@"The Question id in question array is: %@", q.questionId);
            }
            
            NSAssert(question, @"question is nil");
            NSError *error = nil;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[question.question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
            NSAssert(json, @"json is nil");
            NSString *correctResponse =[self getCorrectAnswerFromJson:json];
            NSAssert(correctResponse, @"correct response is nil");
            
            if (!error) {
                
                ERBBeginTutorialViewController *_delegate = (ERBBeginTutorialViewController *)self.delegate;
                
                values = [NSArray arrayWithObjects:[ERBCommons authToken],
                          self.checkinData.examId,
                          [NSNumber numberWithInt:self.section_id],
                          self.checkinData.studentId,
                          userAction,
                          question.questionId,
                          [json objectForKey:@"questionID"],
                          self.questionResponse,
                          correctResponse,
                          ([self.questionResponse isEqualToString:correctResponse] ? @"yes" : @"no"),
                          self.secondsElapsed,
                          [ERBCommons secondsBetweenStartInterval:_delegate.pauseStartTime andEndInterval:_delegate.pauseEndTime],
                          (self.isHelpNeeded ? @"true" : @"false"),
                          (_delegate.pauseStartTime > 0.0 ? @"true" : @"false"),nil];
                
            }
            else{
                DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
            }
            NSDictionary *paramsDict = nil;
            
            @try {
                paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
            if (self.userActionWebServiceHelper) {
                CANCEL_CONNECTION(self.userActionWebServiceHelper);
                [userActionWebServiceHelper release];
                userActionWebServiceHelper = nil;
            }
            
            if (paramsDict) {
                userActionWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_RECORD_USER_ACTION]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_REPORT_USER_EXAM_ACTION withHttpMethod:ERB_REQUEST_POST];
                [paramsDict release];
                userActionWebServiceHelper.delegate = self;
                [userActionWebServiceHelper sendWebServiceRequest];
            }
        }
    }
    else
    {
        [self performSelector:@selector(recallWebService) withObject:nil afterDelay:DELAY];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId   operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    
    //Check request Id
    if ([requestId isEqualToString:REQUEST_ID_REPORT_USER_EXAM_ACTION])
    {
        //Reset the isHelpNeeded to NO
        self.isHelpNeeded = NO;
        NSError *error = nil;
        NSDictionary *userActionResponse = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        if (!error) {
            if (self.currentUserAction == ERB_EXAM_USER_ACTION_END_EXAM)
            {
                if ([[userActionResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
                {
                    if (delegate && [delegate respondsToSelector:@selector(didEndSection:withData:withQuestion:)])
                    {
                        CANCEL_CONNECTION(self.helpWebServiceHelper);
                        CANCEL_CONNECTION(self.userActionWebServiceHelper);
                        
                        // Clear the cached state data so that exam doesn't resume
                        [USER_DEFAULTS removeObjectForKey:KEY_EXAM_STATE];[USER_DEFAULTS synchronize];
                        //Call the end section delegate
                        [delegate didEndSection:self.section_id withData:self.checkinData withQuestion:[self.questionsArray lastObject]];
                    }
                }
                else{
                    [self recallWebService];
                }
            }
            else if (self.currentUserAction == ERB_EXAM_USER_ACTION_END_SECTION || self.currentUserAction == ERB_EXAM_USER_ACTION_BREAK)
            {
                if ([[userActionResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
                {
                    //Remove the previous question
                    [self.questionsArray removeObjectAtIndex:0];
                    //Save the current exam state
                    [self saveExamState];
                    
                    //Change Request, Customer asked to show the end exam image view for 1 more second
                    [self performSelector:@selector(callEndSectionDelegate) withObject:nil afterDelay:DELAY];
                }
                else{
                    [self recallWebService];
                }
            }
            else
            {
                if ([[userActionResponse allKeys] containsObject:RESPONSE_KEY_QUESTION])
                {
                    if ([[userActionResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
                    {
                        ERBBeginTutorialViewController *_delegate = (ERBBeginTutorialViewController *)self.delegate;
                        _delegate.pauseStartTime = 0.0;
                        _delegate.pauseEndTime   = 0.0;
                        
                        //Remove the previous question
                        [self.questionsArray removeObjectAtIndex:0];
                        
                        //Add new Question to question aaray
                        ERBQuestion *questionToSave = [[ERBQuestion alloc] init];
                        questionToSave.question = [userActionResponse objectForKey:RESPONSE_KEY_QUESTION];
                        questionToSave.questionId = [userActionResponse objectForKey:REQUEST_POST_KEY_QUESTION_ID];
                        [self.questionsArray addObject:questionToSave];
                        [questionToSave release];
                        
                        // Increment the question count
                        self.questionsCount++;
                        
                        //Set the didReceiveNextQuestionResponse to YES
                        self.didReceiveNextQuestionResponse = YES;
                        
                        //Save the current exam state
                        [self saveExamState];
                        
                        //We enable the next and replay buttons only when response for submit web service is received and the audio is played once.
                        if (self.didPlayQuestionAudioOnce)
                        {
                            self.nextButton.enabled = YES;
                            self.replayButton.enabled = YES;
                            [self enableAnswerChoices];
                        }
                        
                        ERBQuestion *nextQuestionToLoad = [self.questionsArray objectAtIndex:1];
                        if (self.webView.hidden)
                        {
                            [self loadQuestionUsingJson:nextQuestionToLoad.question inWebView:self.webView];
                        }
                        else
                        {
                            [self loadQuestionUsingJson:nextQuestionToLoad.question inWebView:self.webViewAlternate];
                        }
                    }
                    else
                    {
                        if (self.currentUserAction == ERB_EXAM_USER_ACTION_NONE) {
                            self.currentUserAction = ERB_EXAM_USER_ACTION_SUBMIT;
                        }
                        
                        [self recallWebService];
                    }
                }
            }
        }
        else{
            DLog(@"Error while parsing JSON response for WS :%@", requestId);
        }
    }
    else if([requestId isEqualToString:REQUEST_ID_HELP_WEB_SERVICE]){
        if (!self.didReceiveNextQuestionResponse) {
            self.currentUserAction = ERB_EXAM_USER_ACTION_SUBMIT;
            [self recallWebService];
            
        }
        
    }
}


- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if ([requestId isEqualToString:REQUEST_ID_REPORT_USER_EXAM_ACTION])
    {
        if (self.currentUserAction == ERB_EXAM_USER_ACTION_NONE) {
            self.currentUserAction = ERB_EXAM_USER_ACTION_SUBMIT;
        }
        
        [self recallWebService];
    }
    else if([requestId isEqualToString:REQUEST_ID_HELP_WEB_SERVICE]){
        [self callHelpWebService];
    }
}

#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    if (self.currentUserAction == ERB_EXAM_USER_ACTION_HELP)
    {
        if (!self.didPlayQuestionAudioOnce)
        {
            self.currentUserAction = ERB_EXAM_USER_ACTION_NONE;
            [self playAudioButtonPressed:nil];
        }
        else
        {
            if (self.didReceiveNextQuestionResponse) {
                self.nextButton.enabled = YES;
                self.replayButton.enabled = YES;
                
                //Enable answer choices
                [self enableAnswerChoices];
            }
        }
    }
    else
    {
        switch (self.currentAudioState)
        {
            case AUDIO_STATE_QUESTION:
            {
                //Set the didPlayQuestionAudioOnce to YES
                self.didPlayQuestionAudioOnce = YES;
                
                if(self.didReceiveNextQuestionResponse)
                {
                    // Enable Next and Replay Audio Button
                    self.nextButton.enabled = YES;
                    self.replayButton.enabled = YES;
                    //Enable choices
                    [self enableAnswerChoices];
                }
                
                break;
            }
            case AUDIO_STATE_SECTION:
            {
                if (self.section_id == QUANTITATIVE_REASONING) {
                    [self callReportUserActionWebServiceForAction:USER_ACTION_BREAK];
                }
                else if (self.section_id == VERBAL_REASONING || self.section_id == EARLY_LITERACY)
                {
                    [self callReportUserActionWebServiceForAction:USER_ACTION_END_SECTION];
                }
                break;
            }
            default:
                break;
        }
        
        if (self.didReceiveNextQuestionResponse && self.didPlayQuestionAudioOnce) {
            self.nextButton.enabled = YES;
            self.replayButton.enabled = YES;
            
            //Enable answer choices
            [self enableAnswerChoices];
        }
        
        self.currentAudioState = AUDIO_STATE_NONE;
    }
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - UIScrollViewDelegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BOOL isHelpMode = [USER_DEFAULTS boolForKey:RESPONSE_KEY_HELP_HAND];
    //Set the help mode
    if (!isHelpMode) {
        self.helpButton.hidden = YES;
    }
    
    self.view.userInteractionEnabled = YES;
    
    // Reset the pause values
    ERBBeginTutorialViewController *_delegate = (ERBBeginTutorialViewController *)self.delegate;
    _delegate.pauseStartTime = 0.0;
    _delegate.pauseEndTime   = 0.0;
    
    // Add self as observer for pause end notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playAudio) name:NOTIFICATION_GLOBAL_PAUSE_ENDED object:nil];
    
    //Set the didPlayQuestionAudioOnce flag to NO initially. Will set to YES when audio plays for the first time
    self.didPlayQuestionAudioOnce = NO;
    
    //We set it to YES initially coz we already have the ressponse for the 1sr question.
    self.didReceiveNextQuestionResponse = YES;
    
    
    // Initialize question response to 0;
    self.questionResponse = @"0";
    
    UIScrollView *scrollView = self.webView.scrollView;
    scrollView.delegate = self;
    scrollView.scrollEnabled = NO;
    scrollView.bounces = NO;
    
    
    UIScrollView *alternateScrollView = self.webViewAlternate.scrollView;
    alternateScrollView.delegate = self;
    alternateScrollView.scrollEnabled = NO;
    alternateScrollView.bounces = NO;
    
    self.isHelpNeeded = NO;
    
    ERBQuestion *nextQuestionToLoad = [self.questionsArray objectAtIndex:1];
    [self loadQuestionUsingJson:nextQuestionToLoad.question inWebView:self.webViewAlternate];
    
    ERBQuestion *currentQuestionToLoad = [self.questionsArray objectAtIndex:0];
    [self loadQuestionUsingJson:currentQuestionToLoad.question inWebView:self.webView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SAFE_RELEASE(userActionWebServiceHelper);
    SAFE_RELEASE(helpWebServiceHelper);
    [checkinData release];
    [webView release];
    [activityIndicator release];
    [sectionEndImageView release];
    [questionResponse release];
    [questionsArray release];
    [nextButton release];
    [webViewAlternate release];
    [replayButton release];
    [_helpButton release];
    [super dealloc];
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setCheckinData:nil];
    [self setWebView:nil];
    [self setActivityIndicator:nil];
    [self setSectionEndImageView:nil];
    [self setQuestionResponse:nil];
    [self setNextButton:nil];
    [self setQuestionsArray:nil];
    [self setWebViewAlternate:nil];
    [self setReplayButton:nil];
    [self setHelpButton:nil];
    [super viewDidUnload];
}

@end
