//
//  ERBDeviceSetupViewController.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>

@interface ERBDeviceSetupViewController : UIViewController
- (IBAction)closeButtonPressed:(id)sender;
@end
