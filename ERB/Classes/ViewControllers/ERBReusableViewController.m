//
//  ERBReusableViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBReusableViewController.h"
#import "AudioPlayer.h"

@interface ERBReusableViewController ()
- (void) endExam;
@end

@implementation ERBReusableViewController

@synthesize examState, delegate, reusableImageView, studentNameLabel, studentName;

#pragma mark Life cyscle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - private methods
-(void)endExam{
    if (delegate && [delegate respondsToSelector:@selector(examDidEnd:)])
    {
        [delegate examDidEnd:YES];
    }
}


#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    if (self.examState == ERB_EXAM_STATE_END) {
        [self performSelector:@selector(endExam) withObject:nil afterDelay:END_EXAM_DELAY];
    }
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Set the image to be displayed and audio file to be played
    self.reusableImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"please_wait.png")];
    
    // Unhide the student name label and set the student's name
    self.studentNameLabel.text = self.studentName;
    self.studentNameLabel.hidden = NO;
    
    if (self.examState == ERB_EXAM_STATE_PAUSE) {
        
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_PAUSE withDelegate:self];
    }
    else if (self.examState == ERB_EXAM_STATE_END)
    {
        self.studentNameLabel.hidden = YES;
        self.reusableImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"the_end.png")];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_THE_END withDelegate:self];
        
    }
    else if (self.examState == ERB_EXAM_STATE_BEGIN_SECTION_1)
    {
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_PAUSE_SCREEN_PRE_EXAM_START withDelegate:self];
    }
    else if (self.examState == ERB_EXAM_STATE_PRE_TUTORIAL_PAUSE)
    {
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:nil withDelegate:nil];
        return;
    }
    
    // Play the audio
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0, 0, 1024, 768);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [studentName release];
    [reusableImageView release];
    [studentNameLabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setStudentName:nil];
    [self setReusableImageView:nil];
    [self setStudentNameLabel:nil];
    [super viewDidUnload];
}
@end
