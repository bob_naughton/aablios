//
//  ERBBeginSectionViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBWebOperation.h"
#import "ERBQuestion.h"
#import "ERBCheckIn.h"
#import <AVFoundation/AVFoundation.h>

@protocol ERBBeginSectionViewControllerDelegate <NSObject>
- (void) section:(int)section didBegin:(BOOL)didBegin withCheckinData:(ERBCheckIn *)data withQuestion: (ERBQuestion *)question;
@end

@interface ERBBeginSectionViewController : UIViewController<ERBWebServiceDelegate, AVAudioPlayerDelegate>
@property (retain, nonatomic) IBOutlet UILabel *sectionNameLabel;
@property (retain, nonatomic) ERBCheckIn *checkinData;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, assign) id <ERBBeginSectionViewControllerDelegate> delegate;
@property (nonatomic, assign) int sectionId;
@property (retain, nonatomic) IBOutlet UILabel *arrowLabel;
@property (retain, nonatomic) IBOutlet UIImageView *examThemeImage;
@property (nonatomic, retain) ERBQuestion *previousQuestion;
@property (nonatomic, assign) ERB_USER_ACTION currentUserAction;
@property (retain, nonatomic) IBOutlet UILabel *readyToBeginLabel;
@property (retain, nonatomic) IBOutlet UIButton *beginSectionButton;

- (IBAction)beginExam:(id)sender;
- (void) disableUserInteraction;
- (void) enableUserInteraction;

@end
