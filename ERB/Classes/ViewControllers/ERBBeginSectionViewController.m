//
//  ERBBeginSectionViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBBeginSectionViewController.h"
#import "AudioPlayer.h"

@interface ERBBeginSectionViewController ()
- (void) callReportUserActionWebService;
- (void) customizeUI;
- (void) playAudio;
@property (nonatomic, retain) ERBWebOperation *webServiceHelper;
@end

@implementation ERBBeginSectionViewController

@synthesize checkinData, activityIndicator, delegate, sectionNameLabel, sectionId, arrowLabel, examThemeImage, readyToBeginLabel, beginSectionButton;
@synthesize previousQuestion;
@synthesize currentUserAction;
@synthesize webServiceHelper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Life Cycle Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Add self as observer for pause end notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playAudio) name:NOTIFICATION_GLOBAL_PAUSE_ENDED object:nil];
    
    [self customizeUI];
    // Do any additional setup after loading the view from its nib.
    self.sectionNameLabel.text = [ERBCommons getSectionTitleForSectionId:sectionId];
    
    //Set the arrow labels accordingly
    if (self.sectionId == VERBAL_REASONING)
    {
        self.arrowLabel.text = @"Begin";
    }
    else
    {
        self.arrowLabel.text = @"Next Section";
    }
    
    //Set the background image according to the section
    NSString *imageName = [NSString stringWithFormat:@"intro000%d.png", self.sectionId];
    self.examThemeImage.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(imageName)];
    
    //Configure the audio player to play file based on current Section Id
    [self playAudio];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0, 0, 1024, 768);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SAFE_RELEASE(webServiceHelper);
    [checkinData release];
    [sectionNameLabel release];
    [activityIndicator release];
    [arrowLabel release];
    [examThemeImage release];
    SAFE_RELEASE(previousQuestion);
    [readyToBeginLabel release];
    [beginSectionButton release];
    [super dealloc];
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setCheckinData:nil];
    [self setSectionNameLabel:nil];
    [self setActivityIndicator:nil];
    [self setArrowLabel:nil];
    [self setExamThemeImage:nil];
    [self setReadyToBeginLabel:nil];
    [self setBeginSectionButton:nil];
    [super viewDidUnload];
}

#pragma mark - Action Methods
- (IBAction)beginExam:(id)sender{
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
    {
        [self callReportUserActionWebService];
    }
    else if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_OFFLINE])
    {
        if (delegate && [delegate respondsToSelector:@selector(section:didBegin:withCheckinData:withQuestion:)])
        {
            [self disableUserInteraction];
            [delegate section:self.sectionId didBegin:YES withCheckinData:self.checkinData withQuestion:nil];
        }
    }
}

#pragma mark - Private Methods
- (void) disableUserInteraction
{
    self.view.userInteractionEnabled = NO;
    self.beginSectionButton.enabled = NO;
    [self.activityIndicator startAnimating];
}

- (void) enableUserInteraction
{
    self.view.userInteractionEnabled = YES;
    self.beginSectionButton.enabled = YES;
    [self.activityIndicator stopAnimating];
}

- (void) customizeUI
{
    self.sectionNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    self.readyToBeginLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:55.0];
    self.arrowLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
}

- (void) playAudio{
    
    switch (self.sectionId)
    {
        case VERBAL_REASONING:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_FIRST_SUB_SECTION_INTRO_ANALYTICAL withDelegate:self];
            break;
        case QUANTITATIVE_REASONING:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_PRACTICAL withDelegate:self];
            break;
        case EARLY_LITERACY:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_EARLY_LIT withDelegate:self];
            break;
        case MATH:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_MATH withDelegate:self];
            break;
            
        default:
            break;
    }
    
    
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void)callReportUserActionWebService
{
    if ([[ERBCommons appController] internetCheck])
    {
        NSString *userAction = @"";
        
        //For the first time we send the EXAM_START as userAction
        if (self.sectionId == VERBAL_REASONING) {
            userAction = USER_ACTION_START_EXAM;
        }
        else{
            userAction = USER_ACTION_START_SECTION;
        }
        
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN,
                         REQUEST_POST_KEY_EXAM_ID,
                         REQUEST_POST_KEY_SECTION_ID,
                         REQUEST_POST_KEY_STUDENT_ID,
                         REQUEST_POST_KEY_ACTION,
                         REQUEST_POST_KEY_QUESTION_ID,
                         REQUEST_POST_KEY_RESPONSE,  nil];
        
        NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken],
                           self.checkinData.examId,
                           [NSNumber numberWithInt:self.sectionId],
                           self.checkinData.studentId,
                           userAction,
                           self.previousQuestion.questionId,
                           @"",nil];
        
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if (self.webServiceHelper) {
            
            CANCEL_CONNECTION(self.webServiceHelper);
            
            [webServiceHelper release];
            webServiceHelper = nil;
        }
        
        if (paramsDict) {
            webServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_RECORD_USER_ACTION]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_REPORT_USER_SECTION_ACTION withHttpMethod:ERB_REQUEST_POST];
            [paramsDict release];
            self.webServiceHelper.delegate = self;
            [self.webServiceHelper sendWebServiceRequest];
            
            [self.view setUserInteractionEnabled:NO];
            [self.activityIndicator startAnimating];
        }
        
    }
    else
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    //Check request Id
    if ([requestId isEqualToString:REQUEST_ID_REPORT_USER_SECTION_ACTION])
    {
        NSError *error = nil;
        NSDictionary *userActionResponse = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        if (!error) {
            if ([[userActionResponse objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                if (delegate && [delegate respondsToSelector:@selector(section:didBegin:withCheckinData:withQuestion:)])
                {
                    CANCEL_CONNECTION(self.webServiceHelper);
                    
                    ERBQuestion *questionToSave = [[ERBQuestion alloc] init];
                    questionToSave.question = [userActionResponse objectForKey:RESPONSE_KEY_QUESTION];
                    questionToSave.questionId = [userActionResponse objectForKey:REQUEST_POST_KEY_QUESTION_ID];
                    [delegate section:self.sectionId didBegin:YES withCheckinData:self.checkinData withQuestion:questionToSave];
                    [questionToSave release];
                }
            }
            else
            {
                [ERBCommons showAlert:[userActionResponse  objectForKey:RESPONSE_KEY_ERROR_MESSAGE] delegate:nil];
            }
        }
        else{
            DLog(@"Error while parsing JSON response for WS: %@", requestId);
        }
    }
}

- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if ([requestId isEqualToString:REQUEST_ID_REPORT_USER_SECTION_ACTION])
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
    [self.view setUserInteractionEnabled:YES];
    [self.activityIndicator stopAnimating];
}

#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    self.beginSectionButton.enabled = YES;
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];;
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

@end
