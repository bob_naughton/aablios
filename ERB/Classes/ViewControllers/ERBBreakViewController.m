//
//  ERBBreakViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBBreakViewController.h"
#import "ERBWebServiceConstants.h"
#import "AudioPlayer.h"
#import "AudioVolumeViewController.h"

@interface ERBBreakViewController ()
- (void) startTimer;
- (void) setTimerValue;
- (void) customizeUI;
//@property (nonatomic, retain) AudioVolumeViewController *volumeViewController;

@end

@implementation ERBBreakViewController
@synthesize breakTimeLabel, delegate;
@synthesize previousQuestion;
@synthesize studentName, takeBreakLabel, studentNameLabel;
//@synthesize volumeViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

#pragma mark - Private Methods
- (void) customizeUI
{
    self.takeBreakLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:124.5];
    self.studentNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    
    //Set the student name
    self.studentNameLabel.text = self.studentName;
    
    //    SAFE_RELEASE(volumeViewController);
    //    volumeViewController = [[AudioVolumeViewController alloc] init];
    //    volumeViewController.view.frame = CGRectMake(362, 89, 300, 127);
    //
    //    [self.view addSubview:volumeViewController.view];
}

- (void) startTimer{
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setTimerValue) userInfo:nil repeats:YES];
}

- (void) setTimerValue
{
    NSArray *timeArray = [breakTimeLabel.text componentsSeparatedByString:@":"];
    
    int timeElapsed = [[timeArray objectAtIndex:0] intValue]*60.0 + [[timeArray objectAtIndex:1] intValue];
    
    timeElapsed ++;
    
    int minutes = timeElapsed/60;
    int seconds = timeElapsed%60;
    
    if (breakTimeLabel.isHidden) {
        breakTimeLabel.hidden = NO;
    }
    
    breakTimeLabel.text = [NSString stringWithFormat:@"%.2d:%.2d",minutes, seconds];
    
}

- (void) stopTimer{
    [timer invalidate];
    timer = nil;
    
    if (delegate && [delegate respondsToSelector:@selector(didEndBreak: withQuestion:)]) {
        [delegate didEndBreak:YES withQuestion:self.previousQuestion];
    }
}

#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customizeUI];
    
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
    {
        [self startTimer];
    }
    
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SECTION_BREAK withDelegate:self];
    
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [timer invalidate];
    timer = nil;
    [breakTimeLabel release];
    SAFE_RELEASE(previousQuestion);
    [studentName release];
    [studentNameLabel release];
    [takeBreakLabel release];
    //    SAFE_RELEASE(volumeViewController);
    [super dealloc];
}
- (void)viewDidUnload
{
    [self setStudentName:nil];
    [self setBreakTimeLabel:nil];
    [self setStudentNameLabel:nil];
    [self setTakeBreakLabel:nil];
    [super viewDidUnload];
}
@end
