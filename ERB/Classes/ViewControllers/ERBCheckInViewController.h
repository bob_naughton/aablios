//
//  ERBCheckInViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBCheckInCell.h"
#import "ERBWebOperation.h"
#import "ERBCheckIn.h"
#import "ERBExamData.h"

@protocol ERBCheckInViewControllerDelegate <NSObject>
- (void) adminDidLogout: (BOOL) didLogout;
- (void) student:(NSString *)student didCheckIn: (BOOL) didCheckIn withData:(ERBCheckIn *)theCheckInData  withQuestions: (NSArray *)allQuestions;
- (void) didPressBackFromCheckIn;
@end

@interface ERBCheckInViewController : UIViewController <ERBCheckInCellDelegate,UIAlertViewDelegate,ERBWebServiceDelegate>{
    NSTimer *timer;
}

@property (retain, nonatomic) IBOutlet UITableView *checkinTableView;
@property (nonatomic, assign) id <ERBCheckInViewControllerDelegate> delegate;
@property (nonatomic, retain) NSArray *checkInDataArray;
@property (nonatomic, retain) NSString *examId;
@property (nonatomic, retain) NSString *userName;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) NSDictionary *checkinDictionary;
@property (nonatomic, retain) ERBCheckIn *checkinData;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;
@property (retain, nonatomic) IBOutlet UILabel *timeLabel;
@property (retain, nonatomic) IBOutlet UILabel *userNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *schoolNameLabel;

@property (retain, nonatomic) IBOutlet UILabel *verifyLabel;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (retain, nonatomic) IBOutlet UILabel *genderLabel;
@property (retain, nonatomic) IBOutlet UILabel *dobLabel;
@property (retain, nonatomic) IBOutlet UILabel *studentCheckInLabel;
@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UIButton *logoutButton;
@property (nonatomic, retain) NSString *examMode;

@property (retain, nonatomic) ERBExamData *examData;

- (IBAction)logoutButtonPressed:(id)sender;
- (IBAction)backButtonPressed:(id)sender;
- (void) callWebServiceToRefreshData;

@end
