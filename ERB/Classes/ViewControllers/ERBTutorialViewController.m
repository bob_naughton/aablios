//
//  ERBTutorialViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBTutorialViewController.h"
#import "AudioPlayer.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ERBTutorialViewController ()
- (void) callWebServiceForTutorialEvent;

@property (nonatomic, retain) ERBWebOperation *webServiceHelper;
@property (nonatomic, retain) MPMoviePlayerController *moviePlayerController;
@property (nonatomic, retain) NSArray *dynamicTutorialHTMLNames;
@property (nonatomic, retain) NSArray *dynamicTutorialVideoNames;
@property (nonatomic, assign) ERB_CURRENT_TUTORIAL_USER_ACTION currentUserAction;
@property (nonatomic, assign) BOOL isOmitQuestionAudioPlayed;
@property (nonatomic , assign) BOOL isGlobalPauseOn;
@property (nonatomic, assign) BOOL isExamStopped;

- (void) configureMoviePlayerWithMovieName: (NSString *) movieName ofType: (NSString *)type;
- (void) playPreQuestionAudio;
- (void) playQuestionAudio;
- (void) playPostQuestionAudio;
- (void) disableAllButtons;
- (void) enableAllButtons;
- (void) notificationReceived:(NSNotification *)notification;
- (void) pauseVideo;
- (void) playVideo;
- (void) playAnswerAudioWithAnswerID: (NSString *)answerId;
-(void) displayTutorialQuestion;

@end

@implementation ERBTutorialViewController
@synthesize delegate;
@synthesize tutorialImageView;
@synthesize checkInData;
@synthesize currentTutorialEvent;
@synthesize webServiceHelper;
@synthesize isGlobalPauseOn, isExamStopped;
@synthesize moviePlayerController;
@synthesize dynamicTutorialHTMLNames;
@synthesize dynamicTutorialVideoNames;
@synthesize currentUserAction;
@synthesize isOmitQuestionAudioPlayed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    [NSArray arrayWithObjects:@"", nil];
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - UIWebViewDelegate

-(BOOL) webView:(UIWebView *)_webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [[request URL] absoluteString];
    
    if ([requestString hasPrefix:@"js-frame:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        
        NSString *function = (NSString*)[components objectAtIndex:1];
        NSString *answerId = [components objectAtIndex:2];
        
        if ([function isEqualToString:@"playAnswerAudio"])
        {
            [self playAnswerAudioWithAnswerID:answerId];
        }
        return NO;
    }
    return YES;
}

-(void) webViewDidFinishLoad:(UIWebView *)_webView
{
    [self performSelector:@selector(displayTutorialQuestion) withObject:Nil afterDelay:0.5];
}

#pragma mark - Private Methods

- (void) notificationReceived:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:NOTIFICATION_GLOBAL_PAUSE_STARTED]) {
        
        //Pause the tutorial video
        [self pauseVideo];
        
        // Set Tutorial View Controller's isGlobalPauseOn variable as YES
        self.isGlobalPauseOn = YES;
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_GLOBAL_PAUSE_ENDED]) {
        
        // Set Tutorial View Controller's isGlobalPauseOn variable as NO
        self.isGlobalPauseOn = NO;
        
        if (self.currentTutorialEvent <= TUTORIAL_EVENT_NEXT) {
            [self adjustGUI];
        }
        else{
            
            // If movie player's view is present as subview then play the video else load the practice question
            if ([self.view.subviews containsObject:self.moviePlayerController.view]) {
                [self playVideo];
            }
            else if ([self.view.subviews containsObject:self.dynamicTutorialView]) {
                [self playQuestionAudio];
            }
            else if ([self.view.subviews containsObject:self.beginPracticeView] && !self.beginPracticeButton.isEnabled) {
                
                self.currentUserAction = TUTORIAL_USER_ACTION_BEGIN_PRACTICE;
                [[AudioPlayer sharedAudioPlayer]  resetAudioPlayer];
                [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_BEGIN_PRACTICE withDelegate:self];
                [[AudioPlayer sharedAudioPlayer] playAudio];
            }
        }
        
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_EXAM_STOPPED]) {
        self.isExamStopped = YES;
        [self.moviePlayerController stop];
    }
}

- (void) disableAllButtons
{
#ifdef FAST_FORWARD
    self.helpButton.enabled = NO;
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#else
    self.helpButton.enabled = NO;
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#endif
}

- (void) enableAllButtons
{
    self.helpButton.enabled = YES;
    self.nextButton.enabled = YES;
    self.replayButton.enabled = YES;
}

- (void) playPostQuestionAudio{
    // Play the try yourself audio
    self.currentUserAction = TUTORIAL_USER_ACTION_POST_QUESTION_AUDIO;
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_POST_QUESTION_ONE_AUDIO withDelegate:self];
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void) playPreQuestionAudio{
    // Play the try yourself audio
    self.currentUserAction = TUTORIAL_USER_ACTION_PRE_QUESTION_AUDIO;
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_TRY_QUESTION withDelegate:self];
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void) playQuestionAudio
{
    self.currentUserAction = TUTORIAL_USER_ACTION_QUESTION_AUDIO;
    
    NSString *jsonPath = [[TUTORIAL_JSON_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"json"];
    NSString *json = [NSString stringWithContentsOfURL:[NSURL encryptedFileURLWithPath:jsonPath] encoding:NSUTF8StringEncoding error:nil];
    
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        //Fix for iOS 7
        NSString *questionAudioPath = [dataDic objectForKey:@"questionAudio"];
        NSString *absoluteAudioPath = [TUTORIAL_AUDIO_PATH stringByAppendingPathComponent:[questionAudioPath lastPathComponent]];
        
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] performSelector:@selector(playAudio) withObject:nil afterDelay:0.75];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) playAnswerAudioWithAnswerID: (NSString *)answerId
{
    
    NSString *jsonPath = [[TUTORIAL_JSON_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"json"];
    NSString *json = [NSString stringWithContentsOfURL:[NSURL encryptedFileURLWithPath:jsonPath] encoding:NSUTF8StringEncoding error:nil];
    
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSArray *answerChoicesArray = [dataDic objectForKey:@"choices"];
        
        for (NSDictionary *dict in answerChoicesArray)
        {
            NSString *tempAnswerId = [dict objectForKey:@"answerID"];
            if ([tempAnswerId isEqualToString: answerId])
            {
                NSString *answerAudioPath = [dict objectForKey:@"answerAudio"];
                NSString *absoluteAudioPath = [TUTORIAL_AUDIO_PATH stringByAppendingPathComponent:[answerAudioPath lastPathComponent]];
                
                [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
                //Set the state
                self.currentUserAction = TUTORIAL_USER_ACTION_ANSWER_AUDIO;
                [[AudioPlayer sharedAudioPlayer] playAudio];
                break;
            }
        }
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) enableAnswerChoices
{
    //Enable choices
    [self.dynamicQuestionWebView stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
}

- (void) customizeUI
{
    self.tutorialLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:45.0];
    self.readyToBeginLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    self.beginPracticeLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
}

- (void) adjustGUI{
    
    // Do not perform any activity if global pause is on
    if (self.isGlobalPauseOn) {
        return;
    }
    
    if (self.isExamStopped) {
        return;
    }
    
    self.currentUserAction = TUTORIAL_USER_ACTION_SLIDE_AUDIO;
    
    //Set the image for tutorial image view based on the tag of previously tapped button according to the help mode
    BOOL isHelpMode = [USER_DEFAULTS boolForKey:RESPONSE_KEY_HELP_HAND];
    
    NSString *imageName = nil;
    if (isHelpMode)
    {
        imageName = [NSString stringWithFormat:@"slide0%d.png",self.currentTutorialEvent];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"slide0%d_without_help.png",self.currentTutorialEvent];
    }
    
    UIImage *tutorialImage = [UIImage imageWithContentsOfFile:IMAGE_PATH(imageName)];
    [self.tutorialImageView setImage:tutorialImage];
    
    //Configure the audio player to play file based on current tutorial scene
    switch (self.currentTutorialEvent+1) {
        case TUTORIAL_EVENT_START:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_INTRO withDelegate:self];
            break;
        case TUTORIAL_EVENT_HELP:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_HELP_BUTTON withDelegate:self];
            break;
        case TUTORIAL_EVENT_REPLAY:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_REPLAY_AUDIO_BUTTON withDelegate:self];
            break;
        case TUTORIAL_EVENT_NEXT:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_NEXT_BUTTON withDelegate:self];
            break;
        default:
            break;
    }
    if (self.currentTutorialEvent != TUTORIAL_EVENT_NEXT)
    {
        [[AudioPlayer sharedAudioPlayer] playAudio];
    }
}


- (void) configureMoviePlayerWithMovieName: (NSString *) movieName ofType: (NSString *)type
{
    if (moviePlayerController) {
        SAFE_RELEASE(moviePlayerController);
    }
    moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:[[TUTORIAL_VIDEO_PATH stringByAppendingPathComponent:movieName] stringByAppendingPathExtension:type]]];
    ;
    
    self.moviePlayerController.controlStyle = MPMovieControlStyleNone;
    self.moviePlayerController.view.frame = self.view.frame;
    self.moviePlayerController.currentPlaybackTime = 1.0;
    self.moviePlayerController.backgroundView.backgroundColor = [UIColor clearColor];
    self.moviePlayerController.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.moviePlayerController.view];
    [self.moviePlayerController prepareToPlay];
    [self.moviePlayerController play];
}

- (void) loadHtml
{
    //Covert JSON to NSDictionary
    
    NSString *jsonPath = [[TUTORIAL_JSON_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"json"];
    NSString *json = [NSString stringWithContentsOfURL:[NSURL encryptedFileURLWithPath:jsonPath] encoding:NSUTF8StringEncoding error:nil];
    
    if([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0)){
        json = [json stringByReplacingOccurrencesOfString:@".png" withString:@"@2x.png"];
    }
    
    //Create a request and load HTML Template in webview based on Type Parameter
    NSString *htmlFilePath = nil;
    NSURL *url = nil;
    
    htmlFilePath = [[TUTORIAL_HTML_PATH stringByAppendingPathComponent:[self.dynamicTutorialHTMLNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1]] stringByAppendingPathExtension:@"html"];
    
    url = [NSURL encryptedFileURLWithPath:htmlFilePath];
    
    NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    html = [html stringByReplacingOccurrencesOfString:@"//$$REPLACEDATA$$//" withString:[NSString stringWithFormat:@"var jsondata = '%@';",[json stringByReplacingOccurrencesOfString:@"\n" withString:@""]]];
    
    [self.dynamicQuestionWebView loadHTMLString:html baseURL:url];
    
    [self disableAllButtons];
}

-(void) displayTutorialQuestion {
    [self.moviePlayerController.view removeFromSuperview];
    SAFE_RELEASE(moviePlayerController);
    self.dynamicTutorialView.hidden = NO;
}

#pragma mark - MPMoviePlayerController Notification methods
- (void) playbackFinished : (NSNotification *) notification
{
    
    if (self.isExamStopped) {
        return;
    }
    
    [self loadHtml];
    
    [self.view addSubview:self.dynamicTutorialView];
    self.dynamicTutorialView.hidden = YES;
    // Do not perform any activity if global pause is on
    if (self.isGlobalPauseOn) {
        return;
    }
    
    if (self.isExamStopped) {
        return;
    }
    
    [self playQuestionAudio];
    
    //    [self playPreQuestionAudio];
}

-(void)pauseVideo{
    if (moviePlayerController && self.moviePlayerController.playbackState == MPMoviePlaybackStatePlaying) {
        [self.moviePlayerController pause];
    }
}

-(void)playVideo{
    if (self.moviePlayerController) {
        [self.moviePlayerController play];
    }
}

#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    // Do not perform any activity if global pause is on
    if (self.isGlobalPauseOn) {
        return;
    }
    
    if (self.isExamStopped) {
        return;
    }
    
    if (self.currentUserAction == TUTORIAL_USER_ACTION_QUESTION_AUDIO)
    {
        //We play post question audio only for the first question.
        if ((self.currentTutorialEvent - TUTORIAL_DYNAMIC_1) == 0)
        {
            [self enableAllButtons];
            [self enableAnswerChoices];
        }
        else
        {
            //Enable all buttons
            [self enableAllButtons];
            [self enableAnswerChoices];
        }
    }
    else if (self.currentUserAction == TUTORIAL_USER_ACTION_BEGIN_PRACTICE)
    {
        self.beginPracticeButton.enabled = YES;
    }
    else if (self.currentUserAction ==  TUTORIAL_USER_ACTION_SLIDE_AUDIO){
        
        self.currentTutorialEvent++;
        
        if (self.currentTutorialEvent == TUTORIAL_EVENT_START)
        {
            [self performSelector:@selector(adjustGUI) withObject:nil afterDelay:DELAY];
        }
        else
        {
            if (self.currentTutorialEvent == TUTORIAL_EVENT_REPLAY)
            {
                BOOL isHelpMode = [USER_DEFAULTS boolForKey:RESPONSE_KEY_HELP_HAND];
                
                if (!isHelpMode)
                {
                    self.currentTutorialEvent++;
                    [self performSelector:@selector(adjustGUI) withObject:nil afterDelay:DELAY];
                }
                else
                {
                    [self performSelector:@selector(callWebServiceForTutorialEvent) withObject:nil afterDelay:DELAY];
                }
            }
            else
            {
                [self performSelector:@selector(callWebServiceForTutorialEvent) withObject:nil afterDelay:DELAY];
            }
        }
    }
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - Action Methods
- (IBAction)beginPracticeButtonPressed:(id)sender
{
    [self.beginPracticeView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1];
    [self configureMoviePlayerWithMovieName:[self.dynamicTutorialVideoNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1] ofType:@"m4v"];
    
#ifdef FAST_FORWARD
    [self loadHtml];
#endif
}

- (IBAction)nextButtonPressed:(id)sender
{
    // Get the response of question
    [self.dynamicQuestionWebView stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
    NSString *questionResponse = [self.dynamicQuestionWebView stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    
    
    if ([questionResponse isEqualToString:@"0"] && !self.isOmitQuestionAudioPlayed)
    {
        self.isOmitQuestionAudioPlayed = YES;
        // Stop the audio player
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_OMIT_QUESTION withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] playAudio];
        return;
    }
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    //Set the user action to NONE
    self.currentUserAction = TUTORIAL_USER_ACTION_NONE;
    [self callWebServiceForTutorialEvent];
}

- (IBAction)helpButtonPressed:(id)sender
{
    [[AudioPlayer sharedAudioPlayer] stopAudio];
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_WAIT_FOR_HELP withDelegate:self];
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (IBAction)repeatAudioButtonPressed:(id)sender
{
    [self playQuestionAudio];
}

//Reports admin web panel about various events in tutorial
- (void) callWebServiceForTutorialEvent
{
    // Do not perform any activity if global pause is on
    if (self.isGlobalPauseOn) {
        return;
    }
    
    if (self.isExamStopped) {
        return;
    }
    
    if ([[ERBCommons appController] internetCheck])
    {
        NSString *eventString = nil;
        
        switch (self.currentTutorialEvent) {
            case TUTORIAL_EVENT_HELP:
                eventString = USER_ACTION_TUTORIAL_HELP;
                break;
            case TUTORIAL_EVENT_REPLAY:
                eventString = USER_ACTION_TUTORIAL_REPLAY;
                break;
            case TUTORIAL_EVENT_NEXT:
                eventString = USER_ACTION_TUTORIAL_NEXT;
                break;
            case TUTORIAL_DYNAMIC_1:
                eventString = USER_ACTION_TUTORIAL_DYNAMIC_1;
                break;
            case TUTORIAL_DYNAMIC_2:
                eventString = USER_ACTION_TUTORIAL_DYNAMIC_2;
                break;
            case TUTORIAL_DYNAMIC_3:
                eventString = USER_ACTION_TUTORIAL_DYNAMIC_3;
                break;
            case TUTORIAL_EVENT_END:
                eventString = USER_ACTION_TUTORIAL_END;
                [self disableAllButtons];
                break;
            default:
                break;
        }
        
        if (eventString) {
            NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN,
                             REQUEST_POST_KEY_EXAM_ID,
                             REQUEST_POST_KEY_SECTION_ID,
                             REQUEST_POST_KEY_STUDENT_ID,
                             REQUEST_POST_KEY_ACTION,
                             REQUEST_POST_KEY_QUESTION_ID,
                             REQUEST_POST_KEY_RESPONSE,  nil];
            
            NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken],
                               self.checkInData.examId,
                               @"0",
                               self.checkInData.studentId,
                               eventString,
                               @"0",
                               @"",nil];
            
            NSDictionary *paramsDict = nil;
            
            @try {
                paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
            if (self.webServiceHelper) {
                CANCEL_CONNECTION(self.webServiceHelper);
                [webServiceHelper release];
                webServiceHelper = nil;
            }
            
            if (paramsDict) {
                webServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_RECORD_USER_ACTION]]
                                                                    withParams:paramsDict
                                                         withRequestIdentifier:REQUEST_ID_REPORT_USER_TUTORIAL_ACTION
                                                                withHttpMethod:ERB_REQUEST_POST];
                [paramsDict release];
                
                self.webServiceHelper.delegate = self;
                [self.webServiceHelper sendWebServiceRequest];
            }
        }
    }
    else
    {
        //If no inernet is there, we give a gap of 2 seconds, and call the web service again.
        [self performSelector:@selector(callWebServiceForTutorialEvent) withObject:nil afterDelay:DELAY];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    if ([requestId isEqualToString:REQUEST_ID_REPORT_USER_TUTORIAL_ACTION]) {
        
        // Do not perform any activity if global pause is on
        if (self.isGlobalPauseOn) {
            return;
        }
        
        if (self.isExamStopped) {
            return;
        }
        
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        if (!error) {
            if ([[response objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                //Call adjust UI only if the currentTutorialEvent is less than 5
                if (self.currentTutorialEvent < TUTORIAL_EVENT_NEXT)
                {
                    [self adjustGUI];
                }
                else if (self.currentTutorialEvent == TUTORIAL_EVENT_NEXT)
                {
                    //Increment the tutorial event
                    self.currentTutorialEvent++;
                    [self.view addSubview:self.beginPracticeView];
                    [self.tutorialImageView setImage:nil];
                    
                    self.currentUserAction = TUTORIAL_USER_ACTION_BEGIN_PRACTICE;
                    
                    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_BEGIN_PRACTICE withDelegate:self];
                    [[AudioPlayer sharedAudioPlayer] playAudio];
                    
                }
                else if (self.currentTutorialEvent <= TUTORIAL_EVENT_END)
                {
                    //Increment the tutorial event
                    self.currentTutorialEvent++;
                    self.isOmitQuestionAudioPlayed = NO;
                    if (self.currentTutorialEvent > TUTORIAL_EVENT_END)
                    {
                        //Check the currentTutorialEvent count. If it is more than the number of dynamic tutorials, call delegate
                        if (delegate && [delegate respondsToSelector:@selector(didEndTutorial)])
                        {
                            CANCEL_CONNECTION(self.webServiceHelper);
                            [delegate didEndTutorial];
                            return;
                        }
                    }
                    
                    [self.dynamicTutorialView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1];
                    [self configureMoviePlayerWithMovieName:[self.dynamicTutorialVideoNames objectAtIndex:self.currentTutorialEvent - TUTORIAL_DYNAMIC_1] ofType:@"m4v"];
#ifdef FAST_FORWARD
                    [self loadHtml];
#endif
                }
            }
            else
            {
                [self callWebServiceForTutorialEvent];
            }
        }
        else{
            DLog(@"Error while parsing JSON response for WS: %@", requestId);
        }
    }
}

- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    // Do not perform any activity if global pause is on
    if (self.isGlobalPauseOn) {
        return;
    }
    
    if (self.isExamStopped) {
        return;
    }
    
    [self callWebServiceForTutorialEvent];
}

#pragma mark - UIScrollViewDelegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

#pragma mark- LifeCycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BOOL isHelpMode = [USER_DEFAULTS boolForKey:RESPONSE_KEY_HELP_HAND];
    
    //Set the help mode
    if (!isHelpMode) {
        self.helpButton.hidden = YES;
    }
    
    //Disable scroll in web view
    UIScrollView *scrollView = self.dynamicQuestionWebView.scrollView;
    scrollView.delegate = self;
    scrollView.scrollEnabled = NO;
    scrollView.bounces = NO;
    
    self.currentUserAction = TUTORIAL_USER_ACTION_NONE;
    self.currentUserAction = TUTORIAL_USER_ACTION_SLIDE_AUDIO;
    self.dynamicTutorialHTMLNames = [[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:DYNAMIC_TUTORIAL_PLIST_NAME ofType:@"plist"]] objectAtIndex:0];
    
    if (isHelpMode)
        self.dynamicTutorialVideoNames = [[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:DYNAMIC_TUTORIAL_PLIST_NAME ofType:@"plist"]] objectAtIndex:1];
    else
        self.dynamicTutorialVideoNames = [[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:DYNAMIC_TUTORIAL_PLIST_NAME ofType:@"plist"]] objectAtIndex:2];
    
    //Customize all fonts
    [self customizeUI];
    
    // Add self as observer for pause start notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:NOTIFICATION_GLOBAL_PAUSE_STARTED object:nil];
    // Add self as observer for pause end notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:NOTIFICATION_GLOBAL_PAUSE_ENDED object:nil];
    // Add self as observer for exam end notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:NOTIFICATION_EXAM_STOPPED object:nil];
    
    // Add notification for movie player
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseVideo) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    // Set current tutorial event to non existent value
    self.currentTutorialEvent = TUTORIAL_EVENT_NONE;
    NSString *imageName = [NSString stringWithFormat:@"slide0%d.png",self.currentTutorialEvent];
    UIImage *tutorialImage = [UIImage imageWithContentsOfFile:IMAGE_PATH(imageName)];
    [self.tutorialImageView setImage:tutorialImage];
    
    
    // Do any additional setup after loading the view from its nib.
    [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_TUTORIAL_INTRO withDelegate:self];
    
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0, 0, 1024, 768);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setTutorialImageView:nil];
    [self setCheckInData:nil];
    [self setBeginPracticeView:nil];
    [self setTutorialLabel:nil];
    [self setReadyToBeginLabel:nil];
    [self setBeginPracticeLabel:nil];
    [self setDynamicQuestionWebView:nil];
    [self setDynamicTutorialView:nil];
    [self setHelpButton:nil];
    [self setReplayButton:nil];
    [self setNextButton:nil];
    [self setBeginPracticeButton:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    SAFE_RELEASE(webServiceHelper);
    SAFE_RELEASE(dynamicTutorialHTMLNames);
    [checkInData release];
    [tutorialImageView release];
    [_beginPracticeView release];
    [_tutorialLabel release];
    [_readyToBeginLabel release];
    [_beginPracticeLabel release];
    [_dynamicQuestionWebView release];
    [_dynamicTutorialView release];
    [_helpButton release];
    [_replayButton release];
    [_nextButton release];
    [_beginPracticeButton release];
    [super dealloc];
}

@end
