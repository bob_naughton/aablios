//
//  ERBExamViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBExamViewControllerOffline.h"
#import "EncryptedFileURLProtocol.h"
#import "AudioPlayer.h"
#import "ERBBeginTutorialViewController.h"
#import "ERBExamState.h"

@interface ERBExamViewControllerOffline ()

- (void) unHideExamViewController;
- (void) endSection;
- (void) loadQuestionUsingJson:(NSString *)json inWebView:(UIWebView *)_webView;
- (void) playAudio;
- (void) playAnswerAudioWithAnswerID: (NSString *)answerId;
- (NSString *) getCorrectAnswerFromJson:(NSDictionary *)json;
- (void) enableAnswerChoices;
- (void) callWebServiceToUploadResponses;

@property (assign, nonatomic) int elapsedSectionTime;
@property (assign, nonatomic) int elapsedQuestionTime;
@property (assign, nonatomic) NSTimeInterval questionStartTime;
@property (assign, nonatomic) NSTimeInterval questionEndTime;
@property (retain, nonatomic) NSString *secondsElapsed;
@property (nonatomic, assign) AUDIO_STATE currentAudioState;
@property (nonatomic, assign) BOOL didPlayQuestionAudioOnce;
@property (nonatomic, assign) BOOL isOmitQuestionAudioPlayed;
@property (nonatomic, retain) NSString *questionsFilePath;
@property (nonatomic, retain) ERBWebOperation *uploadResponsesWebServiceHelper;
@property (nonatomic, assign) BOOL isExamViewControllerUnhidden;

@end

@implementation ERBExamViewControllerOffline
@synthesize webView, webViewAlternate, sectionEndImageView, checkinData, activityIndicator, nextButton, replayButton, delegate;
@synthesize section_id, questionsCount;
@synthesize questionsArray, questionResponse;
@synthesize currentUserAction;
@synthesize elapsedSectionTime, elapsedQuestionTime, questionEndTime, questionStartTime, secondsElapsed;
@synthesize currentAudioState;
@synthesize didPlayQuestionAudioOnce;
@synthesize isOmitQuestionAudioPlayed;
@synthesize allQuestionsArray;
@synthesize questionsFilePath;
@synthesize uploadResponsesWebServiceHelper;

#pragma mark - INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - UIWebViewDelegate

-(BOOL) webView:(UIWebView *)_webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [[request URL] absoluteString];
    
    if ([requestString hasPrefix:@"js-frame:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        
        NSString *function = (NSString*)[components objectAtIndex:1];
        NSString *answerId = [components objectAtIndex:2];
        
        if ([function isEqualToString:@"playAnswerAudio"])
        {
            [self playAnswerAudioWithAnswerID:answerId];
        }
        return NO;
    }
    return YES;
}

-(void) webViewDidStartLoad:(UIWebView *)_webView{
    
}

-(void) webViewDidFinishLoad:(UIWebView *)_webView
{
    static int questionLoadCount = 0;
    
    //Disable copy in UIWebView
    // Disable user selection
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    // Disable callout
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    
    //FIx for white screen appearing for the first time.
    DLog(@"Offline Question count: %d",self.questionsCount);
    if (questionLoadCount == 1) { // to avoid white screen in offline resume
        [self performSelector:@selector(unHideExamViewController) withObject:nil afterDelay:3.0];
    }
    else if ((_webView == self.webView && (self.questionsCount-1) % QUESTION_LIMIT_PER_SECTION == 0) && questionLoadCount != 0)
    {
        [self performSelector:@selector(unHideExamViewController) withObject:nil afterDelay:1.0];
    }
    else if (self.isExamResumed){
        self.isExamResumed = NO;
        [self performSelector:@selector(unHideExamViewController) withObject:nil afterDelay:1.0];
    }
    questionLoadCount++;
}

- (void) unHideExamViewController
{
    if (delegate && [delegate respondsToSelector:@selector(questionDidLoadSuccessfullyForFirstTime)])
    {
        if (self.isExamViewControllerUnhidden) {
            return;
        }
        
        self.isExamViewControllerUnhidden = YES;
        // Set the start time for first question
        self.questionStartTime = [[NSDate date] timeIntervalSince1970];
        [delegate questionDidLoadSuccessfullyForFirstTime];
    }
}

-(void) webView:(UIWebView *)_webView didFailLoadWithError:(NSError *)error{
    DLog("Exam Web View error: %@",[error localizedDescription]);
}

#pragma mark - Action Methods
- (IBAction)nextButtonPressed:(id)sender
{
#ifdef FAST_FORWARD
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#else
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#endif
    
    // Get the response of question
    if (!self.webView.isHidden) {
        [self.webView stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
        self.questionResponse = [self.webView stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    }
    else{
        [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
        self.questionResponse = [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    }
    
    if ([self.questionResponse isEqualToString:@"0"] && NO == self.isOmitQuestionAudioPlayed)
    {
        self.nextButton.enabled = YES;
        self.replayButton.enabled = YES;
        self.isOmitQuestionAudioPlayed = YES;
        // Stop the audio player
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_OMIT_QUESTION withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] playAudio];
        
        return;
    }
    else
    {
        //Disable the next and play buttons
#ifdef FAST_FORWARD
        self.nextButton.enabled = YES;
        self.replayButton.enabled = YES  ;
#else
        self.nextButton.enabled = NO;
        self.replayButton.enabled = NO;
#endif
    }
    
    //Reset the bool flag to NO
    self.isOmitQuestionAudioPlayed = NO;
    
    // Stop the audio player
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    // Set the question end time and get the elapsed time
    self.questionEndTime = [[NSDate date] timeIntervalSince1970];
    self.secondsElapsed = [ERBCommons secondsBetweenStartInterval:self.questionStartTime andEndInterval:self.questionEndTime];
    
    // Set the start time for next question
    self.questionStartTime = self.questionEndTime;
    
    //Reset the didPlayQuestionAudioOnce to NO
    self.didPlayQuestionAudioOnce = NO;
    
    // Increment the question count
    self.questionsCount++;
    
    NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_SECTION_ID,
                     REQUEST_POST_KEY_QUESTION_ID,
                     REQUEST_POST_KEY_ERB_QUESTION_ID,
                     REQUEST_POST_KEY_RESPONSE,
                     REQUEST_POST_KEY_DESIRED_RESPONSE,
                     REQUEST_POST_KEY_IS_RESPONSE_CORRECT,
                     REQUEST_POST_KEY_SECONDS_ELAPSED, nil];
    
    // Check the current user action and accordingly set the values array
    
    // Send question ID of the question which is being submitted
    ERBQuestion *question = [self.questionsArray objectAtIndex:0];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[question.question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSString *correctResponse =[self getCorrectAnswerFromJson:json];
        
        NSArray *values = [NSArray arrayWithObjects: [NSNumber numberWithInt:self.section_id],
                           question.erbQuestionId,
                           question.questionId,
                           self.questionResponse,
                           correctResponse,
                           ([self.questionResponse isEqualToString:correctResponse] ? @"yes" : @"no"),
                           self.secondsElapsed, nil];
        
        
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        //Add it to the dict
        NSMutableDictionary *responsesDict = [NSMutableDictionary dictionaryWithContentsOfFile:self.questionsFilePath];
        //Get the responses array from dict and add the current respones
        NSMutableArray *updatedResponsesArray = [responsesDict objectForKey:REQUEST_POST_KEY_RESPONSES_ARRAY];
        [updatedResponsesArray addObject:paramsDict];
        [paramsDict release];
        [responsesDict setObject:updatedResponsesArray forKey:REQUEST_POST_KEY_RESPONSES_ARRAY];
        [responsesDict writeToFile:self.questionsFilePath atomically:YES];
        
        // End Section if question Id is equal to max limit of questions per section
        if ((self.questionsCount-1) % QUESTION_LIMIT_PER_SECTION == 0)
        {
            [self endSection];
        }
        else
        {
            self.currentUserAction = ERB_EXAM_USER_ACTION_SUBMIT;
            
            // Apply the fade animation for questions
            [ERBCommons applyFadeAnimationTo:self.view.window withDuration:0.5];
            
            // Based on current webview's visibility, hide/unhide the webviews
            if (self.webViewAlternate.isHidden) {
                //enable audio
                [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:1]];
                
                self.webViewAlternate.hidden = NO;
                self.webView.hidden = YES;
            }
            else{
                //enable audio
                [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:1]];
                self.webViewAlternate.hidden = YES;
                self.webView.hidden = NO;
            }
            
            //Now update the questions array
            //Remove the previous question
            [self.questionsArray removeObjectAtIndex:0];
            
            if (self.questionsCount < self.allQuestionsArray.count)
            {
                NSDictionary *questionDict = [self.allQuestionsArray objectAtIndex:self.questionsCount];
                ERBQuestion *questionToSave = [[ERBQuestion alloc] init];
                questionToSave.question = [questionDict objectForKey:RESPONSE_KEY_QUESTION];
                questionToSave.questionId = [questionDict objectForKey:@"item_id"];
                questionToSave.erbQuestionId = [questionDict objectForKey:REQUEST_POST_KEY_QUESTION_ID];
                DLog(@"Question added is: %d", self.questionsCount);
                [self.questionsArray addObject:questionToSave];
                [questionToSave release];
                
                ERBQuestion *nextQuestionToLoad = [self.questionsArray objectAtIndex:1];
                if (self.webView.hidden)
                {
                    [self loadQuestionUsingJson:nextQuestionToLoad.question inWebView:self.webView];
                }
                else
                {
                    [self loadQuestionUsingJson:nextQuestionToLoad.question inWebView:self.webViewAlternate];
                }
            }
            
        }
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (IBAction)playAudioButtonPressed:(id)sender
{
    self.currentUserAction = ERB_EXAM_USER_ACTION_NONE;
    
    if (self.didPlayQuestionAudioOnce) {
        // Enable Next and Replay Audio Button
        self.nextButton.enabled = YES;
        
        //Enable answer choices
        [self enableAnswerChoices];
        
        // Disable the Replay Button
        self.replayButton.enabled = NO;
    }
    
    //Play question audio according to whether we have received next question. If we have not, we play the audio at index 0, else we play audio at index 1.
    [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:0]];
}

#pragma mark - Private Methods
- (void) enableAnswerChoices
{
    //Enable choices
    if (!self.webView.isHidden)
    {
        [self.webView stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
    }
    else
    {
        [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
    }
}

- (void)playAudio{
    
    if (!self.didPlayQuestionAudioOnce) {
        [self playAudioButtonPressed:nil];
    }
}

- (void) playAnswerAudioWithAnswerID: (NSString *)answerId
{
    ERBQuestion *question = [self.questionsArray objectAtIndex:0];
    
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[question.question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSArray *answerChoicesArray = [dataDic objectForKey:@"choices"];
        
        for (NSDictionary *dict in answerChoicesArray)
        {
            NSString *tempAnswerId = [dict objectForKey:@"answerID"];
            if ([tempAnswerId isEqualToString: answerId])
            {
                NSString *answerAudioPath = [dict objectForKey:@"answerAudio"];
                NSString *absoluteAudioPath = [[answerAudioPath stringByReplacingOccurrencesOfString:@"audio/" withString:@""] stringByReplacingOccurrencesOfString:@"mp3" withString:@"m4a"];
                absoluteAudioPath = [EXAM_AUDIO_PATH stringByAppendingPathComponent:absoluteAudioPath];
                
                [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
                //Set the state
                self.currentAudioState = AUDIO_STATE_ANSWER;
                [[AudioPlayer sharedAudioPlayer] playAudio];
                break;
            }
        }
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) playQuestionAudioAtIndex: (NSNumber *)index
{
    if ([[AudioPlayer sharedAudioPlayer] playerState] == PLAYING && ([[[AudioPlayer sharedAudioPlayer] audioFile] isEqualToString:AUDIO_PAUSE_SCREEN_PRE_EXAM_START] || [[[AudioPlayer sharedAudioPlayer] audioFile] isEqualToString:AUDIO_PAUSE])) {
        return;
    }
    
    ERBQuestion *question = [self.questionsArray objectAtIndex:[index intValue]];
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[question.question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSString *questionAudioPath = [dataDic objectForKey:@"questionAudio"];
        NSString *absoluteAudioPath = [[questionAudioPath stringByReplacingOccurrencesOfString:@"mp3" withString:@"m4a"] stringByReplacingOccurrencesOfString:@"audio/" withString:@""];
        absoluteAudioPath = [EXAM_AUDIO_PATH stringByAppendingPathComponent:absoluteAudioPath];
        
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
        
        //Set the state
        self.currentAudioState = AUDIO_STATE_QUESTION;
        
        // Play Question Audio
        [[AudioPlayer sharedAudioPlayer] performSelector:@selector(playAudio) withObject:nil afterDelay:0.75];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Change request by Customer to show Reinforcement screen for 1 more second.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) callEndSectionDelegate
{
    if (delegate && [delegate respondsToSelector:@selector(didEndSection:withData:withQuestion:)])
    {
        [delegate didEndSection:self.section_id withData:self.checkinData withQuestion:[self.questionsArray lastObject]];
    }
}

- (void) loadQuestionUsingJson:(NSString *)json inWebView:(UIWebView *)_webView
{
    //Covert JSON to NSDictionary
    
    if([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0)){
        json = [json stringByReplacingOccurrencesOfString:@".png" withString:@"@2x.png"];
    }
    NSError *error = nil;
    
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        
        int type=1;
        
        if(dataDic && [dataDic count]>0){
            type = [[dataDic objectForKey:@"type"] intValue];
        }
        
        //Create a request and load HTML Template in webview based on Type Parameter
        NSString *htmlFilePath = nil;
        NSURL *url = nil;
        
        htmlFilePath = [EXAM_HTML_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"template%d.html",type]];
        url = [NSURL encryptedFileURLWithPath:htmlFilePath];
        
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        html = [html stringByReplacingOccurrencesOfString:@"//$$REPLACEDATA$$//" withString:[NSString stringWithFormat:@"var jsondata = '%@';",[json stringByReplacingOccurrencesOfString:@"\n" withString:@""]]];
        [_webView loadHTMLString:html baseURL:url];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) endSection
{
    //Set the state
    self.currentAudioState = AUDIO_STATE_SECTION;
    
    //Set the current user action
    self.currentUserAction = ERB_EXAM_USER_ACTION_END_SECTION;
    
    self.view.userInteractionEnabled = NO;
    
    DLog(@"Responses saved for section id: %d are: %@:", self.section_id,[NSMutableDictionary dictionaryWithContentsOfFile:self.questionsFilePath]);
    
    //Set the end section image and play the encouragement audio file based on section that ended
    switch (self.section_id)
    {
        case VERBAL_REASONING:
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"super_job.png")];
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_PRACTICAL withDelegate:self];
            break;
        case QUANTITATIVE_REASONING:
            //Set the current user action
            self.currentUserAction = ERB_EXAM_USER_ACTION_BREAK;
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"nice_work.png")];
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_NICE_WORK withDelegate:self];
            break;
        case EARLY_LITERACY:
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"great_work.png")];
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_MATH withDelegate:self];
            break;
        case MATH:
            //Set the state
            SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithInteger:ERB_EXAM_STATE_UPLOAD_RESPONSES], KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE)
            
            self.sectionEndImageView.image = [UIImage imageWithContentsOfFile:IMAGE_PATH(@"the_end.png")];
            [self.view addSubview:self.sectionEndImageView];
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_THE_END withDelegate:self];
            [[AudioPlayer sharedAudioPlayer] playAudio];
            self.currentUserAction = ERB_EXAM_USER_ACTION_END_EXAM;
            
            [self performSelector:@selector(endExam) withObject:nil afterDelay:END_EXAM_DELAY];
            
            if ([[ERBCommons appController] internetCheck])
            {
                // Call the web service to send the saved responses to server
                [self callWebServiceToUploadResponses];
            }
            
            return;
        default:
            break;
    }
    
    [self.view addSubview:self.sectionEndImageView];
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (NSString *) getCorrectAnswerFromJson:(NSDictionary *)json{
    NSArray *choices = [json objectForKey:@"choices"];
    int correct = 0;
    
    for (NSDictionary *choice in choices) {
        correct ++ ;
        if ([[choice objectForKey: @"correct"] integerValue] == 1) {
            return [NSString stringWithFormat:@"%d",correct];
        }
    }
    return @"";
}

- (void) callWebServiceToUploadResponses
{
    DLog(@"Uploading responses.....");
    if (uploadResponsesWebServiceHelper)
    {
        CANCEL_CONNECTION(self.uploadResponsesWebServiceHelper);
        [uploadResponsesWebServiceHelper release];
        uploadResponsesWebServiceHelper = nil;
    }
    
    NSMutableDictionary *responsesDict = [NSMutableDictionary dictionaryWithContentsOfFile:self.questionsFilePath];
    NSMutableArray *responses = [responsesDict objectForKey:@"responses"];
    
    NSError *error = nil;
    id logJSON = [NSJSONSerialization dataWithJSONObject:responses options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[[NSString alloc] initWithData:logJSON encoding:NSUTF8StringEncoding] autorelease];
    [responsesDict removeObjectForKey:@"responses"];
    [responsesDict setObject:jsonString forKey:@"responses"];
    if (responsesDict)
    {
        uploadResponsesWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_ADD_OFFLINE_RESPONSES]] withParams:responsesDict withRequestIdentifier:REQUEST_ID_UPLOAD_RESPONSE_ACTION withHttpMethod:ERB_REQUEST_POST];
        self.uploadResponsesWebServiceHelper.delegate = self;
        [self.uploadResponsesWebServiceHelper sendWebServiceRequest];
    }
}

-(void)endExam{
    if (delegate && [delegate respondsToSelector:@selector(examDidEnd:)])
    {
        [delegate examDidEnd:YES];
    }
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId   operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    NSError *error = nil;
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    if (!error) {
        //Uncomment this later
        if ([[response  objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
        {
            DLog(@"Sucessfully sent offline responses....");
            
            NSMutableDictionary *responsesDict = [NSMutableDictionary dictionaryWithContentsOfFile:self.questionsFilePath];
            [ERBCommons showAlert:[NSString stringWithFormat:@"Data upload for Student %@ was successful", [responsesDict objectForKey:RESPONSE_KEY_STUDENT_NAME]] delegate:nil];
            
            //Here remove the array from docs directory
            [ERBCommons removeOfflineContent];
            [ERBCommons clearUserDefaults];
            //Clear User Defaults
            //            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            //            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        }
        else
        {
            DLog(@"Error while sending offline responses....");
        }
    }
    else{
        DLog(@"Error while parsing JSON data for WS %@", response);
    }
    
    
}


- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if (error)
    {
        DLog(@"ERROR while uploading responses: %@",[error localizedDescription]);
    }
}

#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    switch (self.currentAudioState)
    {
        case AUDIO_STATE_QUESTION:
        {
            //Set the didPlayQuestionAudioOnce to YES
            self.didPlayQuestionAudioOnce = YES;
            
            // Enable Next and Replay Audio Button
            self.nextButton.enabled = YES;
            self.replayButton.enabled = YES;
            //Enable choices
            [self enableAnswerChoices];
            
            break;
        }
        case AUDIO_STATE_SECTION:
        {
            if (self.section_id == QUANTITATIVE_REASONING) {
                [self performSelector:@selector(callEndSectionDelegate) withObject:nil afterDelay:DELAY];
            }
            else if (self.section_id == VERBAL_REASONING || self.section_id == EARLY_LITERACY)
            {
                [self callEndSectionDelegate];
            }
            break;
        }
        default:
            break;
    }
    self.currentAudioState = AUDIO_STATE_NONE;
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - UIScrollViewDelegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DLog(@"All questions are: %@",self.allQuestionsArray);
    self.questionsFilePath = [[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT] stringByAppendingPathComponent:FILE_NAME_QUESTION_RESPONSES];
    
    self.view.userInteractionEnabled = YES;
    
    // Reset the pause values
    ERBBeginTutorialViewController *_delegate = (ERBBeginTutorialViewController *)self.delegate;
    _delegate.pauseStartTime = 0.0;
    _delegate.pauseEndTime   = 0.0;
    
    // Add self as observer for pause end notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playAudio) name:NOTIFICATION_GLOBAL_PAUSE_ENDED object:nil];
    
    //Set the didPlayQuestionAudioOnce flag to NO initially. Will set to YES when audio plays for the first time
    self.didPlayQuestionAudioOnce = NO;
    
    // Initialize question response to 0;
    self.questionResponse = @"0";
    
    UIScrollView *scrollView = self.webView.scrollView;
    scrollView.delegate = self;
    scrollView.scrollEnabled = NO;
    scrollView.bounces = NO;
    
    UIScrollView *alternateScrollView = self.webViewAlternate.scrollView;
    alternateScrollView.delegate = self;
    alternateScrollView.scrollEnabled = NO;
    alternateScrollView.bounces = NO;
    
    questionsArray = [[NSMutableArray alloc] init];
    //Make the ERBQuestion
    NSDictionary *questionDict = nil;
    
    //Make the ERBQuestion
    NSDictionary *secondQuestionDict = nil;
    
    questionDict = [self.allQuestionsArray objectAtIndex:self.questionsCount];
    self.questionsCount++;
    secondQuestionDict = [self.allQuestionsArray objectAtIndex:self.questionsCount];
    
    ERBQuestion *firstQuestion = [[ERBQuestion alloc] init];
    firstQuestion.question = [questionDict objectForKey:RESPONSE_KEY_QUESTION];
    firstQuestion.questionId = [questionDict objectForKey:@"item_id"];
    firstQuestion.erbQuestionId = [questionDict objectForKey:REQUEST_POST_KEY_QUESTION_ID];
    
    [self.questionsArray addObject:firstQuestion];
    [firstQuestion release];
    
    ERBQuestion *secondQuestion = [[ERBQuestion alloc] init];
    secondQuestion.question = [secondQuestionDict objectForKey:RESPONSE_KEY_QUESTION];
    secondQuestion.questionId = [secondQuestionDict objectForKey:@"item_id"];
    secondQuestion.erbQuestionId = [secondQuestionDict objectForKey:REQUEST_POST_KEY_QUESTION_ID];
    
    [self.questionsArray addObject:secondQuestion];
    [secondQuestion release];
    
    ERBQuestion *nextQuestionToLoad = [self.questionsArray objectAtIndex:1];
    [self loadQuestionUsingJson:nextQuestionToLoad.question inWebView:self.webViewAlternate];
    
    ERBQuestion *currentQuestionToLoad = [self.questionsArray objectAtIndex:0];
    [self loadQuestionUsingJson:currentQuestionToLoad.question inWebView:self.webView];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0, 0, 1024, 768);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SAFE_RELEASE(allQuestionsArray);
    SAFE_RELEASE(questionsFilePath);
    [checkinData release];
    [webView release];
    [activityIndicator release];
    [sectionEndImageView release];
    [questionResponse release];
    [questionsArray release];
    [nextButton release];
    [webViewAlternate release];
    [replayButton release];
    [super dealloc];
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setCheckinData:nil];
    [self setWebView:nil];
    [self setActivityIndicator:nil];
    [self setSectionEndImageView:nil];
    [self setQuestionResponse:nil];
    [self setNextButton:nil];
    [self setQuestionsArray:nil];
    [self setWebViewAlternate:nil];
    [self setReplayButton:nil];
    [super viewDidUnload];
}

@end
