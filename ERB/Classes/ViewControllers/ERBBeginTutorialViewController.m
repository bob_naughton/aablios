//
//  ERBBeginTutorialViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBBeginTutorialViewController.h"
#import "AudioPlayer.h"
#import "ERBHelpViewController.h"
#import "AudioVolumeViewController.h"
#import "ERBExamState.h"
#import "NSData+CommonCrypto.h"
#import "ERBTutorialViewControllerOffline.h"

@interface ERBBeginTutorialViewController ()
- (void) callReportUserActionWebService;
- (void) callGetExamStatusWebService;
- (void) callGetAssetsWebServiceForDownload;
- (void) startPolling;
- (void) stopPolling;
- (void) customizeUI;
- (void) startTutorial;
- (void) disableButtons;
- (void) enableButtons;
- (void) removeAllSubviews;
- (void) releaseAllObjects;
- (void) releaseCheckinAndScheduleViewControllers;
- (void) onPauseStart;
- (void) onPauseEnd;
- (void) resetAndLaunchLogin;
- (void) addReusableViewForExamState:(ERB_EXAM_STATE)examState;
- (void) sendLogOutDetailsForEvent:(NSString *)event;
- (void) handleOnlineStateMaintenance;
- (void) saveCurrentExamState:(ERB_EXAM_STATE)currentState;
- (void) loadOnlineExamForState:(ERBExamState *)examState;
- (void) onlineExamLoadedSuccessfully;
- (void) enableLoginView;
- (void) disableLoginView;
- (void) loadDownloadScreen;
- (void) guidedAccessChanged:(NSNotification *)notice;

@property (nonatomic, retain) ERBLoginViewController *loginViewController;
@property (nonatomic, retain) ERBScheduleViewController *scheduleViewController;
@property (nonatomic, retain) ERBCheckInViewController *checkInViewController;
@property (nonatomic, retain) ERBTutorialViewController *tutorialViewController;
@property (nonatomic, retain) ERBTutorialViewControllerOffline *tutorialViewControllerOffline;
@property (nonatomic, retain) ERBBeginSectionViewController *beginSectionViewController;
@property (nonatomic, retain) ERBExamViewController *examViewController;
@property (nonatomic, retain) ERBExamViewControllerOffline *examViewControllerOffline;
@property (nonatomic, retain) ERBBreakViewController *breakViewController;
@property (nonatomic, retain) ERBReusableViewController *reusableViewController;
@property (nonatomic, retain) ERBWebOperation *userActionWebServiceHelper;
@property (nonatomic, retain) ERBWebOperation *examStatusWebServiceHelper;
@property (nonatomic, retain) ERBWebOperation *checkinWebServiceHelper;
@property (nonatomic, retain) ERBWebOperation *assetsWebServiceHelper;
@property (nonatomic, retain) AudioVolumeViewController *volumeViewController;
@property (nonatomic, retain) DownloadViewController *downloadViewController;
@property (nonatomic, assign) BOOL isTutorialFinished;
@property (nonatomic, retain) NSArray *allQuestionsArray;
@property (nonatomic, retain) NSArray *loginData;
@property (nonatomic, retain) NSDictionary *downloadData;

@end

@implementation ERBBeginTutorialViewController
@synthesize loginViewController;
@synthesize checkInViewController;
@synthesize scheduleViewController;
@synthesize tutorialViewController;
@synthesize tutorialViewControllerOffline;
@synthesize beginSectionViewController;
@synthesize examViewController;
@synthesize breakViewController;
@synthesize activityIndicator;
@synthesize reusableViewController;
@synthesize savedQuestionsArray;
@synthesize studentNameLabel;
@synthesize seatNumberLabel;
@synthesize checkInData;
@synthesize putHeadphonesLabel;
@synthesize beginTutorialLabel;
@synthesize pauseEndTime, pauseStartTime;
@synthesize currentExamProgress;
@synthesize userActionWebServiceHelper, examStatusWebServiceHelper, assetsWebServiceHelper;
@synthesize checkinWebServiceHelper;
@synthesize volumeViewController;
@synthesize isTutorialFinished;
@synthesize allQuestionsArray;
@synthesize examViewControllerOffline;
@synthesize downloadViewController;
@synthesize loginData;
@synthesize shouldHideStatusBar;
@synthesize isExamResumed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        shouldHideStatusBar = NO;
        self.isExamResumed = NO;
    }
    return self;
}

#pragma mark - ERBLoginViewControllerDelegate

- (void) testDidResumeInState: (NSString *)examState
{
    [self disableButtons];
    
    [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
    
    if ([examState isEqualToString:EXAM_MODE_OFFLINE])
    {
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:KEY_OFFLINE_QUESTIONS];
        NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        self.allQuestionsArray = array;
        self.isExamResumed = YES;
        
        SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithBool:NO], RESPONSE_KEY_HELP_HAND);
        
        NSDictionary *offlineResponses = [NSDictionary dictionaryWithContentsOfFile:[[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT] stringByAppendingPathComponent:FILE_NAME_QUESTION_RESPONSES]];
        
        //Set the auth token in UserDefaults
        NSString *authToken = [offlineResponses  objectForKey:KEY_AUTH_TOKEN];
        SAVE_USER_DEFAULTS_AND_SYNC(authToken, KEY_AUTH_TOKEN);
        
        //Make ERBCheckin
        ERBCheckIn *checkIn = [[ERBCheckIn alloc] init];
        checkIn.examId = [offlineResponses objectForKey:REQUEST_POST_KEY_EXAM_ID];
        checkIn.studentId = [offlineResponses objectForKey:RESPONSE_KEY_STUDENT_ID];
        checkIn.studentName = [offlineResponses objectForKey:RESPONSE_KEY_STUDENT_NAME];
        self.checkInData = checkIn;
        [checkIn release];
        
        //Check the exam state and proceed accordingly
        NSArray *responses = [offlineResponses objectForKey:@"responses"];
        if (responses)
        {
            NSDictionary *lastQuestion = [responses lastObject];
            
            //Set the current exam progress
            if ((responses.count % QUESTION_LIMIT_PER_SECTION) == 0)
            {
                self.currentExamProgress = [[lastQuestion objectForKey:REQUEST_POST_KEY_SECTION_ID] intValue]+1;
            }
            else
            {
                self.currentExamProgress = [[lastQuestion objectForKey:REQUEST_POST_KEY_SECTION_ID] intValue];
            }
            //Get the last question
        }
        
        ERB_OFFLINE_EXAM_STATE offlineExamState = [[USER_DEFAULTS objectForKey:KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE] intValue];
        if (offlineExamState == ERB_EXAM_STATE_TUTORIAL)
        {
            [self.loginViewController.view removeFromSuperview];
            SAFE_RELEASE(loginViewController);
            [self startTutorial];
        }
        else if (offlineExamState == ERB_EXAM_STATE_BEGIN_TUTORIAL_SCREEN)
        {
            [self.loginViewController.view removeFromSuperview];
            SAFE_RELEASE(loginViewController);
            
            //Set the student name and seat number
            NSString *StudentName = [offlineResponses objectForKey:RESPONSE_KEY_STUDENT_NAME];
            NSString *seatNumber = [offlineResponses objectForKey:RESPONSE_KEY_STUDENT_SEAT];
            self.studentNameLabel.text = StudentName;
            self.seatNumberLabel.text = seatNumber;
            
            self.isExamResumed = NO;
            
            [self enableButtons];
        }
        else if (offlineExamState == ERB_EXAM_STATE_IN_PROGRESS)
        {
            if (responses)
            {
                if ((responses.count % QUESTION_LIMIT_PER_SECTION) == 0)
                {
                    [self.loginViewController.view removeFromSuperview];
                    SAFE_RELEASE(loginViewController);
                    
                    SAFE_RELEASE(beginSectionViewController);
                    //Push the beginSectionViewController
                    beginSectionViewController = [[ERBBeginSectionViewController alloc] init];
                    beginSectionViewController.delegate = self;
                    
                    beginSectionViewController.sectionId = self.currentExamProgress;
                    beginSectionViewController.checkinData = self.checkInData;
                    [self.view addSubview:beginSectionViewController.view];
                }
                else
                {
                    SAFE_RELEASE(examViewControllerOffline);
                    
                    //Push the examViewController
                    examViewControllerOffline = [[ERBExamViewControllerOffline alloc] initWithNibName:@"ERBExamViewControllerOffline" bundle:nil];
                    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                    NSData *data = [def objectForKey:KEY_OFFLINE_QUESTIONS];
                    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    examViewControllerOffline.allQuestionsArray = array;
                    examViewControllerOffline.checkinData = self.checkInData;
                    examViewControllerOffline.isExamResumed = YES;
                    
                    self.isExamResumed = NO;
                    
                    examViewControllerOffline.questionsCount = responses.count;
                    
                    examViewControllerOffline.section_id = self.currentExamProgress;
                    examViewControllerOffline.delegate = self;
                    shouldHideStatusBar = YES;
                    
                    [self.view addSubview:examViewControllerOffline.view];
                    examViewControllerOffline.view.hidden = YES;
                    examViewControllerOffline.isExamResumed = YES;
                    if (self.loginViewController) {
                        [loginViewController.view setUserInteractionEnabled:NO];
                    }
                }
            }
        }
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
        shouldHideStatusBar = YES;
        [self updateStatusBar];
    }
    else if ([examState isEqualToString:EXAM_MODE_ONLINE])
    {
        [self handleOnlineStateMaintenance];
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
        shouldHideStatusBar = YES;
        // Initialize the login data so that same is available in case of exam reset
        self.loginData = [[self.loginViewController loginResponse] objectForKey:RESPONSE_KEY_DATA];
        [self updateStatusBar];
    }
}

//Called when admin/supervisor logs in
- (void) adminDidLogin: (BOOL) didLogin withData:(NSArray *)dataArray
{
    if (didLogin)
    {
        self.activityIndicator.frame = self.loginViewController.activityIndicator.frame;
        [self.activityIndicator startAnimating];
        [self.view bringSubviewToFront:self.activityIndicator];
        self.loginViewController.loginButton.enabled = NO;
        self.loginData = dataArray;
        // Call the Assets Download WS
        [self callGetAssetsWebServiceForDownload];
        
    }
    else
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_UNSUCCESSFUL_LOGIN) delegate:nil];
    }
}

- (void) didPressHelp
{
    ERBHelpViewController *helpViewController = [[ERBHelpViewController alloc] init];
    helpViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    helpViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:helpViewController animated:YES completion:nil];
    [helpViewController release];
}

#pragma mark - DownloadViewControllerDelegate Methods

- (void) downloadsDidFinishSucessfully{
    
    [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
    
    [self enableLoginView];
    [self.loginViewController.view setUserInteractionEnabled:YES];
    
    
    if (downloadViewController) {
        //Remove the downloadViewController's view from superview
        [self.downloadViewController.view removeFromSuperview];
        SAFE_RELEASE(downloadViewController);
    }
    
    if (loginViewController) {
        //Remove the downloadViewController's view from superview
        [self.loginViewController.view removeFromSuperview];
        SAFE_RELEASE(loginViewController);
    }
    
    
    //Push the Schedule ViewController
    scheduleViewController = [[ERBScheduleViewController alloc] init];
    scheduleViewController.delegate = self;
    scheduleViewController.scheduleArray = self.loginData;
    [self.view addSubview:scheduleViewController.view];
    
}

- (void) failedToDownloadAssets{
    
    [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
    //Remove the downloadViewController's view from superview
    [self.downloadViewController.view removeFromSuperview];
    SAFE_RELEASE(downloadViewController);
    
    // Enabled login after asset update fails
    [self enableLoginView];
    [self.loginViewController.view setUserInteractionEnabled:YES];
}

#pragma mark - ERBScheduleViewControllerDelegate

//Called when admin/supervisor logs out from schedule view
- (void) adminDidLogoutFromScheduleView: (BOOL) didLogout
{
    if (didLogout)
    {
        //Stop the polling for get exam status
        [self stopPolling];
        
        [ERBCommons clearUserDefaults];
        
        [self releaseCheckinAndScheduleViewControllers];
        
        //Push the loginViewController
        if (loginViewController) {
            SAFE_RELEASE(loginViewController);
        }
        loginViewController = [[ERBLoginViewController alloc] init];
        loginViewController.delegate = self;
        shouldHideStatusBar = NO;
        [self updateStatusBar];
        [self.view addSubview:loginViewController.view];
        
        // Added log to check random logging out bug
        [self sendLogOutDetailsForEvent:@"LOGOUT-SCHEDULE"];
    }
}

//Called when admin/supervisor clicks on begin exam button in schedule view
- (void) launchExamWithLaunchData: (id)launchData scheduleData:(NSDictionary *) scheduleData
{
    //Apply fade animation
    [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
    
    //Disable user interaction of schedule view controller's view
    self.scheduleViewController.view.userInteractionEnabled = NO;
    if (checkInViewController) {
        SAFE_RELEASE(checkInViewController);
    }
    checkInViewController = [[ERBCheckInViewController alloc] init];
    
    //Pass in the launch data array as well as the exam id.
    checkInViewController.checkInDataArray = launchData;
    checkInViewController.examId = [scheduleData objectForKey:REQUEST_POST_KEY_EXAM_ID];
    
    //creating time schedule string
    NSString *startTime = [scheduleData objectForKey:RESPONSE_KEY_START_TIME];
    NSString *endTime = [scheduleData objectForKey:RESPONSE_KEY_END_TIME];
    
    ERBExamData *examData = [[ERBExamData alloc]init];
    examData.examTime = [ERBCommons returnTimeScheduleStringWithStartTime:startTime endTime:endTime];
    examData.examDate = [ERBCommons examDateAfterFormattingFromString:startTime];
    examData.examName  = [scheduleData objectForKey:RESPONSE_KEY_EXAM_NAME];
    examData.schoolName = [scheduleData objectForKey:RESPONSE_KEY_SCHOOL_NAME];
    checkInViewController.examData = examData;
    [examData release];
    
    checkInViewController.delegate = self;
    [self.view addSubview:checkInViewController.view];
}

#pragma mark ERBCheckInViewControllerDelegate
- (void) didPressBackFromCheckIn
{
    // Remove the checkinViewController's view from super view and release it
    [self.checkInViewController.view removeFromSuperview];
    SAFE_RELEASE(checkInViewController);
    [self.view bringSubviewToFront:self.scheduleViewController.view];
    
    //Enable user interaction of schedule view controller's view
    self.scheduleViewController.view.userInteractionEnabled = YES;
}

//Called when admin/supervisor confirms checkin of a student on checkin view
- (void) student:(NSString *)student didCheckIn: (BOOL) didCheckIn withData:(ERBCheckIn *)_checkInData withQuestions: (NSArray *)allQuestions
{
    if (allQuestions) {
        self.allQuestionsArray = allQuestions;
    }
    
    //Apply fade animation
    [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
    
    if (didCheckIn)
    {
        //Set the examId and studentId in the eventLogger.
        
        [[EventLogger sharedEventLogger] setExamId:_checkInData.examId];
        [[EventLogger sharedEventLogger] setStudentId:_checkInData.studentId];
        
        // Save these in user defaults too for resending in case of crash
        SAVE_USER_DEFAULTS_AND_SYNC(_checkInData.examId, REQUEST_POST_KEY_EXAM_ID);
        SAVE_USER_DEFAULTS_AND_SYNC(_checkInData.studentId, REQUEST_POST_KEY_STUDENT_ID);
        
        // Set the current event as checkin for logging purpose
        [[EventLogger sharedEventLogger] setEventType:LOG_EVENT_CHECKIN];
        
        self.checkInData = _checkInData;
        
        //Hide the status bar and fix 20 pixel issue
        //        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
        
        //Send the checkin VC and schedule VC to back
        [self.view sendSubviewToBack:self.checkInViewController.view];
        [self.view sendSubviewToBack:self.scheduleViewController.view];
        
        shouldHideStatusBar = YES;
        [self updateStatusBar];
        //Enable user interaction for this view.
        [self enableButtons];
        
        self.studentNameLabel.text = student;
        self.seatNumberLabel.text = self.checkInData.seatNumber;
        
        //Set the current exam progress
        self.currentExamProgress = EXAM_PROGRESS_TUTORIAL;
        
        // Start Polling
        [self startPolling];
    }
}

//Called when admin/supervisor logs out from check in view
- (void) adminDidLogout: (BOOL) didLogout
{
    if (didLogout)
    {
        //Stop the polling for get exam status
        [self stopPolling];
        
        [ERBCommons clearUserDefaults];
        
        [self releaseCheckinAndScheduleViewControllers];
        
        //Push the loginViewController
        if (loginViewController) {
            SAFE_RELEASE(loginViewController);
        }
        loginViewController = [[ERBLoginViewController alloc] init];
        loginViewController.delegate = self;
        shouldHideStatusBar = NO;
        [self updateStatusBar];
        [self.view addSubview:loginViewController.view];
        
        // Added log to check random logging out bug
        [self sendLogOutDetailsForEvent:@"LOGOUT-CHECKIN"];
    }
}

#pragma mark - ERBTutorialViewControllerDelegate

// Called when tutorial ends
- (void) didEndTutorial
{
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_OFFLINE])
    {
        //Set exam state to in progress
        SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithInteger:ERB_EXAM_STATE_IN_PROGRESS], KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE);
    }
    
    //Set the isTutorialFinished BOOL to YES
    self.isTutorialFinished = YES;
    
    //Here set the offline tutorial mode to begin
    // Set the current event as tutorial for logging purpose
    [[EventLogger sharedEventLogger] setEventType:LOG_EVENT_TUTORIAL];
    
    // Send Event Logs to server
    [[EventLogger sharedEventLogger] sendLogs];
    
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
    {
        //Remove the Tutorial ViewController's view from superview
        [self.tutorialViewController.view removeFromSuperview];
        SAFE_RELEASE(tutorialViewController);
        
        // Save state for Online Exam
        [self saveCurrentExamState:ERB_EXAM_STATE_BEGIN_SECTION_1];
        
        // Add the Pre-Exam Pause Screen
        [self addReusableViewForExamState:ERB_EXAM_STATE_BEGIN_SECTION_1];
    }
    else
    {
        //Remove the Tutorial ViewController's view from superview
        [self.tutorialViewControllerOffline.view removeFromSuperview];
        SAFE_RELEASE(tutorialViewControllerOffline);
        if (!beginSectionViewController)
        {
            // Save state for Online Exam
            [self saveCurrentExamState:ERB_EXAM_STATE_EXAM];
            //Push the beginSectionViewController
            beginSectionViewController = [[ERBBeginSectionViewController alloc] init];
            
            //Set the previous question iVar, this is to be sent in the webservice for question_id
            beginSectionViewController.previousQuestion = [self.savedQuestionsArray lastObject];
            
            beginSectionViewController.delegate = self;
            beginSectionViewController.sectionId = VERBAL_REASONING;
            beginSectionViewController.checkinData = self.checkInData;
            shouldHideStatusBar = YES;
            [self updateStatusBar];
            [self.view addSubview:beginSectionViewController.view];
        }
    }
}

#pragma mark - ERBBeginSectionViewControllerDelegate

//Called when student taps on begin test/ next section in begin section view
- (void) section:(int)section didBegin:(BOOL)didBegin withCheckinData:(ERBCheckIn *)data withQuestion:(ERBQuestion *)question
{
    if (didBegin)
    {
        //Conditional coding accoirding to mode
        if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
        {
            //Set the current exam progress
            self.currentExamProgress = section;
            
            //Here, check if the count is 2, if count is not 2, that means this is the first section.
            //If count is already 2, that means this is a subsequent section, in that case, remove the first object.
            if (self.savedQuestionsArray.count == 2)
            {
                [self.savedQuestionsArray removeObjectAtIndex:0];
            }
            
            [self.savedQuestionsArray addObject:question];
            
            // Save the exam state object for Online State retention
            if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE]) {
                
                NSData *encodedObject = [USER_DEFAULTS objectForKey:KEY_EXAM_STATE];
                ERBExamState *examState = nil;
                
                if (encodedObject) {
                    examState = (ERBExamState *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
                }
                
                if (!examState) {
                    examState = [[[ERBExamState alloc] init] autorelease];
                }
                
                examState.currentSection = section;
                examState.currentQuestionNumber = 1;
                examState.questionsArray = self.savedQuestionsArray;
                examState.checkinData = data;
                examState.currentExamState = ERB_EXAM_STATE_EXAM;
                examState.currentExamProgress = self.currentExamProgress;
                
                encodedObject = [NSKeyedArchiver archivedDataWithRootObject:examState];
                SAVE_USER_DEFAULTS_AND_SYNC(encodedObject, KEY_EXAM_STATE);
            }
            
            SAFE_RELEASE(examViewController);
            
            //Push the examViewController
            examViewController = [[ERBExamViewController alloc] init];
            examViewController.section_id = section;
            
            //This question_ is for counting the number of questions that has been loaded per section. Never confuse this with the one coming from the web service.
            examViewController.questionsCount = 1;
            examViewController.checkinData = data;
            examViewController.questionsArray = self.savedQuestionsArray;
            examViewController.delegate = self;
            shouldHideStatusBar = YES;
            [self updateStatusBar];
            [self.view addSubview:examViewController.view];
            examViewController.view.hidden = YES;
        }
        else if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_OFFLINE])
        {
            if (self.isExamResumed)
            {
                //Reset the flag
                self.isExamResumed = NO;
                SAFE_RELEASE(examViewControllerOffline);
                
                //Push the examViewController
                examViewControllerOffline = [[ERBExamViewControllerOffline alloc] initWithNibName:@"ERBExamViewControllerOffline" bundle:nil];
                
                //Set the questions array
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                NSData *data = [def objectForKey:KEY_OFFLINE_QUESTIONS];
                NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                examViewControllerOffline.allQuestionsArray = array;
                examViewControllerOffline.section_id = self.currentExamProgress;
                
                NSDictionary *offlineResponses = [NSDictionary dictionaryWithContentsOfFile:[[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT] stringByAppendingPathComponent:FILE_NAME_QUESTION_RESPONSES]];
                
                //Make ERBCheckin
                ERBCheckIn *checkIn = [[ERBCheckIn alloc] init];
                checkIn.examId = [offlineResponses objectForKey:REQUEST_POST_KEY_EXAM_ID];
                checkIn.studentId = [offlineResponses objectForKey:RESPONSE_KEY_STUDENT_ID];
                checkIn.studentName = [offlineResponses objectForKey:RESPONSE_KEY_STUDENT_NAME];
                
                examViewControllerOffline.checkinData = checkIn;
                examViewControllerOffline.isExamResumed = YES;
                [checkIn release]; //Released ..
                //Get the responses array
                NSArray *responses = [offlineResponses objectForKey:@"responses"];
                examViewControllerOffline.questionsCount = responses.count;
                
                examViewControllerOffline.delegate = self;
                
                [self.view addSubview:examViewControllerOffline.view];
                examViewControllerOffline.view.hidden = YES;
            }
            else
            {
                //Set the current exam progress
                self.currentExamProgress = section;
                
                SAFE_RELEASE(examViewControllerOffline);
                
                //Push the examViewController
                examViewControllerOffline = [[ERBExamViewControllerOffline alloc] initWithNibName:@"ERBExamViewControllerOffline" bundle:nil];
                
                //Set the questions array
                examViewControllerOffline.allQuestionsArray = self.allQuestionsArray;
                
                examViewControllerOffline.section_id = section;
                
                //This question_ is for counting the number of questions that has been loaded per section. Never confuse this with the one coming from the web service.
                examViewControllerOffline.questionsCount = QUESTION_LIMIT_PER_SECTION*(section-1);
                examViewControllerOffline.checkinData = data;
                examViewControllerOffline.delegate = self;
                [self.view addSubview:examViewControllerOffline.view];
                examViewControllerOffline.view.hidden = YES;
            }
        }
    }
}

#pragma mark - ERBExamViewControllerDelegate
//For white screen fix
- (void) questionDidLoadSuccessfullyForFirstTime
{
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
    {
        [self.beginSectionViewController.view setUserInteractionEnabled:YES];
        [self.beginSectionViewController.activityIndicator stopAnimating];
        
        examViewController.view.hidden = NO;
        
        //Play question audio for first question
        [examViewController playQuestionAudioAtIndex:[NSNumber numberWithInteger:0]];
        
        
        //Remove the beginSectionViewController's view from superview
        [self.beginSectionViewController.view removeFromSuperview];
        SAFE_RELEASE(beginSectionViewController);
    }
    else if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_OFFLINE])
    {
        //Here, stop the polling as it is offline mode
        [self stopPolling];
        
        [self.beginSectionViewController.view setUserInteractionEnabled:YES];
        [self.beginSectionViewController.activityIndicator stopAnimating];
        
        examViewControllerOffline.view.hidden = NO;
        
        //Play question audio for first question
        [examViewControllerOffline playQuestionAudioAtIndex:[NSNumber numberWithInteger:0]];
        
        
        //Remove the beginSectionViewController's view from superview
        [self.beginSectionViewController.view removeFromSuperview];
        
        //TO DO: SAFELY RELEASE THIS< was causing a crash, look into it..
        //        SAFE_RELEASE(beginSectionViewController);
        
        [ERBCommons applyFadeAnimationTo:self.view.window withDuration:1.0];
        
        //Remove the loginViewController's view from superview
        [self.loginViewController.view removeFromSuperview];
        SAFE_RELEASE(loginViewController);
        
    }
}

//Called when student taps on next button for last question of a section
- (void)didEndSection:(int)section withData:(ERBCheckIn *)data withQuestion: (ERBQuestion *)question
{
    // Set the current event as exam for logging purpose
    [[EventLogger sharedEventLogger] setEventType:LOG_EVENT_EXAM];
    
    // Send Event Logs to server
    [[EventLogger sharedEventLogger] sendLogs];
    
    //Remove the examViewController'view's view from superview
    [self.examViewController.view removeFromSuperview];
    
    SAFE_RELEASE(examViewController);
    
    if (section == QUANTITATIVE_REASONING) {
        
        //Set the current exam progress
        self.currentExamProgress = EXAM_PROGRESS_BREAK;
        
        SAFE_RELEASE(breakViewController);
        //Push the breakViewController
        breakViewController = [[ERBBreakViewController alloc] init];
        
        //Set the student name
        breakViewController.studentName = self.checkInData.studentName;
        breakViewController.previousQuestion = question;
        breakViewController.delegate = self;
        [self.view addSubview:breakViewController.view];
        
        if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_OFFLINE])
        {
            //Remove break after 60 second
            [self performSelector:@selector(didEndBreak:withQuestion:) withObject:nil afterDelay:OFFLINE_BREAK_TIME];
        }
    }
    else if (section == MATH) {
        
        // END OF EXAM
        
        //Stop the polling for get exam status
        [self stopPolling];
        
        if (reusableViewController)
        {
            [self.reusableViewController.view removeFromSuperview];
            SAFE_RELEASE(reusableViewController);
        }
        
        // Add the Exam End Screen
        [self addReusableViewForExamState:ERB_EXAM_STATE_END];
        
    }
    else
    {
        SAFE_RELEASE(beginSectionViewController);
        //Push the beginSectionViewController
        beginSectionViewController = [[ERBBeginSectionViewController alloc] init];
        beginSectionViewController.delegate = self;
        beginSectionViewController.sectionId = section + 1;
        beginSectionViewController.checkinData = self.checkInData;
        
        //If offline mode, don't set question
        if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
        {
            beginSectionViewController.previousQuestion = question;
        }
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
        
        shouldHideStatusBar = YES;
        [self updateStatusBar];
        [self.view addSubview:beginSectionViewController.view];
    }
}

#pragma mark - ERBBreakViewControllerDelegate

//Called when the break ends
- (void) didEndBreak:(BOOL)didEnd withQuestion: (ERBQuestion *)question
{
    // Save state for Online Exam
    [self saveCurrentExamState:ERB_EXAM_STATE_BREAK_END];
    //Remove the breakViewController's view's view from superview
    [self.breakViewController.view removeFromSuperview];
    SAFE_RELEASE(breakViewController);
    
    SAFE_RELEASE(beginSectionViewController);
    beginSectionViewController = [[ERBBeginSectionViewController alloc] init];
    beginSectionViewController.delegate = self;
    beginSectionViewController.sectionId = EARLY_LITERACY;
    //If offline mode, dont set question
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
    {
        beginSectionViewController.previousQuestion = question;
    }
    beginSectionViewController.checkinData = self.checkInData;
    shouldHideStatusBar = YES;
    [self updateStatusBar];
    [self.view addSubview:beginSectionViewController.view];
}

#pragma mark - ERBReusableViewControllerDelegate

//Called when the exam ends
- (void)examDidEnd:(BOOL)didEnd{
    
    if (didEnd)
    {
        SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithBool:YES], KEY_DID_EXAM_END);
        // Set the current event as exam for logging purpose
        [[EventLogger sharedEventLogger] setEventType:LOG_EVENT_EXAM];
        
        // Added log to check random logging out bug
        [self sendLogOutDetailsForEvent:@"LOGOUT-EXAM_END"];
        
        // Remove all the other view controller's subviews from begin tutorial view controller's view
        [self removeAllSubviews];
        
        [self releaseAllObjects];
        
        [self resetAndLaunchLogin];
        
    }
}


#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - ERBWebOperationDelegate

- (void)webServiceDidReceiveResponse:(NSData *)responseData requestIdentifier:(NSString *)requestId operationStatus:(WEB_SERVICE_OPERATION_STATUS)status
{
    NSError *error = nil;
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    if (!error) {
        //Check request Id
        if ([requestId isEqualToString:REQUEST_ID_GET_EXAM_STATUS])
        {
            if ([[response objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                
                if ([[response objectForKey:@"status"] isEqualToString:EXAM_STATE_BEGIN_TUTORIAL])
                {
                    if (self.reusableViewController && self.reusableViewController.examState == ERB_EXAM_STATE_PAUSE)
                    {
                        // GLOBAL PAUSE ENDS
                        [self onPauseEnd];
                    }
                    if (!self.tutorialViewController && !self.isTutorialFinished) {
                        [self startTutorial];
                    }
                }
                else if ([[response objectForKey:@"status"] isEqualToString:EXAM_STATE_BEGIN_SECTION_1])
                {
                    if (self.reusableViewController && self.reusableViewController.examState == ERB_EXAM_STATE_PAUSE)
                    {
                        // GLOBAL PAUSE ENDS
                        [self onPauseEnd];
                    }
                    else if (self.reusableViewController && self.reusableViewController.examState == ERB_EXAM_STATE_BEGIN_SECTION_1) {
                        // Remove reusable view controller from superview and safe release its object
                        [self.reusableViewController.view removeFromSuperview];
                        SAFE_RELEASE(reusableViewController);
                    }
                    
                    if (!beginSectionViewController)
                    {
                        // Save state for Online Exam
                        [self saveCurrentExamState:ERB_EXAM_STATE_EXAM];
                        //Push the beginSectionViewController
                        beginSectionViewController = [[ERBBeginSectionViewController alloc] init];
                        
                        //Set the previous question iVar, this is to be sent in the webservice for question_id
                        beginSectionViewController.previousQuestion = [self.savedQuestionsArray lastObject];
                        
                        beginSectionViewController.delegate = self;
                        beginSectionViewController.sectionId = VERBAL_REASONING;
                        beginSectionViewController.checkinData = self.checkInData;
                        shouldHideStatusBar = YES;
                        [self updateStatusBar];
                        [self.view addSubview:beginSectionViewController.view];
                    }
                    else
                    {
                        [self.view bringSubviewToFront:beginSectionViewController.view];
                    }
                }
                else if ([[response objectForKey:@"status"] isEqualToString:EXAM_STATE_BEGIN_SECTION_2])
                {
                    if (self.reusableViewController && self.reusableViewController.examState == ERB_EXAM_STATE_PAUSE)
                    {
                        // GLOBAL PAUSE ENDS
                        [self onPauseEnd];
                    }
                    
                    if (self.breakViewController) {
                        [self.breakViewController stopTimer];
                    }
                    
                }
                else if ([[response objectForKey:@"status"] isEqualToString:EXAM_STATE_PAUSED]) {
                    
                    // check if resusable view controller exists and it is not the one added after tutorial ends
                    if (self.reusableViewController){
                        if(self.reusableViewController.examState == ERB_EXAM_STATE_PAUSE) {
                            [self.view bringSubviewToFront:self.reusableViewController.view];
                        }
                    }
                    else{
                        // GLOBAL PAUSE STARTS
                        [self onPauseStart];
                    }
                }
                else if ([[response objectForKey:@"status"] isEqualToString:EXAM_STATE_ACTIVE]) {
                    if (self.reusableViewController && self.reusableViewController.examState == ERB_EXAM_STATE_PAUSE)
                    {
                        // GLOBAL PAUSE ENDS
                        [self onPauseEnd];
                    }
                }
                else if ([[response objectForKey:@"status"] isEqualToString:EXAM_STATE_STOPPED]) {
                    
                    // END OF EXAM
                    
                    //Stop the polling for get exam status
                    [self stopPolling];
                    
                    // Stop the audio
                    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                    
                    // Post notification to notify end of exam
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EXAM_STOPPED object:nil];
                    
                    // Send the event logs
                    [[EventLogger sharedEventLogger] sendLogs];
                    
                    // Remove all the other view controller's subviews from begin tutorial view controller's view
                    [self removeAllSubviews];
                    
                    // Add the Exam End Screen
                    [self addReusableViewForExamState: ERB_EXAM_STATE_END];
                    
                    // Remove all data
                    [ERBCommons clearUserDefaults];
                    [ERBCommons removeOfflineContent];
                    
                }
                else if ([[response objectForKey:@"status"] isEqualToString:EXAM_STATE_RESET])
                {
                    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
                    {
                        //Stop the polling for get exam status
                        [self stopPolling];
                        
                        // Stop the audio
                        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                        
                        // Post notification to notify end of exam
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EXAM_STOPPED object:nil];
                        
                        // Send the event logs
                        [[EventLogger sharedEventLogger] sendLogs];
                        
                        self.isTutorialFinished = NO;
                        shouldHideStatusBar = NO;
                        [self updateStatusBar];
                        if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0) {
                            self.view.frame = [[UIScreen mainScreen] applicationFrame];
                        }
                        
                        // Remove all the other view controller's subviews from begin tutorial view controller's view
                        [self removeAllSubviews];
                        // Safely release all the objects
                        [self releaseAllObjects];
                        
                        //Push the Schedule ViewController
                        SAFE_RELEASE(scheduleViewController);
                        scheduleViewController = [[ERBScheduleViewController alloc] init];
                        scheduleViewController.delegate = self;
                        scheduleViewController.scheduleArray = self.loginData;
                        [self.view addSubview:scheduleViewController.view];
                        
                        // Remove all data
                        [ERBCommons clearUserDefaults];
                        [ERBCommons removeOfflineContent];
                    }
                }
                
            }
            
        }
        else if ([requestId isEqualToString:REQUEST_ID_REPORT_USER_ACTION])
        {
            if ([[response objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                CANCEL_CONNECTION(self.userActionWebServiceHelper);
                
                //Save the questions dict in the iVar
                if ([[response  objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
                {
                    
                    if (!savedQuestionsArray)
                    {
                        savedQuestionsArray = [[NSMutableArray alloc] init];
                        ERBQuestion *questionToSave = [[ERBQuestion alloc] init];
                        questionToSave.question = [response objectForKey:RESPONSE_KEY_QUESTION];
                        questionToSave.questionId = [response objectForKey:REQUEST_POST_KEY_QUESTION_ID];
                        [savedQuestionsArray addObject:questionToSave];
                        [questionToSave release];
                    }
                    
                    // Save state for Online Exam
                    [self saveCurrentExamState:ERB_EXAM_STATE_PRE_TUTORIAL_PAUSE];
                    
                    // Add the Pre-Tutorial Pause Screen
                    [self addReusableViewForExamState: ERB_EXAM_STATE_PRE_TUTORIAL_PAUSE];
                    
                    [self releaseCheckinAndScheduleViewControllers];
                    
                    // Send Event Logs to server
                    [[EventLogger sharedEventLogger] sendLogs];
                    
                }
                else
                {
                    [ERBCommons showAlert:[response  objectForKey:RESPONSE_KEY_ERROR_MESSAGE] delegate:nil];
                    [self enableButtons];
                }
                [self.activityIndicator stopAnimating];
            }
            
        }
        else if ([requestId isEqualToString:REQUEST_ID_STUDENT_CHECKIN_WS])
        {
            if ([[response objectForKey:RESPONSE_KEY_CODE] integerValue] == 1)
            {
                //Enable user interaction.
                self.view.userInteractionEnabled = YES;
                
                [self.checkInViewController callWebServiceToRefreshData];
                
                //Show the status bar and fix 20 pixel issue
                shouldHideStatusBar = NO;
                [self updateStatusBar];
                self.checkInViewController.view.frame = CGRectMake(0, 0, 1024, 768);
                [self.view bringSubviewToFront:self.checkInViewController.view];
                self.scheduleViewController.view.frame = CGRectMake(0, 0, 1024, 768);
                
                [self.activityIndicator stopAnimating];
            }
        }
        else if ([requestId isEqualToString:REQUEST_ID_GET_ASSETS_DOWNLOAD]){
            
            if ([[response  objectForKey:RESPONSE_KEY_CODE] integerValue] == 1){
                [self.activityIndicator stopAnimating];
                self.downloadData = response;
#ifdef LOCAL
                if ([[ERBCommons getCurrentAssetsRevision] intValue] == [[response objectForKey:RESPONSE_KEY_CURRENT_REVISION] intValue]) {
                    [self downloadsDidFinishSucessfully];
                }
                else {
                    [ERBCommons showAlert:@"Assets are not updated. Please update them to continue." delegate:self btnTitle:@"Cancel" otherBtnTitle:@"Update" tag:2];
                }
#else
                [ERBCommons showAlert:@"Assets are not updated. Please update them to continue." delegate:self btnTitle:@"Cancel" otherBtnTitle:@"Update" tag:2];
#endif
            }
            // Error code 2 indicates that resources are updated in app and at same svn rev as on server
            else if ([[response  objectForKey:RESPONSE_KEY_CODE] integerValue] == 2){
                [self downloadsDidFinishSucessfully];
            }
            else{
                [self enableLoginView];
                [self.loginViewController.view setUserInteractionEnabled:YES];
                [ERBCommons showAlert:@"Some error occured while checking for asset updates. Please retry to continue further." delegate:self btnTitle:@"Cancel" otherBtnTitle:@"Retry" tag:1];
            }
        }
        
    }
    else{
        DLog(@"Error while parsing JSON response for WS: %@", requestId);
    }
    
}

- (void) webServiceDidFailWithError:(NSError *)error requestIdentifier:(NSString *)requestId
{
    if ([requestId isEqualToString:REQUEST_ID_REPORT_USER_ACTION])
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
        [self enableButtons];
    }
    else if ([requestId isEqualToString:REQUEST_ID_STUDENT_CHECKIN_WS])
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
        
        //Enable user interaction.
        self.view.userInteractionEnabled = YES;
        [self.activityIndicator stopAnimating];
        
        //Start Polling again
        [self startPolling];
    }
    else if ([requestId isEqualToString:REQUEST_ID_GET_ASSETS_DOWNLOAD]){
        
        [self enableLoginView];
        [self.loginViewController.view setUserInteractionEnabled:YES];
        [ERBCommons showAlert:@"Some error occured while checking for asset updates. Please retry to continue further." delegate:self btnTitle:@"Cancel" otherBtnTitle:@"Retry" tag:1];
    }
    
    [self.activityIndicator stopAnimating];
}

#pragma mark - UIAlertViewDelegate Methods

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            [self callGetAssetsWebServiceForDownload];
        }
    }
    else if (alertView.tag == 2){
        if (buttonIndex == 1) {
            [self loadDownloadScreen];
        }
        else {
            [self enableLoginView];
        }
    }
    
}

#pragma mark - Private Methods

- (void)guidedAccessChanged:(NSNotification *)notice{
    if (!UIAccessibilityIsGuidedAccessEnabled()) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EXAM_STOPPED object:nil];
        // Stop the audio
        CANCEL_CONNECTION(self.userActionWebServiceHelper);
        CANCEL_CONNECTION(self.examStatusWebServiceHelper);
        CANCEL_CONNECTION(self.checkinWebServiceHelper);
        CANCEL_CONNECTION(self.assetsWebServiceHelper);
        [self stopPolling];
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [self releaseAllObjects];
        [self removeAllSubviews];
    }
}

- (void) releaseCheckinAndScheduleViewControllers
{
    // Remove the checkinViewController's view from super view and release it
    [self.checkInViewController.view removeFromSuperview];
    SAFE_RELEASE(checkInViewController);
    
    //Remove the Schedule ViewController's view from superview
    [self.scheduleViewController.view removeFromSuperview];
    SAFE_RELEASE(scheduleViewController);
}

- (void) onPauseStart{
    
    // Stop playing the audio
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    // Disable view's user interaction so that touch events are nor passed on to subviews
    [self.view setUserInteractionEnabled:NO];
    
    // Post notification to notify end of global pause
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GLOBAL_PAUSE_STARTED object:nil];
    
    // Add Exam Pause Screen
    [self addReusableViewForExamState: ERB_EXAM_STATE_PAUSE];
    
    // Exam can be paused during tutorial or in between exam, save the state for both scenarios
    if (self.currentExamProgress == EXAM_PROGRESS_TUTORIAL) {
        [self saveCurrentExamState:ERB_EXAM_STATE_PAUSE_TUTORIAL];
    }
    else if (self.currentExamProgress >= EXAM_PROGRESS_SECTION_ONE && self.currentExamProgress <= EXAM_PROGRESS_SECTION_FOUR) {
        [self saveCurrentExamState:ERB_EXAM_STATE_PAUSE_EXAM];
    }
    
    // set the pause start time parameter
    self.pauseStartTime = [[NSDate date] timeIntervalSince1970];
}

- (void) onPauseEnd{
    // set the pause end time parameter
    self.pauseEndTime = [[NSDate date] timeIntervalSince1970];
    
    // Enable view's userinteraction
    [self.view setUserInteractionEnabled:YES];
    
    // Remove reusable view controller from superview and safe release its object
    if (reusableViewController)
    {
        [self.reusableViewController.view removeFromSuperview];
        SAFE_RELEASE(reusableViewController);
    }
    // Post notification to notify end of global pause
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GLOBAL_PAUSE_ENDED object:nil];
    
    // Exam can be paused during tutorial or in between exam, save the state for both scenarios
    if (self.currentExamProgress == EXAM_PROGRESS_TUTORIAL) {
        [self saveCurrentExamState:ERB_EXAM_STATE_TUTORIAL];
    }
    else if (self.currentExamProgress >= EXAM_PROGRESS_SECTION_ONE && self.currentExamProgress <= EXAM_PROGRESS_SECTION_FOUR) {
        [self saveCurrentExamState:ERB_EXAM_STATE_EXAM];
    }
    
}

- (void) uncheckStudent
{
    NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN, REQUEST_POST_KEY_EXAM_ID, RESPONSE_KEY_STUDENT_ID, REQUEST_POST_KEY_CHECK_IN, REQUEST_POST_KEY_RECOVER, nil];
    
    ERBCheckIn *checkedInStudentData = self.checkInViewController.checkinData;
    
    // For normal uncheck
    NSString *shouldRecover = @"FALSE";
    
    // For unchecking when admin no longer wants to continue a student's incomplete exam
    if (!checkedInStudentData) {
        
        if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE]){
            NSData *encodedObject = [USER_DEFAULTS objectForKey:KEY_EXAM_STATE];
            ERBExamState *examState = nil;
            
            if (encodedObject) {
                examState = (ERBExamState *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
            }
            
            if (examState) {
                // Initialize the check in data object
                checkedInStudentData = examState.checkinData;
                
                shouldRecover = @"TRUE";
            }
        }
    }
    
    NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken], checkedInStudentData.examId, checkedInStudentData.studentId, @"NO", shouldRecover, nil];
    NSDictionary *paramsDict = nil;
    
    @try {
        paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    if (self.checkinWebServiceHelper)
    {
        CANCEL_CONNECTION(self.checkinWebServiceHelper);
        
        [checkinWebServiceHelper release];
        checkinWebServiceHelper = nil;
    }
    
    checkinWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_CHECK_IN_PATH]] withParams:paramsDict withRequestIdentifier:REQUEST_ID_STUDENT_CHECKIN_WS withHttpMethod:ERB_REQUEST_POST];
    [paramsDict release];
    self.checkinWebServiceHelper.delegate = self;
    [self.checkinWebServiceHelper sendWebServiceRequest];
    
    self.activityIndicator.center = self.view.center;
    [self.activityIndicator startAnimating];
    [self.view bringSubviewToFront:self.activityIndicator];
}

- (void) removeAllSubviews
{
    // Remove all the other view controller's subviews from begin tutorial view controller's view
    for (UIView *view in self.view.subviews)
    {
        if ([view isMemberOfClass:[UIView class]] && view.tag !=  VOL_VIEW_CONTROLLER_TAG)
        {
            [view removeFromSuperview];
        }
    }
}

- (void) releaseAllObjects{
    SAFE_RELEASE(loginViewController);
    SAFE_RELEASE(scheduleViewController);
    SAFE_RELEASE(checkInViewController);
    SAFE_RELEASE(tutorialViewController);
    SAFE_RELEASE(tutorialViewControllerOffline);
    SAFE_RELEASE(beginSectionViewController);
    SAFE_RELEASE(examViewController);
    SAFE_RELEASE(examViewControllerOffline);
    SAFE_RELEASE(breakViewController);
    SAFE_RELEASE(reusableViewController);
    SAFE_RELEASE(savedQuestionsArray);
}

// Called when the tutorial starts
- (void) startTutorial
{
    [self saveCurrentExamState:ERB_EXAM_STATE_BEGIN_TUTORIAL];
    
    if (reusableViewController)
    {
        [self.reusableViewController.view removeFromSuperview];
        SAFE_RELEASE(reusableViewController);
    }
    
    self.isTutorialFinished = NO;
    
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE])
    {
        SAFE_RELEASE(tutorialViewController);
        //Add the tutorialViewController as a subview.
        tutorialViewController = [[ERBTutorialViewController alloc] init];
        self.tutorialViewController.delegate = self;
        self.tutorialViewController.checkInData = self.checkInData;
        [self.view addSubview: self.tutorialViewController.view];
    }
    else
    {
        SAFE_RELEASE(tutorialViewControllerOffline);
        //Add the tutorialViewController as a subview.
        tutorialViewControllerOffline = [[ERBTutorialViewControllerOffline alloc] init];
        self.tutorialViewControllerOffline.delegate = self;
        self.tutorialViewControllerOffline.checkInData = self.checkInData;
        [self.view addSubview: self.tutorialViewControllerOffline.view];
    }
}

- (void) disableButtons
{
    self.beginTutorialButton.enabled = NO;
    self.backButton.enabled = NO;
    self.volumeViewController.view.userInteractionEnabled = NO;
    self.beginTutorialButton.userInteractionEnabled = NO;
}

- (void) enableButtons
{
    self.beginTutorialButton.enabled = YES;
    self.backButton.enabled = YES;
    self.volumeViewController.view.userInteractionEnabled = YES;
    self.beginTutorialButton.userInteractionEnabled = YES;
    
}

- (void) customizeUI
{
    self.putHeadphonesLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:45.0];
    self.studentNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    self.beginTutorialLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    
    SAFE_RELEASE(volumeViewController);
    volumeViewController = [[AudioVolumeViewController alloc] init];
    volumeViewController.view.frame = CGRectMake(358, 583, 300, 127);
    [self.view addSubview:volumeViewController.view];
    
}

//Reports admin web panel about beginning of tutorial
- (void)callReportUserActionWebService
{
    if ([[ERBCommons appController] internetCheck])
    {
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN,
                         REQUEST_POST_KEY_EXAM_ID,
                         REQUEST_POST_KEY_SECTION_ID,
                         REQUEST_POST_KEY_STUDENT_ID,
                         REQUEST_POST_KEY_ACTION,
                         REQUEST_POST_KEY_QUESTION_ID,
                         REQUEST_POST_KEY_RESPONSE,  nil];
        
        NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken],
                           self.checkInData.examId,
                           @"0",
                           self.checkInData.studentId,
                           USER_ACTION_TUTORIAL_START,
                           @"0",
                           @"",nil];
        
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if (self.userActionWebServiceHelper) {
            CANCEL_CONNECTION(self.userActionWebServiceHelper);
            [userActionWebServiceHelper release];
            userActionWebServiceHelper = nil;
        }
        
        if (paramsDict) {
            userActionWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_RECORD_USER_ACTION]]
                                                                          withParams:paramsDict
                                                               withRequestIdentifier:REQUEST_ID_REPORT_USER_ACTION
                                                                      withHttpMethod:ERB_REQUEST_POST];
            [paramsDict release];
            
            self.userActionWebServiceHelper.delegate = self;
            [self.userActionWebServiceHelper sendWebServiceRequest];
            
            //We set the user interaction to NO, to resolve the accidental button click.
            [self disableButtons];
        }
        
    }
    else
    {
        [ERBCommons showAlert:ERBLocalizedString(LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN) delegate:nil];
    }
}

- (void) callGetExamStatusWebService {
    
    if ([[ERBCommons appController] internetCheck])
    {
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN,
                         REQUEST_POST_KEY_EXAM_ID, REQUEST_POST_KEY_STUDENT_ID, REQUEST_POST_KEY_STUDENT_PROGRESS_IDENTIFIER, nil];
        
        NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken],
                           self.checkInData.examId, self.checkInData.studentId, [NSString stringWithFormat:@"%d",self.currentExamProgress], nil];
        
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if (self.examStatusWebServiceHelper) {
            CANCEL_CONNECTION(self.examStatusWebServiceHelper);
            [examStatusWebServiceHelper release];
            examStatusWebServiceHelper = nil;
        }
        
        if (paramsDict) {
            examStatusWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_GET_EXAM_STATUS_PATH]]
                                                                          withParams:paramsDict
                                                               withRequestIdentifier:REQUEST_ID_GET_EXAM_STATUS
                                                                      withHttpMethod:ERB_REQUEST_POST];
            [paramsDict release];
            
            self.examStatusWebServiceHelper.delegate = self;
            [self.examStatusWebServiceHelper sendWebServiceRequest];
        }
    }
}

- (void) callGetAssetsWebServiceForDownload{
    if ([[ERBCommons appController] internetCheck])
    {
        NSArray *keys = [NSArray arrayWithObjects:REQUEST_POST_KEY_AUTH_TOKEN, REQUEST_POST_KEY_CURRENT_ASSET_REVISION,
                         REQUEST_POST_KEY_LOCAL, nil];
        
        NSString *local = @"0";
        
        // This is for local asset download
#ifdef LOCAL
        local = @"1";
#endif
        
        NSArray *values = [NSArray arrayWithObjects:[ERBCommons authToken], [ERBCommons getCurrentAssetsRevision], local, nil];
        
        NSDictionary *paramsDict = nil;
        
        @try {
            paramsDict = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if (self.assetsWebServiceHelper) {
            CANCEL_CONNECTION(self.assetsWebServiceHelper);
            [assetsWebServiceHelper release];
            assetsWebServiceHelper = nil;
        }
        
        if (paramsDict) {
            assetsWebServiceHelper = [[ERBWebOperation alloc] initWithRequestURL:[NSURL URLWithString:[ERB_URL_BASE_PATH stringByAppendingString:ERB_URL_DOWNLOAD_ASSETS_PATH]]
                                                                      withParams:paramsDict
                                                           withRequestIdentifier:REQUEST_ID_GET_ASSETS_DOWNLOAD
                                                                  withHttpMethod:ERB_REQUEST_POST];
            [paramsDict release];
            
            self.assetsWebServiceHelper.delegate = self;
            [self.assetsWebServiceHelper sendWebServiceRequest];
        }
    }
    
}

- (void) startPolling{
    timer = [NSTimer scheduledTimerWithTimeInterval: TIME_GET_EXAM_STATUS target: self selector:@selector(callGetExamStatusWebService) userInfo: nil repeats:YES];
}

- (void) stopPolling{
    [timer invalidate];
    timer = nil;
}

- (void) resetAndLaunchLogin {
    
    // Enable user interaction of the view
    [self.view setUserInteractionEnabled:YES];
    
    self.beginTutorialButton.exclusiveTouch = YES;
    self.isTutorialFinished = NO;
    self.pauseEndTime = 0.0;
    self.pauseStartTime = 0.0;
    
    //Add the loginViewController as a subview.
    if (loginViewController) {
        SAFE_RELEASE(loginViewController); //Released ..
    }
    loginViewController = [[ERBLoginViewController alloc] init];
    self.loginViewController.delegate = self;
    
    //Show the status bar and fix 20 pixel issue
    //    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    self.loginViewController.view.frame = CGRectMake(0, 0, 1024, 768);
    shouldHideStatusBar = NO;
    [self updateStatusBar];
    [self.view addSubview: self.loginViewController.view];
}

- (void) addReusableViewForExamState:(ERB_EXAM_STATE)examState{
    
    SAFE_RELEASE(reusableViewController);
    // Initialize and add reusable view controller for given exam state
    reusableViewController = [[ERBReusableViewController alloc] init];
    self.reusableViewController.studentName = self.checkInData.studentName;//self.studentNameLabel.text;
    self.reusableViewController.examState = examState;
    self.reusableViewController.delegate = self;
    [self.view addSubview:self.reusableViewController.view];
    
}

- (void)sendLogOutDetailsForEvent:(NSString *)event{
    
    NSMutableDictionary *response = [[[NSMutableDictionary alloc] init] autorelease];
    [response setObject:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:@"TIMESTAMP"];
    [response setObject:event forKey:@"LOG-OUT"];
    [[EventLogger sharedEventLogger] saveLog:response];
    
    // Send Event Logs to server
    [[EventLogger sharedEventLogger] sendLogs];
}

- (void) handleOnlineStateMaintenance{
    
    NSData *encodedObject = [USER_DEFAULTS objectForKey:KEY_EXAM_STATE];
    ERBExamState *examState = nil;
    if (encodedObject) {
        examState = (ERBExamState *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    }
    
    if (examState) {
        
        // Initialize the desired parameters
        self.checkInData = examState.checkinData;
        self.currentExamProgress = examState.currentExamProgress;
        self.savedQuestionsArray = examState.questionsArray;
        
        switch (examState.currentExamState) {
            case ERB_EXAM_STATE_EXAM:
            {
                [self loadOnlineExamForState:examState];
            }
                break;
                
            case ERB_EXAM_STATE_BEGIN_TUTORIAL:
            {
                [self startTutorial];
            }
                break;
                
            case ERB_EXAM_STATE_PAUSE:
            {
                [self onPauseStart];
            }
                break;
                
            case ERB_EXAM_STATE_PAUSE_TUTORIAL:
            {
                [self startTutorial];
                [self onPauseStart];
            }
                break;
                
            case ERB_EXAM_STATE_PAUSE_EXAM:
            {
                [self loadOnlineExamForState:examState];
                [self onPauseStart];
            }
                break;
                
            case ERB_EXAM_STATE_BREAK_END:
            {
                [self loadOnlineExamForState:examState];
            }
                
                break;
                
            default:
            {
                [self saveCurrentExamState:examState.currentExamState];
                [self addReusableViewForExamState:examState.currentExamState];
            }
                break;
        }
        
        // Start the polling process
        [self startPolling];
    }
    
}
- (void) saveCurrentExamState:(ERB_EXAM_STATE)currentState{
    
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_ONLINE]) {
        NSData *encodedObject = [USER_DEFAULTS objectForKey:KEY_EXAM_STATE];
        
        ERBExamState *examState = nil;
        
        if (encodedObject) {
            examState = (ERBExamState *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        }
        
        if (!examState) {
            examState = [[[ERBExamState alloc] init] autorelease];
        }
        examState.currentExamState = currentState;
        examState.checkinData = self.checkInData;
        examState.currentExamProgress = self.currentExamProgress;
        examState.questionsArray = self.savedQuestionsArray;
        encodedObject = [NSKeyedArchiver archivedDataWithRootObject:examState];
        SAVE_USER_DEFAULTS_AND_SYNC(encodedObject, KEY_EXAM_STATE);
        
    }
    
}

- (void) loadOnlineExamForState:(ERBExamState *)examState{
    if (examState.questionsArray.count == 1) {
        if (examState.currentExamState == ERB_EXAM_STATE_BREAK_END) {
            [self didEndBreak:YES withQuestion:[examState.questionsArray lastObject]];
        }
        else{
            [self didEndSection:examState.currentSection withData:examState.checkinData withQuestion:[examState.questionsArray lastObject]];
        }
    }
    else{
        SAFE_RELEASE(examViewController);
        examViewController = [[ERBExamViewController alloc] init];
        examViewController.section_id = examState.currentSection;
        examViewController.questionsCount = (examState.currentQuestionNumber == 0) ? 1 : examState.currentQuestionNumber;
        examViewController.checkinData = examState.checkinData;
        examViewController.questionsArray = examState.questionsArray;
        examViewController.delegate = self;
        examViewController.view.hidden = YES;
        [self.view addSubview:examViewController.view];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
            CGRect frame = examViewController.webView.frame;
            frame.size.height += 20;
            examViewController.webView.frame = frame;
            examViewController.webViewAlternate.frame = frame;
        }
        
        // Hack to prevent flashing of white screen when online exam is resumed
        [self performSelector:@selector(onlineExamLoadedSuccessfully) withObject:nil afterDelay:3];
    }
}

- (void) onlineExamLoadedSuccessfully{
    examViewController.view.hidden = NO;
    [examViewController playQuestionAudioAtIndex:[NSNumber numberWithInteger:0]];
}

- (void) updateStatusBar {
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:shouldHideStatusBar withAnimation:UIStatusBarAnimationFade];
    }
}

- (void) enableLoginView{
    [self.activityIndicator stopAnimating];
    [self.view bringSubviewToFront:self.activityIndicator];
    self.activityIndicator.center = self.view.center;
    self.loginViewController.loginButton.enabled = YES;
}

- (void) disableLoginView{
    [self.loginViewController.view setUserInteractionEnabled:NO];
}

- (void) loadDownloadScreen{

    SAFE_RELEASE(downloadViewController);
    //Push the Download ViewController
    downloadViewController = [[DownloadViewController alloc] init];
    self.downloadViewController.delegate = self;
    self.downloadViewController.downloadsArray = [self.downloadData objectForKey:RESPONSE_KEY_DOWNLOADS_LIST];
    self.downloadViewController.deletedItems = [self.downloadData objectForKey:RESPONSE_KEY_DELETED_FILES];
    self.downloadViewController.updatedItems = [self.downloadData objectForKey:RESPONSE_KEY_UPDATED_FILES];
    self.downloadViewController.currentRevision = [NSString stringWithFormat:@"%@", [self.downloadData objectForKey:RESPONSE_KEY_CURRENT_REVISION]];
    [self.view addSubview:self.downloadViewController.view];
    
    [self performSelector:@selector(enableLoginView) withObject:nil afterDelay:3];
    [self performSelector:@selector(disableLoginView) withObject:nil afterDelay:4];
}

#pragma mark - Action Methods
- (IBAction)beginTutorialButtonPressed:(id)sender
{
    if ([[ERBCommons getExamMode] isEqualToString:EXAM_MODE_OFFLINE])
    {
        //Set the exam mode to begin_exam
        SAVE_USER_DEFAULTS_AND_SYNC([NSNumber numberWithInteger:ERB_EXAM_STATE_TUTORIAL], KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE);
        [self disableButtons];
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        
        self.activityIndicator.center = [sender center];
        
        [self startTutorial];
    }
    else
    {
        [self disableButtons];
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        
        self.activityIndicator.center = [sender center];
        [self callReportUserActionWebService];
    }
    
}

- (IBAction)backButtonPressed:(id)sender
{
    //Disable user interaction.
    self.view.userInteractionEnabled = NO;
    [self disableButtons];
    
    //Stop the polling for get exam status
    [self stopPolling];
    
    //Uncheck the student here.
    [self uncheckStudent];
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(guidedAccessChanged:) name:@"Guided Access Disabled" object:nil];
    
    [self customizeUI];
    
    [self resetAndLaunchLogin];
    
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setSavedQuestionsArray:nil];
    [self setCheckInData:nil];
    [self setActivityIndicator:nil];
    [self setStudentNameLabel:nil];
    [self setSeatNumberLabel:nil];
    [self setPutHeadphonesLabel:nil];
    [self setBeginTutorialLabel:nil];
    [self setBeginTutorialButton:nil];
    [self setBackButton:nil];
    [super viewDidUnload];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return shouldHideStatusBar;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SAFE_RELEASE(userActionWebServiceHelper);
    SAFE_RELEASE(examStatusWebServiceHelper);
    SAFE_RELEASE(checkinWebServiceHelper);
    SAFE_RELEASE(volumeViewController);
    SAFE_RELEASE(allQuestionsArray);
    SAFE_RELEASE(downloadViewController);
    SAFE_RELEASE(examViewControllerOffline);
    SAFE_RELEASE(assetsWebServiceHelper);
    SAFE_RELEASE(loginData);
    [savedQuestionsArray release];
    [checkInData release];
    [activityIndicator release];
    [studentNameLabel release];
    [seatNumberLabel release];
    [putHeadphonesLabel release];
    [beginTutorialLabel release];
    [_beginTutorialButton release];
    [_backButton release];
    [super dealloc];
}
@end
