//
//  ERBBreakViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBQuestion.h"
#import <AVFoundation/AVFoundation.h>

@protocol ERBBreakViewControllerDelegate <NSObject>
- (void) didEndBreak:(BOOL) didEnd withQuestion: (ERBQuestion *)question;
@end

@interface ERBBreakViewController : UIViewController<AVAudioPlayerDelegate>{
    NSTimer *timer;
}

@property (retain, nonatomic) id <ERBBreakViewControllerDelegate> delegate;
@property (retain, nonatomic) IBOutlet UILabel *breakTimeLabel;
@property (nonatomic, retain) ERBQuestion *previousQuestion;
@property (retain, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (nonatomic, retain) NSString *studentName;
@property (retain, nonatomic) IBOutlet UILabel *takeBreakLabel;

- (void) stopTimer;

@end
