//
//  ERBExamViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBWebOperation.h"
#import "ERBQuestion.h"
#import "ERBCheckIn.h"
#import <AVFoundation/AVFoundation.h>

@protocol ERBExamViewControllerDelegate <NSObject>
- (void)didEndSection:(int)section withData:(ERBCheckIn *)data withQuestion: (ERBQuestion *)question;
- (void) questionDidLoadSuccessfullyForFirstTime;
@end

@interface ERBExamViewController : UIViewController<ERBWebServiceDelegate, UIWebViewDelegate, AVAudioPlayerDelegate,UIScrollViewDelegate>{
    NSTimer *sectionTimer;
    NSTimer *questionTimer;
}

- (IBAction)nextButtonPressed:(id)sender;
- (IBAction)helpButtonPressed:(id)sender;
- (IBAction)playAudioButtonPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *helpButton;
- (void) playQuestionAudioAtIndex: (NSNumber *)index;

@property (retain, nonatomic) IBOutlet UIButton *replayButton;
@property (retain, nonatomic) IBOutlet UIButton *nextButton;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIWebView *webViewAlternate;
@property (retain, nonatomic) ERBCheckIn *checkinData;
//Array that contains next questions
@property (nonatomic, retain) NSMutableArray *questionsArray;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UIImageView *sectionEndImageView;
@property (assign, nonatomic) int section_id;
@property (assign, nonatomic) int questionsCount;
@property (nonatomic, retain) NSString *questionResponse;
@property (assign, nonatomic) id <ERBExamViewControllerDelegate> delegate;

@property (nonatomic, assign) ERB_USER_ACTION currentUserAction;

@end
