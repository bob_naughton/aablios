//
//  ERBAppleReviewerExamViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBAppleReviewerExamViewController.h"
#import "AudioPlayer.h"

#define AR_QUESTION_LIMIT_PER_SECTION       6

@interface ERBAppleReviewerExamViewController ()

@property (nonatomic, assign) int  sectionId;
@property (assign, nonatomic) int questionsCount;
@property (nonatomic, assign) BOOL didPlayQuestionAudioOnce;
@property (nonatomic, assign) BOOL isOmitQuestionAudioPlayed;
@property (nonatomic, retain) NSMutableArray *allQuestionsArray;
@property (nonatomic, retain) NSMutableArray *questionsArray;
@property (nonatomic, assign) AUDIO_STATE currentAudioState;

- (void) setupSectionStartScreen;
- (void) sendBeginSectionViewToBack;
- (void) endSection;
- (void) loadQuestionUsingJson:(NSString *)json inWebView:(UIWebView *)kWebView;
- (void) playAudio;
- (void) playBeginSectionAudio;
- (void) playAnswerAudioWithAnswerID: (NSString *)answerId;
- (void) enableAnswerChoices;
- (NSString *) getJSONContentForId:(NSString *)jsonId;
- (void) endExam;

@end

@implementation ERBAppleReviewerExamViewController

#pragma mark - Private Methods

- (void) setupSectionStartScreen{
    
    // Do any additional setup after loading the view from its nib.
    self.sectionNameLabel.text = [ERBCommons getSectionTitleForSectionId:self.sectionId];
    
    //Set the arrow labels accordingly
    if (self.sectionId == VERBAL_REASONING)
    {
        self.arrowLabel.text = @"Begin";
    }
    else
    {
        self.arrowLabel.text = @"Next Section";
    }
    
    //Set the background image according to the section
    NSString *imageName = [NSString stringWithFormat:@"intro000%d.png", self.sectionId];
    self.examThemeImage.image = SET_IMAGE(imageName);
    
    // Disable the begin section button;
    self.beginSectionButton.enabled = NO;
    
    [self.view addSubview:self.beginSectionView];
    
    //Configure the audio player to play file based on current Section Id
    [self playBeginSectionAudio];
    
}

- (void) sendBeginSectionViewToBack {
    [self.beginSectionView  removeFromSuperview];
    [self.sectionEndImageView removeFromSuperview];
    [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:0]];
}

- (void) endSection {
    
    //Set the state
    self.currentAudioState = AUDIO_STATE_SECTION;
    
    self.view.userInteractionEnabled = NO;
    
    //Set the end section image and play the encouragement audio file based on section that ended
    switch (self.sectionId)
    {
        case VERBAL_REASONING:
            self.sectionEndImageView.image = SET_IMAGE(@"super_job.png");
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_PRACTICAL withDelegate:self];
            break;
        case QUANTITATIVE_REASONING:
            self.sectionEndImageView.image = SET_IMAGE(@"nice_work.png");
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_NICE_WORK withDelegate:self];
            break;
        case EARLY_LITERACY:
            self.sectionEndImageView.image = SET_IMAGE(@"great_work.png");
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_MATH withDelegate:self];
            break;
        case MATH:
            self.currentAudioState = AUDIO_STATE_NONE;
            self.sectionEndImageView.image = SET_IMAGE(@"the_end.png");
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_THE_END withDelegate:self];
            break;
        default:
            break;
    }
    
    [self.view addSubview:self.sectionEndImageView];
    [[AudioPlayer sharedAudioPlayer] playAudio];
    // Increment the section
    self.sectionId++;
}

- (void) loadQuestionUsingJson:(NSString *)json inWebView:(UIWebView *)kWebView{
    
    json = [self getJSONContentForId:json];
    
    //Covert JSON to NSDictionary
    if([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0)){
        json = [json stringByReplacingOccurrencesOfString:@".png" withString:@"@2x.png"];
    }
    NSError *error = nil;
    
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        
        int type=1;
        
        if(dataDic && [dataDic count] > 0){
            type = [[dataDic objectForKey:@"type"] intValue];
        }
        
        //Create a request and load HTML Template in webview based on Type Parameter
        NSString *htmlFilePath = nil;
        NSURL *url = nil;
        
        NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"alpha"];
        htmlFilePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"template%d.html",type]];
        url = [NSURL encryptedFileURLWithPath:htmlFilePath];
        
        NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        html = [html stringByReplacingOccurrencesOfString:@"//$$REPLACEDATA$$//" withString:[NSString stringWithFormat:@"var jsondata = '%@';",[json stringByReplacingOccurrencesOfString:@"\n" withString:@""]]];
        [kWebView loadHTMLString:html baseURL:url];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void)playAudio{
    if (!self.didPlayQuestionAudioOnce) {
        [self playAudioButtonPressed:nil];
    }
}

- (void) playQuestionAudioAtIndex: (NSNumber *)index
{
    if ([[AudioPlayer sharedAudioPlayer] playerState] == PLAYING && ([[[AudioPlayer sharedAudioPlayer] audioFile] isEqualToString:AUDIO_PAUSE_SCREEN_PRE_EXAM_START] || [[[AudioPlayer sharedAudioPlayer] audioFile] isEqualToString:AUDIO_PAUSE])) {
        return;
    }
    
    NSString *question = [self getJSONContentForId:[self.questionsArray objectAtIndex:[index intValue]]];
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSString *questionAudioPath = [dataDic objectForKey:@"questionAudio"];
        NSString *absoluteAudioPath = [[questionAudioPath stringByReplacingOccurrencesOfString:@"mp3" withString:@"m4a"] stringByReplacingOccurrencesOfString:@"audio/" withString:@""];
        NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:[@"alpha" stringByAppendingPathComponent:@"audio"]];
        absoluteAudioPath = [filePath stringByAppendingPathComponent:absoluteAudioPath];
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
        
        //Set the state
        self.currentAudioState = AUDIO_STATE_QUESTION;
        
        // Play Question Audio
        [[AudioPlayer sharedAudioPlayer] performSelector:@selector(playAudio) withObject:nil afterDelay:0.75];
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
    
}

- (void) playBeginSectionAudio{
    
    switch (self.sectionId)
    {
        case VERBAL_REASONING:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_FIRST_SUB_SECTION_INTRO_ANALYTICAL withDelegate:self];
            break;
        case QUANTITATIVE_REASONING:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_PRACTICAL withDelegate:self];
            break;
        case EARLY_LITERACY:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_EARLY_LIT withDelegate:self];
            break;
        case MATH:
            [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_SUB_SECTION_INTRO_MATH withDelegate:self];
            break;
            
        default:
            break;
    }
    
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

- (void) playAnswerAudioWithAnswerID: (NSString *)answerId
{
    NSString *question = [self getJSONContentForId:[self.questionsArray objectAtIndex:0]];
    
    NSError *error = nil;
    //Covert JSON to NSDictionary
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:[question dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error) {
        NSArray *answerChoicesArray = [dataDic objectForKey:@"choices"];
        
        for (NSDictionary *dict in answerChoicesArray)
        {
            NSString *tempAnswerId = [dict objectForKey:@"answerID"];
            if ([tempAnswerId isEqualToString: answerId])
            {
                NSString *answerAudioPath = [dict objectForKey:@"answerAudio"];
                NSString *absoluteAudioPath = [[answerAudioPath stringByReplacingOccurrencesOfString:@"audio/" withString:@""] stringByReplacingOccurrencesOfString:@"mp3" withString:@"m4a"];
                NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:[@"alpha" stringByAppendingPathComponent:@"audio"]];
                absoluteAudioPath = [filePath stringByAppendingPathComponent:absoluteAudioPath];
                [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
                [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithEncryptedAudioFile:absoluteAudioPath withDelegate:self];
                //Set the state
                self.currentAudioState = AUDIO_STATE_ANSWER;
                [[AudioPlayer sharedAudioPlayer] playAudio];
                break;
            }
        }
    }
    else{
        DLog(@"Error while parsing JSON data for %s", __FUNCTION__);
    }
}

- (void) enableAnswerChoices
{
    //Enable choices
    if (!self.webView.isHidden)
    {
        [self.webView stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
    }
    else
    {
        [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"enableChoices()"];
    }
}

- (NSString *) getJSONContentForId:(NSString *)jsonId{
    
    NSString *path = [[[@"alpha" stringByAppendingPathComponent:@"data"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", jsonId]] stringByAppendingPathExtension:@"json"];
    NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:path];
    NSString *data = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    return data;
}


-(void)endExam{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppleReviewerTestEnded" object:nil];
    [self.view removeFromSuperview];
}


#pragma mark - Action Methods

- (IBAction)beginExam:(id)sender{
    SAFE_RELEASE(_questionsArray);
    _questionsArray = [[NSMutableArray alloc] init];
    NSString *question = nil;
    NSString *secondQuestion = nil;
    
    question = [self.allQuestionsArray objectAtIndex:self.questionsCount-1];
    secondQuestion = [self.allQuestionsArray objectAtIndex:self.questionsCount];
    
    [self.questionsArray addObject:question];
    [self.questionsArray addObject:secondQuestion];
    
    if (self.sectionId % 2 == 0) {
        [self loadQuestionUsingJson:[self.questionsArray objectAtIndex:0] inWebView:self.webViewAlternate];
        [self loadQuestionUsingJson:[self.questionsArray objectAtIndex:1] inWebView:self.webView];
    }
    else{
        [self loadQuestionUsingJson:[self.questionsArray objectAtIndex:1] inWebView:self.webViewAlternate];
        [self loadQuestionUsingJson:[self.questionsArray objectAtIndex:0] inWebView:self.webView];
    }
}

- (IBAction)nextButtonPressed:(id)sender {
#ifdef FAST_FORWARD
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#else
    self.nextButton.enabled = NO;
    self.replayButton.enabled = NO;
#endif
    
    NSString *questionResponse = @"";
    // Get the response of question
    if (!self.webView.isHidden) {
        [self.webView stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
        questionResponse = [self.webView stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    }
    else{
        [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"$('#getSingleSelectionResult').click();"];
        questionResponse = [self.webViewAlternate stringByEvaluatingJavaScriptFromString:@"$('#singleSelectionResultText').text();"];
    }
    
    if ([questionResponse isEqualToString:@"0"] && NO == self.isOmitQuestionAudioPlayed)
    {
        self.nextButton.enabled = YES;
        self.replayButton.enabled = YES;
        self.isOmitQuestionAudioPlayed = YES;
        // Stop the audio player
        [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
        [[AudioPlayer sharedAudioPlayer] configureAudioPlayerWithAudioFile:AUDIO_OMIT_QUESTION withDelegate:self];
        [[AudioPlayer sharedAudioPlayer] playAudio];
        
        return;
    }
    else
    {
        //Disable the next and play buttons
#ifdef FAST_FORWARD
        self.nextButton.enabled = YES;
        self.replayButton.enabled = YES  ;
#else
        self.nextButton.enabled = NO;
        self.replayButton.enabled = NO;
#endif
    }
    
    //Reset the bool flag to NO
    self.isOmitQuestionAudioPlayed = NO;
    
    // Stop the audio player
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    //Reset the didPlayQuestionAudioOnce to NO
    self.didPlayQuestionAudioOnce = NO;
    
    // Increment the question count
    self.questionsCount++;
    
    // End Section if question Id is equal to max limit of questions per section
    if ((self.questionsCount-1) % AR_QUESTION_LIMIT_PER_SECTION == 0)
    {
        [self endSection];
    }
    else
    {
        // Apply the fade animation for questions
        [ERBCommons applyFadeAnimationTo:self.view.window withDuration:0.5];
        
        //enable audio
        [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:1]];
        
        // Based on current webview's visibility, hide/unhide the webviews
        if (self.webViewAlternate.isHidden) {
            
            self.webViewAlternate.hidden = NO;
            self.webView.hidden = YES;
        }
        else{
            
            self.webViewAlternate.hidden = YES;
            self.webView.hidden = NO;
        }
        
        //Now update the questions array
        //Remove the previous question
        [self.questionsArray removeObjectAtIndex:0];
        
        if (self.questionsCount < self.allQuestionsArray.count)
        {
            [self.questionsArray addObject:[self.allQuestionsArray objectAtIndex:self.questionsCount]];
            
            if (self.webView.hidden)
            {
                [self loadQuestionUsingJson:[self.questionsArray objectAtIndex:1] inWebView:self.webView];
            }
            else
            {
                [self loadQuestionUsingJson:[self.questionsArray objectAtIndex:1] inWebView:self.webViewAlternate];
            }
        }
    }
}

- (IBAction)playAudioButtonPressed:(id)sender
{
    if (self.didPlayQuestionAudioOnce) {
        // Enable Next and Replay Audio Button
        self.nextButton.enabled = YES;
        
        //Enable answer choices
        [self enableAnswerChoices];
        
        // Disable the Replay Button
        self.replayButton.enabled = NO;
    }
    
    // Play question audio according to whether we have received next question. If we have not, we play the audio at index 0, else we play audio at index 1.
    [self playQuestionAudioAtIndex:[NSNumber numberWithInteger:0]];
}


#pragma mark - UIWebViewDelegate

-(BOOL) webView:(UIWebView *)kWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [[request URL] absoluteString];
    
    if ([requestString hasPrefix:@"js-frame:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        
        NSString *function = (NSString*)[components objectAtIndex:1];
        NSString *answerId = [components objectAtIndex:2];
        
        if ([function isEqualToString:@"playAnswerAudio"])
        {
            [self playAnswerAudioWithAnswerID:answerId];
        }
        return NO;
    }
    return YES;
}

-(void) webViewDidStartLoad:(UIWebView *)kWebView{
    
}

-(void) webViewDidFinishLoad:(UIWebView *)kWebView
{
    static int questionLoadCount = 0;
    
    //Disable copy in UIWebView
    // Disable user selection
    [kWebView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    // Disable callout
    [kWebView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    
    if ([self.view.subviews objectAtIndex:self.view.subviews.count-1] == self.beginSectionView) {
        [self performSelector:@selector(sendBeginSectionViewToBack) withObject:nil afterDelay:1.0];
    }
    
    questionLoadCount++;
}

-(void) webView:(UIWebView *)kWebView didFailLoadWithError:(NSError *)error{
    DLog("Exam Web View error: %@",[error localizedDescription]);
}

#pragma mark - AVAudioPlayerDelegate Methods

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [[AudioPlayer sharedAudioPlayer] resetAudioPlayer];
    
    if ([self.view.subviews objectAtIndex:self.view.subviews.count-1] == self.beginSectionView) {
        [self.beginSectionButton setEnabled:YES];
        [self.view setUserInteractionEnabled:YES];
    }
    else if ([self.view.subviews objectAtIndex:self.view.subviews.count-1] == self.sectionEndImageView && (self.sectionId == MATH+1)) {
        [self performSelector:@selector(endExam) withObject:nil afterDelay:END_EXAM_DELAY];
    }
    else{
        
        switch (self.currentAudioState)
        {
            case AUDIO_STATE_QUESTION:
            {
                //Set the didPlayQuestionAudioOnce to YES
                self.didPlayQuestionAudioOnce = YES;
                
                // Enable Next and Replay Audio Button
                self.nextButton.enabled = YES;
                self.replayButton.enabled = YES;
                //Enable choices
                [self enableAnswerChoices];
                
                break;
            }
            case AUDIO_STATE_SECTION:
            {
                [self setupSectionStartScreen];
                break;
            }
            default:
                break;
        }
        self.currentAudioState = AUDIO_STATE_NONE;
    }
    
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] pauseAudio];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player{
    [[AudioPlayer sharedAudioPlayer] playAudio];
}

#pragma mark - UIScrollViewDelegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

#pragma mark - Lifecycle Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.sectionId = VERBAL_REASONING;
    self.questionsCount = 1;
    self.view.userInteractionEnabled = YES;
    
    //Set the didPlayQuestionAudioOnce flag to NO initially. Will set to YES when audio plays for the first time
    self.didPlayQuestionAudioOnce = NO;
    
    UIScrollView *scrollView = self.webView.scrollView;
    scrollView.delegate = self;
    scrollView.scrollEnabled = NO;
    scrollView.bounces = NO;
    
    UIScrollView *alternateScrollView = self.webViewAlternate.scrollView;
    alternateScrollView.delegate = self;
    alternateScrollView.scrollEnabled = NO;
    alternateScrollView.bounces = NO;
    
    _allQuestionsArray = [@[
                            @"655",
                            @"288",
                            @"604",
                            @"2004",
                            @"2077",
                            @"2038",
                            @"215",
                            @"989",
                            @"1072",
                            @"243",
                            @"986",
                            @"1158",
                            @"4031",
                            @"933",
                            @"4045",
                            @"887",
                            @"4000",
                            @"4064",
                            @"66",
                            @"3050",
                            @"3001",
                            @"3002",
                            @"27",
                            @"40"
                            ] mutableCopy];
    
    self.sectionNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    self.readyToBeginLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:55.0];
    self.arrowLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    
    [self setupSectionStartScreen];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
