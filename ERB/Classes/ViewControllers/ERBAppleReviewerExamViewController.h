//
//  ERBAppleReviewerExamViewController.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>

@interface ERBAppleReviewerExamViewController : UIViewController<UIScrollViewDelegate>

@property (retain, nonatomic) IBOutlet UIButton *replayButton;
@property (retain, nonatomic) IBOutlet UIButton *nextButton;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIWebView *webViewAlternate;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UIImageView *sectionEndImageView;
@property (retain, nonatomic) IBOutlet UILabel *sectionNameLabel;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *beginSectionActivityIndicator;
@property (retain, nonatomic) IBOutlet UILabel *arrowLabel;
@property (retain, nonatomic) IBOutlet UIImageView *examThemeImage;
@property (retain, nonatomic) IBOutlet UILabel *readyToBeginLabel;
@property (retain, nonatomic) IBOutlet UIButton *beginSectionButton;
@property (retain, nonatomic) IBOutlet UIView *beginSectionView;
- (IBAction)beginExam:(id)sender;
- (IBAction)nextButtonPressed:(id)sender;
- (IBAction)playAudioButtonPressed:(id)sender;
- (void) playQuestionAudioAtIndex: (NSNumber *)index;
@end
