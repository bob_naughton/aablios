//
//  ERBTutorialViewController.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>
#import "ERBCheckIn.h"
#import <AVFoundation/AVFoundation.h>

@protocol ERBTutorialViewControllerDelegate <NSObject>
- (void) didEndTutorial;
@end

@interface ERBTutorialViewControllerOffline : UIViewController<AVAudioPlayerDelegate,UIScrollViewDelegate, UIWebViewDelegate>
@property (retain, nonatomic) IBOutlet UIImageView *tutorialImageView;
@property (nonatomic, assign) id <ERBTutorialViewControllerDelegate> delegate;
@property (nonatomic, retain) ERBCheckIn *checkInData;
@property (nonatomic, assign) TUTORIAL_EVENT currentTutorialEvent;
@property (retain, nonatomic) IBOutlet UIView *beginPracticeView;
@property (retain, nonatomic) IBOutlet UILabel *tutorialLabel;
@property (retain, nonatomic) IBOutlet UILabel *readyToBeginLabel;
@property (retain, nonatomic) IBOutlet UILabel *beginPracticeLabel;
@property (retain, nonatomic) IBOutlet UIWebView *dynamicQuestionWebView;
@property (retain, nonatomic) IBOutlet UIView *dynamicTutorialView;
@property (retain, nonatomic) IBOutlet UIButton *replayButton;
@property (retain, nonatomic) IBOutlet UIButton *nextButton;
@property (retain, nonatomic) IBOutlet UIButton *beginPracticeButton;

- (void) adjustGUI;
- (IBAction)beginPracticeButtonPressed:(id)sender;
- (IBAction)nextButtonPressed:(id)sender;
- (IBAction)repeatAudioButtonPressed:(id)sender;

@end
