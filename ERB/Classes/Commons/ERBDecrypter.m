//
//  ERBDecrypter.m
//  ERB
//
//  Copyright 2013 © WebINTENSIVE Software. All rights reserved.
//

#import "ERBDecrypter.h"
#import "SSZipArchive.h"

@implementation ERBDecrypter
@synthesize data,filePath, file;

- (id)init
{
	if (self = [super init])
	{
		data = [[NSMutableData alloc] init];
    }
	return self;
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (self.filePath)
    {
        [[NSFileManager defaultManager] createFileAtPath:self.filePath contents:nil attributes:nil];
        self.file = [NSFileHandle fileHandleForUpdatingAtPath:self.filePath];
        
        if (self.file)
        {
            [self.file seekToEndOfFile];
        }
    }
    else
    {
        [data setLength:0];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)_data
{
    if (self.filePath)
    {
        if (file)
        {
            [file seekToEndOfFile];
        }
        [file writeData:_data];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //Notify delegate that the web service has failed

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Notify delegate with the we service response data
    if (self.filePath)
    {
         NSFileManager *fileManager = [NSFileManager defaultManager];
        //Unzip zip file
        [SSZipArchive unzipFileAtPath:filePath toDestination:DOCUMENTS_DIRECTORY];
        [fileManager removeItemAtPath:filePath error:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DECRYPTION_DONE object:nil];
    }
    
}

-(void)dealloc
{
    SAFE_RELEASE(filePath);
    SAFE_RELEASE(file);
    SAFE_RELEASE(data);
    [super dealloc];
}

@end
