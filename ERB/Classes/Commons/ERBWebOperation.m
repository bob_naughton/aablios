//
//  ERBWebOperation.m
//  ERB

//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.

#define ALLOW_LOGGING

#import "ERBWebOperation.h"

@interface ERBWebOperation ()
@property (nonatomic, retain) NSURL *requestURL;
@property (nonatomic, retain) NSDictionary *requestParams;
@property (nonatomic, retain) NSMutableURLRequest *theRequest;
//Variable to see if it is a download request
@property (nonatomic, retain) NSString *requestIdentifier;

- (void) cleanUpWebServiceRequest: (NSURLConnection *) connection;
- (BOOL) sendRequestToWebServer;
- (void) setPostBodyData;
- (void) saveWebServiceLogWithStatus:(NSString *)status error:(NSString *)error type:(NSString *)type;
- (void) cancelConnection;
@end

@implementation ERBWebOperation

@synthesize activeConnection;
@synthesize delegate;
@synthesize requestURL;
@synthesize requestParams;
@synthesize requestMethod;
@synthesize postDataEncodingType;
@synthesize theRequest;
@synthesize requestIdentifier;


#pragma mark Initialization
- (id) initWithRequestURL: (NSURL *)url
               withParams: (NSDictionary *)params
    withRequestIdentifier: (NSString *)requestId
           withHttpMethod: (WEB_SERVICE_REQUEST_METHOD)method;
{
    self.requestURL = url;
    self.requestParams = params;
    self.requestIdentifier = requestId;
    self.requestMethod = method;
    return [self init];
}

- (id)init
{
	if (self = [super init])
	{
		webData = nil;
		busyStatus = NO;
		delegate = nil;
		self.activeConnection = nil;
        
        // Add self as observer for exam end notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelConnection) name:NOTIFICATION_EXAM_STOPPED object:nil];
    }
	return self;
}

#pragma mark - DEALLOC
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	[requestURL release];
    SAFE_RELEASE(requestParams);
    SAFE_RELEASE(requestIdentifier);
	[super dealloc];
}

#pragma mark - Private Methods
///////////////////////////////////////////////////////////////////////
//// Purpose            : Builds the post data and appends it to the request
///////////////////////////////////////////////////////////////////////
- (void) setPostBodyData
{
    switch (self.postDataEncodingType)
    {
        case ERBPostDataEncodingTypeURL:
        {
            NSString *requestString = [[NSString alloc] initWithString:[self.requestParams urlEncodedKeyValueString]];
            [self.theRequest addValue: [NSString stringWithFormat:@"%lu",(unsigned long)requestString.length] forHTTPHeaderField: @"Content-Length"];
            [self.theRequest setHTTPBody:[requestString dataUsingEncoding:NSUTF8StringEncoding]];
            [requestString release];
            break;
        }
        case ERBPostDataEncodingTypeJSON:
        {
            NSString *requestString = [[NSString alloc] initWithString:[self.requestParams jsonEncodedKeyValueString]];
            [self.theRequest addValue: [NSString stringWithFormat:@"%lu",(unsigned long)requestString.length] forHTTPHeaderField: @"Content-Length"];
            [self.theRequest setHTTPBody:[requestString dataUsingEncoding:NSUTF8StringEncoding]];
            [requestString release];
            break;
        }
        case ERBPostDataEncodingTypeMultipart:
        {
            
        }
        default:
            break;
    }
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Prepare SOAP/JSON header, content and send request to the web server
///////////////////////////////////////////////////////////////////////
- (BOOL)sendRequestToWebServer
{
	self.theRequest = [NSMutableURLRequest requestWithURL:self.requestURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:TIME_OUT_INTERVAL];
	
    //Set the content type
    switch (self.postDataEncodingType)
    {
        case ERBPostDataEncodingTypeURL:
            [self.theRequest addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
            break;
        case ERBPostDataEncodingTypeJSON:
            [self.theRequest addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
            break;
        case ERBPostDataEncodingTypeSOAP:
            [self.theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
            break;
        default:
            break;
    }
    
    //Set the http method
    switch (self.requestMethod) {
        case ERB_REQUEST_GET:
            [self.theRequest setHTTPMethod: @"GET"];
            break;
        case ERB_REQUEST_POST:
            [self.theRequest setHTTPMethod: @"POST"];
            break;
        default:
            break;
    }
    
    //Set the language
    [self.theRequest setValue:[NSString stringWithFormat:@"%@, en-us",
                               [[NSLocale preferredLanguages] componentsJoinedByString:@", "]
                               ] forHTTPHeaderField:@"Accept-Language"];
    
    //Build post body
    if (self.requestMethod == ERB_REQUEST_POST)
    {
        [self setPostBodyData];
    }
    
    [ERBCommons appController].bytesSent += [[self.theRequest HTTPBody] length];
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:self.theRequest delegate: self];
	self.activeConnection = theConnection;
	[theConnection release];
	
	return (nil == self.activeConnection) ? NO : YES;
}

- (void) cancelConnection{
    if (self.activeConnection) {
        [self.activeConnection cancel];
    }
    
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : to do cleanup activities when a communication is complete
///////////////////////////////////////////////////////////////////////
- (void)cleanUpWebServiceRequest:(NSURLConnection *)connection
{
    SAFE_RELEASE(webData);
    SAFE_RELEASE(activeConnection);
}

- (void) saveWebServiceLogWithStatus:(NSString *)status error:(NSString *)error type:(NSString *)type
{
    // check whether logging needs to be done or not; if not then return.
    BOOL isLogginEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:RESPONSE_KEY_ENABLE_CLIENT_LOGGING];
    if (!isLogginEnabled)
    {
        return;
    }
    
    // Do not save the logs for logging web service
    
    if (![self.requestIdentifier isEqualToString:REQUEST_ID_LOG_EVENTS] && ![self.requestIdentifier isEqualToString:REQUEST_ID_UPLOAD_RESPONSE_ACTION]){
        
        // create a log DATA dictionary
        NSMutableDictionary *response = [[NSMutableDictionary alloc] init];
        [response setObject:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:@"TIMESTAMP"];
        [response setObject:[self.requestURL absoluteString] forKey:@"REQUEST_URL"];
        [response setObject:self.requestParams forKey:@"REQUEST_PARAMS"];
        
        if ([type isEqualToString:@"RESPONSE"]) {
            
            [response setObject:status forKey:@"STATUS"];
            
            if ([status isEqualToString:@"SUCCESS"]) {
                NSError *error = nil;
                NSMutableDictionary *JSONResponse = [NSJSONSerialization JSONObjectWithData:webData options:kNilOptions error:&error];
                
                if (!error) {
                    [response setObject:JSONResponse forKey:@"REQUEST_RESPONSE"];
                }
                else{
                    DLog(@"Attention: Web service has an invalid JSON response: %@", self.requestIdentifier);
                }
            }
            else{
                if (error) {
                    [response setObject:error forKey:@"ERROR"];
                }
            }
            
        }
        
        [[EventLogger sharedEventLogger] saveLog:response];
        SAFE_RELEASE(response);
    }
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [webData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData: data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [connection cancel];
    
    DLog(@"Web service failed, Error: %@, Delegate: %@", [error localizedDescription], delegate);
    
    if (error) {
        // Save loggging data when request for WS does not complete and fails with error.
        [self saveWebServiceLogWithStatus:@"FAILURE" error:[error localizedDescription] type:@"RESPONSE"];
    }
    
    if (nil != delegate)
    {
        //Notify delegate that the web service has failed
        if (delegate && [delegate respondsToSelector: @selector(webServiceDidFailWithError:requestIdentifier:)])
        {
            [delegate webServiceDidFailWithError:error requestIdentifier:self.requestIdentifier];
        }
        //Do Cleanup
        [self cleanUpWebServiceRequest: connection];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [connection cancel];
    NSError *error = nil;
    NSMutableDictionary *JSONResponse = [NSJSONSerialization JSONObjectWithData:webData options:kNilOptions error:&error];
    if (JSONResponse) {
        
        [ERBCommons appController].bytesReceived += [webData length];
        
        if (nil != delegate)
        {
            
#ifdef ALLOW_LOGGING
            NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:webData options:kNilOptions error:nil];
            DLog(@"WEB SERVICE RESPONSE: %@", response);
#endif
            
            // Save loggging data when request for WS finishes loading without anu error
            [self saveWebServiceLogWithStatus:@"SUCCESS" error:nil type:@"RESPONSE"];
            
            //Notify delegate with the we service response data
            if (delegate && [delegate respondsToSelector: @selector(webServiceDidReceiveResponse:requestIdentifier:operationStatus:)])
            {
                [delegate webServiceDidReceiveResponse: webData requestIdentifier:self.requestIdentifier operationStatus:ERB_WS_OPERATION_STATUS_SUCCESS];
            }
            //Do Cleanup
            [self cleanUpWebServiceRequest: connection];
        }
        
    }
    else{
        [self cleanUpWebServiceRequest: connection];
        [self sendWebServiceRequest];
    }
}

#pragma mark - Public
- (BOOL)sendWebServiceRequest
{
	BOOL isSuccess = NO;
    
    if (YES == [self sendRequestToWebServer])
    {
        
#ifdef ALLOW_LOGGING
        DLog(@"WEB SERVICE REQUEST: %@ \n with Params: %@", self.requestURL, self.requestParams);
#endif
        
        webData = [[NSMutableData data] retain];
        isSuccess = YES;
        
    }
    else{
        DLog(@"REQUEST NOT INSTANTIATED: %@", self.requestURL);
    }
	return isSuccess;
}



@end
