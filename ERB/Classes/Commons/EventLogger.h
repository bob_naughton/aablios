//
// ERBEventLogs.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <Foundation/Foundation.h>
#import "ERBWebOperation.h"

@interface EventLogger : NSObject<ERBWebServiceDelegate>
@property (nonatomic, assign) LOG_EVENT eventType;
@property (nonatomic, retain) NSMutableArray *events;
@property (nonatomic, retain) NSString *examId;
@property (nonatomic, retain) NSString *studentId;

+(id)sharedEventLogger;
-(void)saveLog:(NSDictionary *)logDict;
-(void)sendLogs;
- (void)clearLogs;

@end
