//
//  ERBConstants.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#ifndef ERB_ERBConstants_h
#define ERB_ERBConstants_h


#define _DEBUG                                                //For Testing only
//#define FAST_FORWARD

// string localization
#define ERBLocalizedString(key)                                 [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:@"ERBStrings"]

//Get Documents Directory
#define DOCUMENTS_DIRECTORY                                     [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

//Get User Defaults
#define USER_DEFAULTS                                           [NSUserDefaults standardUserDefaults]

// utility macro
#define SAFE_RELEASE(obj)                                       if(obj){[obj release]; obj = nil;}

#define CANCEL_CONNECTION(obj)                                  if(obj){if([obj activeConnection]){ [[obj activeConnection] cancel];}}

#define SAVE_USER_DEFAULTS_AND_SYNC(object,key)                 [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];[[NSUserDefaults standardUserDefaults] synchronize];

#define COLOR(r,g,b)                                            [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]

#define SET_IMAGE(file_name) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:file_name ofType:nil]]


//file extension

#define EXTENSION_PNG                                           @"png"
#define EXTENSION_JPG                                           @"jpg"

#define KEY_CURRENT_ASSETS_REVISION                             @"AssetsRevision"


#define PATH_OFFLINE_MODE_CONTENT                               @"OfflineResources"
//User default keys
#define USER_DEFAULTS_KEY_LOCAL_APP                             @"LocalApp"

//Localization_KEYS
#define LOCALIZATION_KEY_APPLICATION_NAME                       @"APPLICATION_NAME"

#define LOCALIZATION_KEY_TITLE_OK                               @"TITLE_OK"
#define LOCALIZATION_KEY_TITLE_CANCEL                           @"TITLE_CANCEL"
#define LOCALIZATION_KEY_TITLE_CONFIRM                          @"TITLE_CONFIRM"

#define LOCALIZATION_KEY_ALERT_MESSAGE_UNSUCCESSFUL_LOGIN       @"ALERT_MESSAGE_UNSUCCESSFUL_LOGIN"
#define LOCALIZATION_KEY_ALERT_MESSAGE_UNSUCCESSFUL_CHECKIN     @"ALERT_MESSAGE_UNSUCCESSFUL_CHECKIN"
#define LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_LOGIN         @"ALERT_MESSAGE_NO_NETWORK_LOGIN"
#define LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_EXAM_SUMMARY  @"ALERT_MESSAGE_NO_NETWORK_EXAM_SUMMARY"
#define LOCALIZATION_KEY_ALERT_MESSAGE_NO_NETWORK_CHECKIN       @"ALERT_MESSAGE_NO_NETWORK_CHECKIN"
#define LOCALIZATION_KEY_ALERT_MESSAGE_NO_STUDENT_NAME          @"ALERT_MESSAGE_NO_STUDENT_NAME"
#define LOCALIZATION_KEY_ALERT_MESSAGE_DOB_NO_MATCH             @"ALERT_MESSAGE_DOB_NO_MATCH"
#define LOCALIZATION_KEY_ALERT_MESSAGE_EMPTY_LOGIN_FIELDS       @"ALERT_MESSAGE_EMPTY_LOGIN_FIELDS"
#define LOCALIZATION_KEY_ALERT_NO_EXAMS_TO_LAUNCH               @"ALERT_NO_EXAMS_TO_LAUNCH"

//Cell Identifiers
#define CELL_IDENTIFER_ERBCHECKINCELL                           @"ERBCheckInCellIdentifier"
#define CELL_IDENTIFER_ERBSCHEDULECELL                          @"ERBScheduleCellIdentifier"

//FIle names
#define FILE_NAME_QUESTION_RESPONSES                            @"QuestionResponsesDictionary"

//Image Names
#define IMAGE_NAME_UNCHECK_GRAY                                 @"uncheck.png"
#define IMAGE_NAME_CHECK_BLUE                                   @"check_blue.png"
#define IMAGE_NAME_CHECK_GRAY                                   @"check_gray.png"

//Tags
#define TAG_SLIDE_SHOW_IMAGE_VIEW                               1001
#define SLIDE_SHOW_TRANSITION_TIME                              16.0
#define END_EXAM_DELAY                                          2.0*60.0
#define DELAY                                                   2.0
#define PROCTOR_ERROR_DELAY                                     5.0

#define QUESTION_LIMIT_PER_SECTION                              [[USER_DEFAULTS objectForKey: RESPONSE_KEY_QUESTIONS_COUNT] intValue]
#define TIME_LIMIT_PER_SECTION                                  600
#define TIME_LIMIT_PER_QUESTION                                 (TIME_LIMIT_PER_SECTION/QUESTION_LIMIT_PER_SECTION)

#define OFFLINE_BREAK_TIME                                      60
#define TIME_GET_EXAM_STATUS                                    10
#define ROLE_SUPERVISOR                                         @"SUPERVISOR"
#define ROLE_ADMIN                                              @"ADMINISTRATOR"


#define KEY_LAST_SYNC_DATE                                      @"last_sync_date"


#define LOG_TYPE_CHECKIN                                        @"CheckIn"
#define LOG_TYPE_TUTORIAL                                       @"Tutorial"
#define LOG_TYPE_EXAM                                           @"Exam"
#define LOG_TYPE_APP_RESTART                                    @"AppRestart"

#define USER_ACTION_TUTORIAL                                    @"TUTORIAL"
#define USER_ACTION_TUTORIAL_START                              @"TUTORIAL_START"
#define USER_ACTION_TUTORIAL_HELP                               @"TUTORIAL_HELP"
#define USER_ACTION_TUTORIAL_REPLAY                             @"TUTORIAL_REPLAY"
#define USER_ACTION_TUTORIAL_NEXT                               @"TUTORIAL_NEXT"
#define USER_ACTION_TUTORIAL_MULTIPLE_SELECTION                 @"TUTORIAL_MULTIPLE_SELECTION"
#define USER_ACTION_TUTORIAL_DYNAMIC_1                          @"TUTORIAL_DYNAMIC_1"
#define USER_ACTION_TUTORIAL_DYNAMIC_2                          @"TUTORIAL_DYNAMIC_2"
#define USER_ACTION_TUTORIAL_DYNAMIC_3                          @"TUTORIAL_DYNAMIC_3"
#define USER_ACTION_TUTORIAL_DYNAMIC_4                          @"TUTORIAL_DYNAMIC_4"
#define USER_ACTION_TUTORIAL_END                                @"TUTORIAL_END"
#define USER_ACTION_HELP                                        @"HELP"
#define USER_ACTION_SUBMIT                                      @"SUBMIT"
#define USER_ACTION_BREAK                                       @"BREAK"
#define USER_ACTION_START_EXAM                                  @"START_EXAM"
#define USER_ACTION_END_EXAM                                    @"END_EXAM"
#define USER_ACTION_START_SECTION                               @"START_SECTION"
#define USER_ACTION_END_SECTION                                 @"END_SECTION"
#define EXAM_STATE_PAUSED                                       @"PAUSED"
#define EXAM_STATE_ACTIVE                                       @"ACTIVE"
#define EXAM_STATE_STOPPED                                      @"STOPPED"
#define EXAM_STATE_RESET                                        @"RESET"
#define EXAM_STATE_BEGIN_TUTORIAL                               @"BEGIN_0"
#define EXAM_STATE_BEGIN_SECTION_1                              @"BEGIN_1"
#define EXAM_STATE_BEGIN_SECTION_2                              @"BEGIN_2"

//Audio file names
#define AUDIO_ADJUST_VOLUME                                     @"bubble pop.wav"
#define AUDIO_WAIT_FOR_HELP                                     @"Wait For Help.mp3"
#define AUDIO_TUTORIAL_INTRO                                    @"Tutorial Intro.m4a"
#define AUDIO_TUTORIAL_HELP_BUTTON                              @"Tutorial Help Button.m4a"
#define AUDIO_TUTORIAL_REPLAY_AUDIO_BUTTON                      @"Tutorial Replay.m4a"
#define AUDIO_TUTORIAL_NEXT_BUTTON                              @"Tutorial Next.m4a"
#define AUDIO_TUTORIAL_BEGIN_PRACTICE                           @"Tutorial Begin Practice.m4a"
#define AUDIO_TUTORIAL_TRY_QUESTION                             @"Tutorial Try Question.m4a"
#define AUDIO_TUTORIAL_POST_QUESTION_ONE_AUDIO                  @"Tutorial Post Question One Audio.m4a"
#define AUDIO_PAUSE_SCREEN_PRE_EXAM_START                       @"Pause Screen.m4a"
#define AUDIO_FIRST_SUB_SECTION_INTRO_ANALYTICAL                @"First Sub Section Intro Analytical.mp3"
#define AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_PRACTICAL         @"Sub Section Intro Encouragement Practical.mp3"
#define AUDIO_SUB_SECTION_INTRO_PRACTICAL                       @"Sub Section Intro Practical.mp3"
#define AUDIO_SECTION_BREAK                                     @"Section Break.mp3"
#define AUDIO_SUB_SECTION_INTRO_EARLY_LIT                       @"Sub Section Intro Early Lit.mp3"
#define AUDIO_SUB_SECTION_INTRO_ENCOURAGEMENT_MATH              @"Sub Section Intro Encouragement Math.mp3"
#define AUDIO_SUB_SECTION_INTRO_MATH                            @"Sub Section Intro Math.mp3"
#define AUDIO_THE_END                                           @"The End.mp3"
#define AUDIO_PAUSE                                             @"Pause.mp3"
#define AUDIO_CONNECTIVITY_LOST                                 @"If Platform Loses Connectivity.mp3"
#define AUDIO_NICE_WORK                                         @"nice work.mp3"
#define AUDIO_OMIT_QUESTION                                     @"omit_question.m4a"

#define NOTIFICATION_GLOBAL_PAUSE_STARTED                       @"Global Pause Started"
#define NOTIFICATION_GLOBAL_PAUSE_ENDED                         @"Global Pause Ended"
#define NOTIFICATION_EXAM_STOPPED                               @"Exam Stopped"

//Font Names
#define FONT_NAME_FUTURA_HEAVY                                  @"Futura Hv BT"
#define FONT_NAME_FUTURA_BOOK                                   @"Futura Bk BT"
#define FONT_NAME_FUTURA_BOLD                                   @"Futura-Bold"
#define FONT_NAME_FUTURA_MEDIUM                                 @"Futura-Medium"

//Event Logs
#define EVENT_LOG_LOGIN_KEY                                     @"UserLogin"
#define EVENT_LOG_LOGIN_VALUE_BUTTON_PRESS                      @"Login button pressed"
#define EVENT_LOG_LOGIN_VALUE_BUTTON_LOGIN_SUCCESS              @"User logged in successfully"
#define EVENT_LOG_LOGIN_VALUE_BUTTON_LOGIN_FAILURE              @"Login login failed due to connection error"

#define EXAM_MODE_ONLINE            @"ONLINE"
#define EXAM_MODE_OFFLINE           @"OFFLINE"
#define EXAM_MODE_NONE              @"NONE"

#define IS_DOWNLOAD_COMPLETE        @"isDownloadComplete"

#define RESPONSES_FILE_NAME         @"Responses.json"

//Dynamic Tutorial
#define DYNAMIC_TUTORIAL_NUMBER                                 4

#define DYNAMIC_TUTORIAL_PLIST_NAME                             @"DynamicTutorialSequence"

#define KEY_USER_DEFAULTS_ERB_OFFLINE_EXAM_STATE                @"OfflineExamState"

#define RESOURCES_DIRECTORY_PATH    [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources"]
#define UPDATES_DIRECTORY_PATH      [[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:@"Updates"]
#define IMAGES_DIRECTORY_PATH       [[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources"] stringByAppendingPathComponent:@"images"]
#define ASSETS_BACKGROUND_DIRECTORY_PREFIX      @"background_"

#define IMAGE_PATH(imageName)       [IMAGES_DIRECTORY_PATH stringByAppendingPathComponent:imageName]

#define TUTORIAL_HTML_PATH          [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/dynamic"]
#define TUTORIAL_JSON_PATH          [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/dynamic/data"]
#define TUTORIAL_AUDIO_PATH         [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/dynamic/audio"]
#define TUTORIAL_VIDEO_PATH         [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/video"]

#define EXAM_HTML_PATH              [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/alpha"]
#define EXAM_JSON_PATH              [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/alpha/data"]
#define EXAM_AUDIO_PATH             [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/alpha/audio"]
#define EXAM_ASSETS_BACKGROUND_PATH [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/alpha/assets/background"]
#define REQUIRED_DATE_FORMAT                        @"yyyy-MM-dd"

//Enums
typedef enum {
    HELP,
    BREAK,
    SUBMIT,
    TUTORIAL,
    END
}USER_ACTION;

typedef enum {
    TUTORIAL_EVENT_NONE = 0,
    TUTORIAL_EVENT_START,
    TUTORIAL_EVENT_REPLAY,
    TUTORIAL_EVENT_HELP,
    TUTORIAL_EVENT_NEXT,
    TUTORIAL_DYNAMIC_1,
    TUTORIAL_DYNAMIC_2,
    TUTORIAL_DYNAMIC_3,
    TUTORIAL_EVENT_END
}TUTORIAL_EVENT;

typedef enum{
    VERBAL_REASONING = 1,
    QUANTITATIVE_REASONING,
    EARLY_LITERACY,
    MATH
}SECTION;

typedef enum{
    ERB_EXAM_STATE_PAUSE = 0,
    ERB_EXAM_STATE_PRE_TUTORIAL_PAUSE,
    ERB_EXAM_STATE_BEGIN_TUTORIAL,
    ERB_EXAM_STATE_BEGIN_SECTION_1,
    ERB_EXAM_STATE_END,
    ERB_EXAM_STATE_EXAM,
    ERB_EXAM_STATE_PAUSE_TUTORIAL,
    ERB_EXAM_STATE_PAUSE_EXAM,
    ERB_EXAM_STATE_BREAK_END
}ERB_EXAM_STATE;

typedef enum{
    ERB_EXAM_USER_ACTION_TUTORIAL = 0,
    ERB_EXAM_USER_ACTION_START_EXAM,
    ERB_EXAM_USER_ACTION_START_SECTION,
    ERB_EXAM_USER_ACTION_END_SECTION,
    ERB_EXAM_USER_ACTION_BREAK,
    ERB_EXAM_USER_ACTION_END_EXAM,
    ERB_EXAM_USER_ACTION_HELP,
    ERB_EXAM_USER_ACTION_SUBMIT,
    ERB_EXAM_USER_ACTION_NONE
}ERB_USER_ACTION;

typedef enum {
    ERB_EXAM_MODE_SERVER = 0,
    ERB_EXAM_MODE_LOCAL
}ERB_EXAM_MODE;
//if before section 1, 1 in section 1, 2 in section 2, 5 in break, 3 in section 3, 4 in section 4

typedef enum{
    EXAM_PROGRESS_TUTORIAL = 0,
    EXAM_PROGRESS_SECTION_ONE,
    EXAM_PROGRESS_SECTION_TWO ,
    EXAM_PROGRESS_SECTION_THREE,
    EXAM_PROGRESS_SECTION_FOUR,
    EXAM_PROGRESS_BREAK
}EXAM_PROGRESS_IDENTIFIER;

typedef enum {
    AUDIO_STATE_SECTION = 0,
    AUDIO_STATE_QUESTION,
    AUDIO_STATE_ANSWER,
    AUDIO_STATE_NONE
}AUDIO_STATE;

typedef enum {
    LOG_EVENT_CHECKIN = 0,
    LOG_EVENT_TUTORIAL,
    LOG_EVENT_EXAM,
    LOG_EVENT_APP_RESTART
}LOG_EVENT;

typedef enum {
    ERB_EXAM_STATE_NONE = 0,
    ERB_EXAM_STATE_BEGIN_TUTORIAL_SCREEN,
    ERB_EXAM_STATE_TUTORIAL,//Set
    ERB_EXAM_STATE_IN_PROGRESS,//Set
    ERB_EXAM_STATE_UPLOAD_RESPONSES,//Set
}ERB_OFFLINE_EXAM_STATE;

typedef enum
{
    TUTORIAL_USER_ACTION_NONE = 0,
    TUTORIAL_USER_ACTION_BEGIN_PRACTICE,
    TUTORIAL_USER_ACTION_HELP,
    TUTORIAL_USER_ACTION_PRE_QUESTION_AUDIO,
    TUTORIAL_USER_ACTION_POST_QUESTION_AUDIO,
    TUTORIAL_USER_ACTION_QUESTION_AUDIO,
    TUTORIAL_USER_ACTION_ANSWER_AUDIO,
    TUTORIAL_USER_ACTION_SLIDE_AUDIO
}ERB_CURRENT_TUTORIAL_USER_ACTION;

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//
//  ARC Helper
//
//  Version 2.1
//
//  Created by Nick Lockwood on 05/01/2012.
//  Copyright 2012 Charcoal Design
//
//  Distributed under the permissive zlib license
//  Get the latest version from here:
//
//  https://gist.github.com/1563325
//

#ifndef ah_retain
#if __has_feature(objc_arc)
#define ah_retain self
#define ah_dealloc self
#define release self
#define autorelease self
#else
#define ah_retain retain
#define ah_dealloc dealloc
#define __bridge
#endif
#endif

//  Weak delegate support

#ifndef ah_weak
#import <Availability.h>
#if (__has_feature(objc_arc)) && \
((defined __IPHONE_OS_VERSION_MIN_REQUIRED && \
__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_5_0) || \
(defined __MAC_OS_X_VERSION_MIN_REQUIRED && \
__MAC_OS_X_VERSION_MIN_REQUIRED > __MAC_10_7))
#define ah_weak weak
#define __ah_weak __weak
#else
#define ah_weak unsafe_unretained
#define __ah_weak __unsafe_unretained
#endif
#endif

//  ARC Helper ends



#ifdef _DEBUG
#       define DLog(fmt, ...) NSLog((@"< %@>" fmt), [NSString stringWithUTF8String:__PRETTY_FUNCTION__], ##__VA_ARGS__);
#else
#       define DLog(...)
#endif

#endif

