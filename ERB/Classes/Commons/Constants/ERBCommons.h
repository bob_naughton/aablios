//
//  ERBCommons.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <Foundation/Foundation.h>
#import "ERBConstants.h"
#import "ERBWebServiceConstants.h"
#import "ERBAppController.h"
#import "EncryptedFileURLProtocol.h"
#import "NSURL+EncryptedFileURLProtocol.h"
#import "ERBSynthesizeSingleton.h"
#import "EventLogger.h"

@interface ERBCommons : NSObject
+(ERBAppController *) appController;
+ (void) showAlert:(NSString*)message delegate:(id)delegate;
+ (void) showAlert:(NSString*)message delegate:(id)delegate btnTitle:(NSString*)btnTitle otherBtnTitle:(NSString*)otherbtnTitle tag:(NSInteger)tag;
+ (UIImage *) loadImageFromResourcePath:(NSString *)path ofType: (NSString *) type;
+ (BOOL) isStringEmpty:(NSString*) aString;
+ (void) applyFadeAnimationTo: (UIWindow *)window withDuration: (CFTimeInterval) duration;
+ (NSString *) authToken;
+ (NSString *) getSectionTitleForSectionId:(int)sec_id;
+ (NSString *) returnTimeScheduleStringWithStartTime:(NSString *)startTime endTime:(NSString *) endTime ;
+ (NSString *) examDateAfterFormattingFromString:(NSString *) dateString;
+ (NSString *) getUserName;
+ (void) clearUserDefaults;
+ (NSString *)secondsBetweenStartInterval:(NSTimeInterval)startTime andEndInterval:(NSTimeInterval)endTime;
+ (NSString *)getStringForLogType: (LOG_EVENT)eventType;
+ (NSString *)getUserAgentString;
+ (NSString *)getExamMode;
+ (void)writeToFile:(NSString *)fileName data:(NSData *)fileData;
+ (NSData *)getDataFromFile:(NSString *)fileName;
+ (void) removeOfflineContent;
+ (NSString *) getCurrentAssetsRevision;
+ (void) setCurrentAssetsRevision: (NSString *)revision;
+ (NSString *) getStringFromDate:(NSDate *)date inFormat:(NSString *)format;
+ (NSString *) getTimeElapsedForInterval:(double)interval;

@end
