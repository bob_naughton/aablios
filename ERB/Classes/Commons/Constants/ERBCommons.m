//
//  ERBCommons.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "ERBCommons.h"
#import "AppDelegate.h"
#import "SSZipArchive.h"
#import "AudioPlayer.h"
#import <QuartzCore/QuartzCore.h>
#import "KeychainItemWrapper.h"

@implementation ERBCommons

#pragma mark - INIT Method
- (id)init
{
    self = [super init];
    if (self) {
    }
    
    return self;
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Returns the application controller
///////////////////////////////////////////////////////////////////////
+ (ERBAppController*)appController
{
    ERBAppController *appController = nil;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appController = appDelegate.appController;
    return appController;
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Show alert message box..
///////////////////////////////////////////////////////////////////////
+ (void)showAlert:(NSString*)message delegate:(id)delegate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (NO == [message isEqualToString:@""] && nil != message)
        {
            UIAlertView *alertView  = [[UIAlertView alloc] init];
            alertView.title         = ERBLocalizedString(LOCALIZATION_KEY_APPLICATION_NAME);
            //alertView.title         = NSLocalizedString(@"PROJECT_TITLE",nil);
            alertView.message       = message;
            if (delegate!= nil)
            {
                alertView.delegate  = delegate;
            }
            [alertView addButtonWithTitle:ERBLocalizedString(LOCALIZATION_KEY_TITLE_OK)];
            //[alertView addButtonWithTitle:NSLocalizedString(@"ALERT_OK", nil)];
            [alertView show];
            [alertView release];
        }
    });
	
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Show alert message box with custom buttons..
///////////////////////////////////////////////////////////////////////
+ (void)showAlert:(NSString*)message delegate:(id)delegate btnTitle:(NSString*)btnTitle otherBtnTitle:(NSString*)otherbtnTitle tag:(NSInteger)tag
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (NO == [message isEqualToString:@""] && nil != message)
        {
            UIAlertView *alertView  = [[UIAlertView alloc] init];
            alertView.title         = ERBLocalizedString(LOCALIZATION_KEY_APPLICATION_NAME);
            //alertView.title         = NSLocalizedString(@"PROJECT_TITLE",nil);
            alertView.message       = message;
            if (delegate!= nil)
            {
                alertView.delegate  = delegate;
            }
            if (btnTitle != nil)
            {
                [alertView addButtonWithTitle:btnTitle];
            }
            if (otherbtnTitle != nil)
            {
                [alertView addButtonWithTitle:otherbtnTitle];
            }
            alertView.tag           = tag;
            [alertView show];
            [alertView release];
        }
    });
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Loads image from the given resource path
///////////////////////////////////////////////////////////////////////
+ (UIImage *)loadImageFromResourcePath:(NSString *)path ofType: (NSString *) type
{
    NSString *imagePath = [NSString stringWithFormat:@"%@.%@",path,type];
    UIImage *img = [UIImage imageNamed:imagePath];
	return img;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Purpose		: Method which checks whether user has clicked done button without enterring any value
//////////////////////////////////////////////////////////////////////////////////////////////
+ (BOOL) isStringEmpty:(NSString*) aString
{
	BOOL isSuccess = NO;
	
	NSString *newString = [aString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
	if([newString isEqual:@""] || nil == newString)
	{
		isSuccess = YES;
	}
	
	return isSuccess;
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Apply a simple fade animation to window..
///////////////////////////////////////////////////////////////////////
+ (void) applyFadeAnimationTo: (UIWindow *)window withDuration: (CFTimeInterval) duration
{
	CATransition *animation = [CATransition animation];
	[animation setType:kCATransitionFade];
	[animation setSubtype:nil];
	[animation setDuration:duration];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	[window.layer addAnimation:animation forKey:nil];
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Returns he auth
///////////////////////////////////////////////////////////////////////
+ (NSString *)authToken
{
    return [USER_DEFAULTS objectForKey:KEY_AUTH_TOKEN];
}

///////////////////////////////////////////////////////////////////////
//// Purpose            : Returns Section Title for a section id
///////////////////////////////////////////////////////////////////////
+ (NSString *)getSectionTitleForSectionId:(int)sec_id{
    
    switch (sec_id) {
        case VERBAL_REASONING:
            return @"Verbal Reasoning";
        case QUANTITATIVE_REASONING:
            return @"Quantitative Reasoning";
        case EARLY_LITERACY:
            return @"Early Literacy";
        case MATH:
            return @"Math";
        default:
            break;
    }
    
    return nil;
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Returns date string in format hh:mma-hh:mma from start and end time strings
///////////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSString *) returnTimeScheduleStringWithStartTime:(NSString *)startTime endTime:(NSString *) endTime {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startTimeDate = [dateFormatter dateFromString:startTime];
    NSDate *endTimeDate = [dateFormatter dateFromString:endTime];
    [dateFormatter setDateFormat:@"hh:mma"];
    NSString *startTimeString = [dateFormatter stringFromDate:startTimeDate];
    NSString *endTimeString = [dateFormatter stringFromDate:endTimeDate];
    [dateFormatter release];
    return [NSString stringWithFormat:@"%@-%@", startTimeString, endTimeString];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Returns date string in format MMMM dd, yyyy from a date string
///////////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSString *) examDateAfterFormattingFromString:(NSString *) dateString {
    //creating exam-date string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *tempDate = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    [dateFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
    NSString *formattedDate = [[dateFormatter stringFromDate:tempDate] uppercaseString];
    [dateFormatter release];
    return formattedDate;
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Returns seconds elapsed as string between two time intervals
///////////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSString *)secondsBetweenStartInterval:(NSTimeInterval)startTime andEndInterval:(NSTimeInterval)endTime{
    
    NSTimeInterval interval = 0.0;
    interval = endTime - startTime;
    interval = (interval > 0.0 && interval < 24.0*60.0*60.0) ? interval : 0.0;
    return [NSString stringWithFormat:@"%f",interval];
}


///////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Returns user name for the logged in administrator/supervisor
///////////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSString *) getUserName{
    return [USER_DEFAULTS objectForKey:KEY_USER_NAME];
}


///////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Deletes all dat from NSUserDefaults
///////////////////////////////////////////////////////////////////////////////////////////////////////
+ (void) clearUserDefaults{
    NSDictionary *defaultsDict = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    for (NSString *key in [defaultsDict allKeys])
    {
        if(![key isEqualToString:@"WebKitLocalStorageDatabasePathPreferenceKey"]
           && ![key isEqualToString:KEY_AUTH_TOKEN]
           && ![key isEqualToString:KEY_USER_AGENT]
           && ![key isEqualToString:REQUEST_POST_KEY_STUDENT_ID]
           && ![key isEqualToString:REQUEST_POST_KEY_EXAM_ID]
           && ![key isEqualToString:RESPONSE_KEY_QUESTIONS_COUNT]
           && ![key isEqualToString:KEY_CURRENT_ASSETS_REVISION])
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//// Purpose            : Returns the log type string from the event Type enum value
///////////////////////////////////////////////////////////////////////////////////////////////////////
+ (NSString *)getStringForLogType: (LOG_EVENT)eventType
{
    switch (eventType)
    {
        case LOG_EVENT_CHECKIN:
            return LOG_TYPE_CHECKIN;
        case LOG_EVENT_TUTORIAL:
            return LOG_TYPE_TUTORIAL;
        case LOG_EVENT_EXAM:
            return LOG_TYPE_EXAM;
        case LOG_EVENT_APP_RESTART:
            return LOG_TYPE_APP_RESTART;
        default:
            break;
    }
    return @"";
}

+ (NSString *)getUserAgentString{
    
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UDID" accessGroup:nil];
    
    NSString *UDIDString = [wrapper objectForKey:(id)kSecAttrAccount];
    
    if (UDIDString.length == 0) {
        UDIDString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [wrapper setObject:UDIDString forKey: (id)kSecAttrAccount];
    }
    SAFE_RELEASE(wrapper); //Released ..
    
    // changes for customized user-agent
    UIDevice *myDevice = [UIDevice currentDevice];
    
    NSString *userAgentString = [NSString stringWithFormat:@"%@ (%@; %@ %@)" , UDIDString,  myDevice.model, myDevice.systemName, [myDevice.systemVersion substringToIndex:1]];
    
    SAVE_USER_DEFAULTS_AND_SYNC(userAgentString, KEY_USER_AGENT);
    
    return userAgentString;
}

+ (NSString *) getCurrentAssetsRevision{
    
    NSString *revision = [USER_DEFAULTS objectForKey:KEY_CURRENT_ASSETS_REVISION];
    
    if (revision.length == 0) {
        revision = @"0";
    }
    else if (![[NSFileManager defaultManager] fileExistsAtPath:RESOURCES_DIRECTORY_PATH]){
        [self setCurrentAssetsRevision:@"0"];
        revision = @"0";
    }
    
    return revision;
}

+ (void) setCurrentAssetsRevision: (NSString *)revision{
    
    [USER_DEFAULTS setObject:revision forKey:KEY_CURRENT_ASSETS_REVISION];
    
}


+ (NSString *)getExamMode
{
    return [USER_DEFAULTS objectForKey:RESPONSE_KEY_ADMIN_EXAM_MODE];
}

+ (void)writeToFile:(NSString *)fileName data:(NSData *)fileData{
    
    NSError *error = nil;
    
    NSString *directoryPath = [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/Others"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:directoryPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
    
    NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
    
    if (! [[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
        if (error) {
            DLog(@"ERROR WHILE CREATING DATA FILE: %@",[error localizedDescription]);
        }
    }
    
    [fileData writeToFile:filePath options:NSDataWritingAtomic error:&error];
    
    if (error) {
        DLog(@"ERROR WHILE WRITING DATA TO FILE: %@",[error localizedDescription]);
    }
}

+ (NSData *)getDataFromFile:(NSString *)fileName {
    
    NSString *directoryPath = [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:@"Resources/Others"];
    NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
    NSError *error = nil;
    NSData *fileData = [[[NSData alloc] initWithContentsOfFile:filePath options:NSDataReadingUncached error:&error] autorelease];
    
    if (error) {
        DLog(@"ERROR WHILE READING DATA FROM FILE: %@",[error localizedDescription]);
        return nil;
    }
    
    return fileData;
}

+ (void) removeOfflineContent
{
    NSString *offlineResourcesPath = [DOCUMENTS_DIRECTORY stringByAppendingPathComponent:PATH_OFFLINE_MODE_CONTENT];
    NSError *e = nil;
    [[NSFileManager defaultManager] removeItemAtPath:offlineResourcesPath error:&e];
    if (e)
    {
        DLog(@"%@", e);
    }
}

+ (NSString *) getStringFromDate:(NSDate *)date inFormat:(NSString *)format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSString *stringFromDate = [formatter stringFromDate:date];
    [formatter release];
    
    return stringFromDate;
}

+ (NSString *)getTimeElapsedForInterval:(double)interval{
   
    NSString *date = @"";
    
    if (interval < 60.0) {
        date =  [NSString stringWithFormat:@"%0.2fs",  interval];
    }
    else if (interval >= 60.0 && interval < 60.0*60.0){
        date =  [NSString stringWithFormat:@"%0.2fm",  (interval/60.0)];
    }
    else if (interval >= 60.0*60.0 && interval < 60.0*60.0*24.0){
        date =  [NSString stringWithFormat:@"%0.2fh",  (interval/(60.0*60.0))];
    }
    else if (interval >= 60.0*60.0*24.0 && interval < 60.0*60.0*24.0*7.0){
        date =  [NSString stringWithFormat:@"%0.2fd",  (interval/(60.0*60.0*24.0))];
    }
    else if (interval >= 60.0*60.0*24.0*7.0){
        date =  [NSString stringWithFormat:@"%0.2fw",  (interval/(60.0*60.0*24.0*7.0))];
    }
    
    return date;
}

@end
