//
//  ERBDecrypter.h
//  ERB
//
//  Copyright 2013 © WebINTENSIVE Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ERBDecrypter : NSObject <NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
    NSMutableData *data;
    NSString *filePath;
    NSFileHandle *file;
}

@property (retain) NSString *filePath;
@property (retain) NSMutableData *data;
@property (retain) NSFileHandle *file;
@end
