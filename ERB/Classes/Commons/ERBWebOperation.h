//
//  ERBWebOperation.h
//  ERB
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ERBWebServiceConstants.h"
#import "NSDictionary+PostDataEncoding.h"

typedef enum
{
    ERB_REQUEST_POST = 0,
    ERB_REQUEST_GET
}WEB_SERVICE_REQUEST_METHOD;

typedef enum
{
	ERB_WS_OPERATION_STATUS_NONE = 0,
	ERB_WS_OPERATION_STATUS_SUCCESS,
	ERB_WS_OPERATION_STATUS_NETWORK_ERROR,
	ERB_WS_OPERATION_STATUS_LOCATION_ERROR
} WEB_SERVICE_OPERATION_STATUS;

typedef enum
{
    ERBPostDataEncodingTypeURL = 0,     //Default is URLEncoded
    ERBPostDataEncodingTypeJSON,
    ERBPostDataEncodingTypeSOAP,
    ERBPostDataEncodingTypeMultipart
}ERBPostDataEncodingType;

@protocol ERBWebServiceDelegate
- (void)webServiceDidReceiveResponse:(NSData *)responseData
				  requestIdentifier:(NSString *)requestId
			  operationStatus:(WEB_SERVICE_OPERATION_STATUS)status;
- (void) webServiceDidFailWithError: (NSError *) error requestIdentifier:(NSString *)requestId;
@end

@interface ERBWebOperation : NSObject
{
	BOOL busyStatus;
	NSMutableData *webData;
	NSURLConnection *activeConnection;
	id delegate;
}

@property (nonatomic, assign) id <ERBWebServiceDelegate> delegate;
@property (nonatomic, retain) NSURLConnection *activeConnection;
@property (nonatomic, assign) WEB_SERVICE_REQUEST_METHOD requestMethod;
@property (nonatomic, assign) ERBPostDataEncodingType postDataEncodingType;

- (BOOL)sendWebServiceRequest;
- (id) initWithRequestURL: (NSURL *)url
               withParams: (NSDictionary *)params
          withRequestIdentifier: (NSString *)requestId
           withHttpMethod: (WEB_SERVICE_REQUEST_METHOD)method;
@end
