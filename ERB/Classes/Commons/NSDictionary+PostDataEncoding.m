//
//  NSDictionary+PostDataEncoding.m
//  ERB
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#import "NSDictionary+PostDataEncoding.h"

@implementation NSDictionary (PostDataEncoding)

-(NSString*) urlEncodedKeyValueString {
    
    NSMutableString *string = [NSMutableString string];
    for (NSString *key in self) {
        
        NSObject *value = [self valueForKey:key];
        NSString *valueString = [NSString stringWithFormat:@"%@", value];
        [string appendFormat:@"%@=%@&", [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [valueString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if([string length] > 0)
        [string deleteCharactersInRange:NSMakeRange([string length] - 1, 1)];
    
    DLog(@"ENCODED URL STRING: %@", string);
    return string;
}


-(NSString*) jsonEncodedKeyValueString {
    
    if([NSJSONSerialization class]) {
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                       options:0 // non-pretty printing
                                                         error:&error];
        if(error)
            DLog(@"JSON Parsing Error: %@", error);
        
        return [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    } else {
        
        DLog(@"JSON encoder missing, falling back to URL encoding");
        return [self urlEncodedKeyValueString];
    }
}

@end
