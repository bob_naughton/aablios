//
//  ERBWebServiceConstants.h
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#ifndef ERB_ERBWebServiceConstants_h
#define ERB_ERBWebServiceConstants_h

#ifdef QA_VGD_SCHEME
#define ERB_URL_BASE_PATH                         @"https://erbdev:erbdev@vgdev1.erblearn.org/qa"
#elif VGD_SCHEME
#define ERB_URL_BASE_PATH                         @"https://erbdev:erbdev@vgdev1.erblearn.org"
#elif AABL_SCHEME
#define ERB_URL_BASE_PATH                         @"https://aabl.erblearn.org"
#elif AABL_STAGE_SCHEME
#define ERB_URL_BASE_PATH                         @"https://aablstg.erblearn.org"
#endif

#define LOG_WEB_SERVICE_DESCRIPTION(url,params)     DLog(@"Calling Web Service: %@ \n with Params: %@", url, params);

#define ERB_URL_AUTHENTICATION_PATH             @"/api/rest/authenticate"
#define ERB_URL_FETCH_EXAM_LIST_PATH            @"/api/rest/getCheckInList"
#define ERB_URL_CHECK_IN_PATH                   @"/api/rest/checkIn"
#define ERB_URL_GET_EXAM_STATUS_PATH            @"/api/rest/getExamStatus"
#define ERB_URL_RECORD_USER_ACTION              @"/api/rest/userActions"
#define ERB_URL_LOG_EVENT                       @"/api/rest/recordEventLogs"
#define ERB_URL_STUDENT_HELP                    @"/api/rest/addStudentHelp"
#define ERB_URL_SAVE_CLIENT_LOG_FILE            @"/api/rest/saveClientLogFile"
#define ERB_URL_GET_ALL_QUESTIONS               @"/api/rest/getAllQuestions"
#define ERB_URL_GET_ASSETS_DOWNLOAD             @"/api/rest/getResourcePath"
#define ERB_URL_ADD_OFFLINE_RESPONSES           @"/api/rest/addOfflineResponses"
//Download path
#define ERB_URL_DOWNLOAD_ASSETS_PATH            @"/api/rest/getResourcePath"

#define TIME_OUT_INTERVAL                                       15.0

//Post body keys

// For Asset update

#define REQUEST_POST_KEY_CURRENT_ASSET_REVISION @"revision_number"
#define REQUEST_POST_KEY_LOCAL                  @"local"

//For Web service getCheckInList
#define REQUEST_POST_KEY_AUTH_TOKEN             @"authToken"
#define REQUEST_POST_KEY_EXAM_ID                @"exam_id"
#define REQUEST_POST_KEY_DIGEST                 @"digest"
#define RESPONSE_KEY_SCHOOL_NAME                @"school_name"

// Server Response Keys
//User Defaults keys
#define KEY_EXAM_STATE                          @"examState"
#define KEY_DID_EXAM_END                        @"didExamEnd"
#define KEY_OFFLINE_QUESTIONS                   @"OfflineQuestionArray"
#define KEY_USER_AGENT                          @"User_Agent"
#define KEY_AUTH_TOKEN                          @"authToken"
#define KEY_USER_NAME                           @"user_name"

#define RESPONSE_KEY_CODE                       @"code"
#define RESPONSE_KEY_DATA                       @"data"
#define RESPONSE_KEY_ERROR_MESSAGE              @"error_message"
#define RESPONSE_KEY_FIRST_NAME                 @"first_name"
#define RESPONSE_KEY_LAST_NAME                  @"last_name"
#define RESPONSE_KEY_ROLE                       @"role"
#define RESPONSE_KEY_EXAM_NAME                  @"exam_name"
#define RESPONSE_KEY_START_TIME                 @"start_time"
#define RESPONSE_KEY_END_TIME                   @"end_time"
#define RESPONSE_KEY_EXAM_NAME                  @"exam_name"
#define RESPONSE_KEY_STUDENTS                   @"students"
#define RESPONSE_KEY_SUPERVISOR_NAMES           @"supervisor_names"
#define RESPONSE_KEY_ADMIN_NAMES                @"admin_names"
#define RESPONSE_KEY_ADMIN_ID                   @"admin_id"
#define RESPONSE_KEY_DOB                        @"dob"
#define RESPONSE_KEY_GENDER                     @"gender"
#define RESPONSE_KEY_STUDENT_NAME               @"name"
#define RESPONSE_KEY_STUDENT_ID                 @"student_id"
#define RESPONSE_KEY_VERIFIED                   @"verified"
#define RESPONSE_KEY_IS_PROCTOR                 @"is_proctor"
#define RESPONSE_KEY_STUDENT_SEAT               @"student_seat"
#define RESPONSE_KEY_QUESTION                   @"question"
#define RESPONSE_KEY_ADMIN_EXAM_MODE            @"admin_exam_mode"
#define RESPONSE_KEY_ALL_QUESTIONS              @"all_questions"
#define RESPONSE_KEY_HELP_HAND                  @"help_hand"
#define RESPONSE_KEY_CURRENT_REVISION           @"current_revision"
#define RESPONSE_KEY_DELETED_FILES              @"deleted_files"
#define RESPONSE_KEY_UPDATED_FILES              @"updated_files"
#define RESPONSE_KEY_DOWNLOADS_LIST             @"download_list"
#define RESPONSE_KEY_DOWNLOAD_PATH              @"download_path"
#define RESPONSE_KEY_DOWNLOAD_ZIP_HASH          @"hash"
#define RESPONSE_KEY_DOWNLOAD_SIZE              @"size"
#define RESPONSE_KEY_QUESTIONS_COUNT            @"question_count_per_section"

#define REQUEST_POST_KEY_STUDENT_ID             @"student_id"
#define REQUEST_POST_KEY_SECTION_ID             @"section_id"
#define REQUEST_POST_KEY_ACTION                 @"action"
#define REQUEST_POST_KEY_QUESTION_ID            @"question_id"
#define REQUEST_POST_KEY_ERB_QUESTION_ID        @"erb_question_id"
#define REQUEST_POST_KEY_RESPONSE               @"response"
#define REQUEST_POST_KEY_RESPONSES_ARRAY        @"responses"
#define REQUEST_POST_KEY_DESIRED_RESPONSE       @"desired_response"
#define REQUEST_POST_KEY_IS_RESPONSE_CORRECT    @"is_response_correct"
#define REQUEST_POST_KEY_HELP_FLAG              @"help_flag"
#define REQUEST_POST_KEY_PAUSE_FLAG             @"pause_flag"
#define REQUEST_POST_KEY_SECONDS_ELAPSED        @"seconds_elapsed"
#define REQUEST_POST_KEY_SECONDS_PAUSED         @"seconds_paused"
#define REQUEST_POST_KEY_CHECK_IN               @"check_in"
#define REQUEST_POST_KEY_RECOVER                @"recover"

#define REQUEST_POST_KEY_LOG_JSON               @"data"
#define REQUEST_POST_KEY_LOG_TIMESTAMP          @"timeStamp"
#define REQUEST_POST_KEY_LOG_TYPE               @"logType"
#define REQUEST_POST_KEY_USER_AGENT             @"userAgent"
#define REQUEST_POST_KEY_BUILD_VERSION          @"buildVersion"
#define REQUEST_POST_KEY_RESPONSES_ARRAY        @"responses"


#define REQUEST_POST_KEY_STUDENT_PROGRESS_IDENTIFIER    @"student_progress_identifier"

#define REQUEST_ID_AUTHENTICATE                 @"authenticate"
#define REQUEST_ID_REPORT_USER_ACTION           @"ReportUserActionWebService"
#define REQUEST_ID_REPORT_USER_EXAM_ACTION      @"ReportUserExamActionWebService"
#define REQUEST_ID_HELP_WEB_SERVICE             @"HelpWebService"
#define REQUEST_ID_REPORT_USER_TUTORIAL_ACTION  @"ReportUserTutorialActionWebService"
#define REQUEST_ID_REPORT_USER_SECTION_ACTION   @"ReportUserSectionActionWebService"
#define REQUEST_ID_UPLOAD_RESPONSE_ACTION       @"UploadResonseWebService"

#define REQUEST_ID_GET_EXAM_STATUS              @"GetExamStatusWebService"
#define REQUEST_ID_STUDENT_CHECKIN_WS           @"StudentCheckinWebService"
#define REQUEST_ID_CHECKIN_WS                   @"CheckinWS"
#define REQUEST_ID_LOG_EVENTS                   @"LogEvents"
#define REQUEST_ID_GET_ALL_QUESTIONS            @"GetAllQuestions"
#define REQUEST_ID_GET_ASSETS_DOWNLOAD          @"GetAsetsDownload"

#define RESPONSE_KEY_ENABLE_CLIENT_LOGGING      @"enable_client_logging"
#endif
