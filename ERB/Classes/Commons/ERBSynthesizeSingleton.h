//
//  ERBSynthesizeSingleton.h
//  ERB
//
//  Copyright 2015 © WebINTENSIVE Software. All rights reserved.
//

#define SYNTHESIZE_SINGLETON(class) \
 \
static class *shared##class = nil; \
 \
+ (class *)shared##class \
{ \
	@synchronized(self) \
	{ \
		if (shared##class == nil) \
		{ \
			shared##class = [[self alloc] init]; \
		} \
	} \
	 \
	return shared##class; \
} \
 \
+ (id)allocWithZone:(NSZone *)zone \
{ \
	@synchronized(self) \
	{ \
		if (shared##class == nil) \
		{ \
			shared##class = [super allocWithZone:zone]; \
			return shared##class; \
		} \
	} \
	 \
	return nil; \
} \
 \
- (id)copyWithZone:(NSZone *)zone \
{ \
	return self; \
} \
 \
- (id)retain \
{ \
	return self; \
} \
 \
- (NSUInteger)retainCount \
{ \
	return NSUIntegerMax; \
} \
 \
- (oneway void)release \
{ \
} \
 \
- (id)autorelease \
{ \
	return self; \
}
