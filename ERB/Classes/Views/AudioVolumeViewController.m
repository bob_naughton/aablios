//
//  AudioVolumeViewController.m
//  ERB

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */

#import "AudioVolumeViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import "AudioPlayer.h"

@interface AudioVolumeViewController ()
- (void) playBubblePopSound;
- (void) handleExternalVolumeChanged: (NSNotification *)notification;
@property (nonatomic, retain) AVAudioPlayer *localAudioPlayer;
@end

@implementation AudioVolumeViewController
@synthesize volumeLabel;
@synthesize volumeNameLabel;
@synthesize plusButton, minusButton, localAudioPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.tag = VOL_VIEW_CONTROLLER_TAG;
    
    self.volumeNameLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:30.0];
    self.volumeLabel.font = [UIFont fontWithName:FONT_NAME_FUTURA_HEAVY size:100.0];
    
    //Register for volume change notification.
    MPMusicPlayerController *musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleExternalVolumeChanged:)
                                                 name:MPMusicPlayerControllerVolumeDidChangeNotification
                                               object:musicPlayer];
    
    [musicPlayer beginGeneratingPlaybackNotifications];
    
    //Set the initial volume labels
    [[AudioPlayer sharedAudioPlayer] setAudioVolume:1.0];
    
    MPMusicPlayerController *musicPlayerController = [MPMusicPlayerController applicationMusicPlayer];
    [self.volumeLabel setText:[NSString stringWithFormat:@"%1.0f", musicPlayerController.volume*10]];
}

-(void)viewDidUnload{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setVolumeLabel:nil];
    [self setVolumeNameLabel:nil];
    [self setPlusButton:nil];
    [self setMinusButton:nil];
  
    [super viewDidUnload];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SAFE_RELEASE(localAudioPlayer);    
    [volumeLabel release];
    [volumeNameLabel release];
    [plusButton release];
    [minusButton release];
    [super dealloc];
}


#pragma mark - Action Methods

- (IBAction)minusVolumeButtonPressed:(id)sender
{
    MPMusicPlayerController *musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    
    if (musicPlayer.volume == 0.0)
    {
        [sender setEnabled:NO];
    }
    else
    {
        musicPlayer.volume -= 0.1;
        [sender setEnabled:YES];
    }
    
    //Set the sharedAudioPlayer's volume as its audioPlayer's volume
    [[AudioPlayer sharedAudioPlayer] setAudioVolume:musicPlayer.volume];
    
    self.plusButton.enabled = YES;
    [self.volumeLabel setText:[NSString stringWithFormat:@"%1.0f", musicPlayer.volume*10]];
    
    [self playBubblePopSound];
}

- (IBAction)plusVolumeButtonPressed:(id)sender
{
    MPMusicPlayerController *musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    
    if (musicPlayer.volume == 1.0)
    {
        [sender setEnabled:NO];
    }
    else
    {
        musicPlayer.volume += 0.1;
        [sender setEnabled:YES];
    }
    
    //Enable the minus volume button (in case disabled) when volume is increased
    self.minusButton.enabled = YES;
    
    //Set the sharedAudioPlayer's volume as its audioPlayer's volume
    [[AudioPlayer sharedAudioPlayer] setAudioVolume:musicPlayer.volume];
    
    [self.volumeLabel setText:[NSString stringWithFormat:@"%1.0f", musicPlayer.volume*10]];
    
    [self playBubblePopSound];
}

#pragma mark - Private Methods

- (void) playBubblePopSound
{
    if (localAudioPlayer)
    {
        SAFE_RELEASE(localAudioPlayer);
    }
    NSURL *soundurl   = [[NSBundle mainBundle] URLForResource:AUDIO_ADJUST_VOLUME withExtension:nil];
    NSError *error;
    localAudioPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:soundurl error:&error];
    [self.localAudioPlayer prepareToPlay];
    
    [self.localAudioPlayer stop];
    [self.localAudioPlayer play];
}

- (void) handleExternalVolumeChanged: (NSNotification *)notification
{
    MPMusicPlayerController *musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    [self.volumeLabel setText:[NSString stringWithFormat:@"%1.0f", musicPlayer.volume*10]];
    
    //Set the sharedAudioPlayer's volume as its audioPlayer's volume
    [[AudioPlayer sharedAudioPlayer] setAudioVolume:musicPlayer.volume];
    
    if (musicPlayer.volume == 0.0) {
        self.minusButton.enabled = NO;
    }
    else if (musicPlayer.volume == 1.0) {
        self.plusButton.enabled = NO;
    }
    
    if (musicPlayer.volume > 0.0)
    {
        self.minusButton.enabled = YES;
    }
    else if (musicPlayer.volume < 1.0)
    {
        self.plusButton.enabled = YES;
    }
}



@end
