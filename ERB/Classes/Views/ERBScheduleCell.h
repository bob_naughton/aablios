//
//  ERBScheduleCell.h

/*
 * @author WebINTENSIVE Software
 *
 * Copyright © 2015 Vanguard Direct. All rights reserved. Licensed for use in ERB's AABL 2015 Application.
 *
 */


#import <UIKit/UIKit.h>

@protocol ERBScheduleCellDelegate <NSObject>
-(void)didPressLaunchNowWithTag:(int) tag;
@end

@interface ERBScheduleCell : UITableViewCell

@property (nonatomic, assign) id <ERBScheduleCellDelegate> delegate;
@property (retain, nonatomic) IBOutlet UILabel *timeLabel;
@property (retain, nonatomic) IBOutlet UILabel *examLabel;
@property (retain, nonatomic) IBOutlet UILabel *supervisorLabel;
@property (retain, nonatomic) IBOutlet UILabel *studentsLabel;
@property (retain, nonatomic) IBOutlet UILabel *administratorsLabel;
@property (retain, nonatomic) IBOutlet UIButton *launchButton;

- (IBAction)launchExamButtonPressed:(id)sender;

@end
